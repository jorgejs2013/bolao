<?php
namespace Models;
use Core\Model;
use PDO;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email extends Model{

	private $mailer;

	public function __construct(){		

	$this->mailer = new PHPMailer(true);

    //Server settings
    //$mail->SMTPDebug = 2;   // Enable verbose debug output
    $this->mailer->isSMTP();           // Set mailer to use SMTP
    $this->mailer->Host = 'a1-caju43.servidorwebfacil.com';  // Specify main and backup SMTP servers
    $this->mailer->SMTPAuth = true;                               // Enable SMTP authentication
    $this->mailer->Username = 'contato@dinamicaitororo.com.br';                 // SMTP username
    $this->mailer->Password = '#dinamica123#';                           // SMTP password
    $this->mailer->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $this->mailer->Port = 465; 

    //Recipients
    $this->mailer->setFrom('contato@dinamicaitororo.com.br', 'Escola Dinamica');
    $this->mailer->isHTML(true);
    $this->mailer->CharSet = 'UTF-8';  
    }//construtor


   public function addAdress($email, $nome){
 	   $this->mailer->addAddress($email, $nome);
   }

   public function formatarEmail($info){
    //Content
    $this->mailer->Subject = $info['assunto'];
    $this->mailer->Body    = $info['corpo'];
    $this->mailer->AltBody = strip_tags($info['corpo']);
   }//formatarEmail

   public function enviarEmail(){
   	 if($this->mailer->send()){
   	 	return true;
   	 }else{
   	 	return false;
   	 }    
    
   }//enviarEmail 

}

// Como usar
/*
$email = new Email();
$email = new Email();
    $email->addAdress('conquistaprime@gmail.com', 'Conquista prime');
    $info = array('assunto'=>'Um novo email', 'corpo'=>'mensagem do email');
    $email->formatarEmail($info);
    if($email->enviarEmail()){
        echo "E-mail enviado com sucesso";
    }else{
        echo "algo deu errado tente novamente!";
    } 

    */