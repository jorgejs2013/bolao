<?php
namespace Models;
use Core\Model;
use PDO;

class Graficos extends Model {

public function totalPorTurno(){
$dados = array();

$sql = "SELECT DISTINCT(turno) FROM contato";
$sql = $this->db->query($sql);
$result = $sql->fetchAll();
$dados = array();

foreach($result as $key => $turno):
	$nome_turno = ($turno->turno === '0') ? 'Indefinido' : $turno->turno;	
	$dados[$key][$nome_turno] = $this->contaPorTurno($turno->turno);
endforeach;

return $dados;
}//totalPorTurno

private function contaPorTurno($turno){
	$sql = "SELECT COUNT(id_contato) as soma FROM contato WHERE turno = '$turno'";
	$sql  = $this->db->query($sql);
	$result = $sql->fetch();
      
      return $result->soma;     
}//contaPorTurno



public function getAniversariantes($mes){
$sql = "SELECT nome, data_nasc FROM contato WHERE MONTH(data_nasc) = :mes";
$sql = $this->db->prepare($sql);
$sql->bindValue(":mes", $mes);
$sql->execute();

return $sql->fetchAll();
}//getAniversariantes



public function totalPorCurso(){
$sql = "SELECT DISTINCT(id_curso) FROM contato";
$sql = $this->db->query($sql);
$result = $sql->fetchAll();
$dados = array();

foreach ($result as $key => $value) {
	$nome_curso = $this->getCurso($value->id_curso);
	$dados[$key][$this->filtrarNome($nome_curso)] = $this->qtdeAlunos($value->id_curso);
}

return $dados;
}//totalPorCurso

private function getCurso($curso){
$sql = "SELECT nome FROM cursos WHERE id_curso = '$curso'";
$sql = $this->db->query($sql);

if($sql->rowCount() >0){
	$result = $sql->fetch();
	return $result->nome;
}else{
	return 'Indefinido';
}

}//getCurso

public function qtdeAlunos($curso){
$sql = "SELECT COUNT(id_contato) as soma FROM contato WHERE id_curso = '$curso'";
$sql = $this->db->query($sql);
$result = $sql->fetch();
return $result->soma;
}//qtdeAlunos



private function filtrarNome($SetPalavra){
$this->string = preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($SetPalavra)));
return strtoupper($this->string);	
}

}
