<?php
namespace Models;
use Core\Model;
use PDO;

Class SearchProduto extends Model{

private $limit = 10;
private $page = 1;
private $start = 0;
private $query = '';
private $filtro = '';
private $totalRegistros = 0;
private $totalRegistrosFiltro = 0;
private $saida = '';
private $resultado = array();
private $total_links = 0;
private $link_anterior = '';
private $link_proximo = '';
private $link_pagina = '';


public function verificaPagina($pagina){

  if(isset($pagina) && $pagina >1){
    $this->page = $pagina;
    $this->start = ($pagina -1) * $this->limit;
  }
}//verificaPagina

public function consulta($tabela, $query = '', $query2 = '', $coluna = '', $ordem = ''){  
$this->query = 'SELECT * FROM '.$tabela;

if($query != ''){
  $this->query .= ' WHERE '.$coluna.' LIKE "%'.str_replace(' ', '%', $query).'%" ';
}

if($query == '' && $query2 != ''){
  $this->query .= ' WHERE tipo = "'.$query2.'"';
}else if($query != '' && $query2 != ''){
  $this->query .= ' OR tipo = "'.$query2.'"';
}

if($ordem != ''){ $this->query .= ' ORDER BY '.$ordem.' ASC'; }
$this->filtro  = $this->query . ' LIMIT '.$this->start.', '.$this->limit;

$sql = $this->db->prepare($this->query);
$sql->execute();
$this->totalRegistros = $sql->rowCount();

$sql = $this->db->prepare($this->filtro);
$sql->execute();
$this->resultado = $sql->fetchAll();
$this->totalRegistrosFiltro = $sql->rowCount();

$this->montaSaida();
}//consulta



public function montaSaida(){

$this->saida = '<label></label>';

if($this->totalRegistros > 0)
{ 
  foreach($this->resultado as $row)
  {
    $style = ($row->estoque_atual > $row->estoque_minimo) ? '': 'style="background:#ff7979;opacity:0.55"';   
    $img = ($row->imagem == '') ? 'not_image.jpg' : $row->produto;
    $this->saida .= '
    <tr '.$style.'>
      <td>'.$row->id_produto.'</td>      
      <td><img src="'.BASE_URL."medias/produtos/".$img.'" width="50" height="30"/></td>
      <td>'.$row->produto.'</td>
      <td>'.$row->estoque_minimo.'</td>
      <td>'.$row->estoque_atual.'</td>
      <td>R$ '.number_format($row->preco,2,',','.').'</td>
      <td>
     <a href="'.BASE_URL.'produtos/edit/'.$row->id_produto.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  | 
     <a href="'.BASE_URL.'produtos/del/'.$row->id_produto.'"><i class="fa fa-trash" aria-hidden="true"></i></a> 
     </td>
    </tr>
    ';
  }

  //linha do rodape
  $this->saida .= '<tr>
                    <td colspan="5" align="left"><strong>Total de registros</strong> - '.$this->totalRegistros.'</td>
                  </tr>';  
}

else
{
  $this->saida .= '
  <tr>
    <td colspan="5" align="center">Nenhum registro encontrado!</td>
  </tr>
  ';
}

$this->saida .= '
<tr>
<td colspan="5" align="center">
<ul class="pagination">';
$this->total_links = ceil($this->totalRegistros / $this->limit);

if($this->total_links > 4)
{ 
 
  if($this->page < 5)
  {  
    
    for($count = 1; $count <= 5; $count++)
    {
      $page_array[] = $count;
    }

    $page_array[] = '...';
    $page_array[] = $this->total_links;
  }
  else //se page menor que 5
  {      
    $end_limit = $this->total_links - 5;
    if($this->page > $end_limit)
    {
      $page_array[] = 1;
      $page_array[] = '...';

      for($count = $end_limit; $count <= $this->total_links; $count++)
      {
        $page_array[] = $count;
      }
    }
    else // se page maior do que o limite
    {

      $page_array[] = 1;
      $page_array[] = '...';
      for($count = $this->page - 1; $count <= $this->page + 1; $count++)
      {
        $page_array[] = $count;
      }//fim do for

      $page_array[] = '...';
      $page_array[] = $this->total_links;
    }
  }
}

else//se  total links menor que 4
{ 
  
  for($count = 1; $count <= $this->total_links; $count++)
  {
    $page_array[] = $count;
  }
}

if(empty($page_array)){
 $page_array = array();
}

for($count = 0; $count < count($page_array); $count++)
{
  if($this->page == $page_array[$count])
  {
    $this->link_pagina .= '
    <li>
      <a class="page-item active" href="#">'.$page_array[$count].' <span class="sr-only">(current)</span></a>
    </li>';

    $previous_id = $page_array[$count] - 1;   
    if($previous_id > 0)
    {
      $this->link_anterior = '<li><a class="page-item page-link" href="javascript:void(0)" data-page_number="'.$previous_id.'">Anterior</a></li>';     
    }
    else
    {
      $this->link_anterior = '
      <li class="page-item disabled">
        <a class="page-link" href="#">Anterior</a>
      </li>';
    }

    $next_id = $page_array[$count] + 1;
    if($next_id >= $this->total_links)
    {
      $this->link_proximo = '
      <li class="page-item disabled">
        <a class="page-link" href="#">Próximo</a>
      </li>';
    }
    else
    {
       $this->link_proximo = '<li><a class="page-link" href="javascript:void(0)" data-page_number="'.$next_id.'">Próximo</a></li>';
    }
  }
  else
  {  

    if($page_array[$count] == '...')
    {
      $this->link_pagina .= '
      <li class="disabled">
          <a class="page-link" href="#">...</a>
      </li>';
    }
    else
    {
      $this->link_pagina .= '
      <li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$page_array[$count].'">'.$page_array[$count].'</a></li>';
    }
  }
}

$this->saida .= $this->link_anterior . $this->link_pagina . $this->link_proximo;
$this->saida .= ' </li></td> </tr>';

echo $this->saida;
}//montaSaida


}//classe