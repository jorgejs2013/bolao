<?php
namespace Models;
use \Core\Model;
use PDO;

class Concursos extends Model{

public function listaConcursos($concurso, $operador = null, $apostador = null){
$retorno = array();

$sql = "SELECT 
concursos.ID as id_concurso,
concursos.NOME,
concursos.DATA,
concursos.DATA_LIMITE,
concursos.HORA_LIMITE,
concursos.VALOR_APOSTA,
concursos.VALOR_ACUMULADO,
concursos.TAXA_ADMINISTRACAO,
concursos.COMISSAO_VENDEDOR,
concursos.OBSERVACAO,
numeros_apostados.ID_CONCURSO,
numeros_apostados.DATA,
numeros_apostados.HORA,
numeros_apostados.USUARIO,
numeros_apostados.NUMEROS_APOSTADOS,
numeros_apostados.NUMEROS_ACERTADOS,
numeros_apostados.NUMEROS_NAO_ACERTADOS,
numeros_apostados.PONTUACAO,
numeros_apostados.APOSTADOR,
numeros_apostados.TELEFONE,
numeros_apostados.VALOR_APOSTA,
numeros_apostados.SITUACAO,
numeros_apostados.DISPOSITIVO,
numeros_apostados.data_cancelamento,
numeros_apostados.hora_cancelamento,
numeros_apostados.GANHOU_MILHAR_BRINDE,
operadores.NOME as nome_operador
FROM concursos
INNER JOIN numeros_apostados
ON concursos.ID = numeros_apostados.ID_CONCURSO
INNER JOIN operadores
ON numeros_apostados.USUARIO = operadores.ID";
if($operador != null){
	$sql.= " AND numeros_apostados.USUARIO = $operador";
}

if($apostador != null){
	$sql .= " AND numeros_apostados.APOSTADOR = '$apostador'";
}

$sql = $this->db->query($sql);

if($sql->rowCount() >0){
	$retorno = $sql->fetchAll();
}


return $retorno;
}//listaPorIdPedido

}