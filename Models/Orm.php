<?php
namespace models;
use Core\Model;
use Models\Ormbuilder;
use Models\Selectpaginatebuilder;
use PDO;

Class Orm extends Model{

private $tabela;
private $sql;
private $dados = array();
private $error = array();
private $debug;

private $select;
private $del;
private $save;
private $set;

private $first;
private $where;
private $wherecustom;
private $e;
private $ou;
private $join;
private $on;
private $search;
private $orSearch;
private $order;
private $group;
private $limit;
private $total;
private $paginate;
private $selectToPaginate;
private $conditionsPaginate;
private $selectPaginateBuilder;
private $isSerch;


public function __construct($tabela){
  $this->tabela = $tabela;
  parent::__construct();	
}//construtor

public function select($select){
$this->select = $select;

return $this;
}//select

public function search(...$search){	

	if($search[0][1] != ''){
		$this->search = $search;
	}	
	return $this;
}//search


public function orSearch(...$orSearch){
$retorno = [];

foreach($orSearch as $key=>$orsearch){
   if($orsearch[1] != ''){
    $retorno[] = $orsearch;
   }
}	

$this->orSearch = $retorno;
return $this;
}//orSearch

public function where(...$where){
	$this->where = $where;
	return $this;
}//where


public function wherecustom($where){
	$this->wherecustom = $where;
	return $this;
}//wherecustom

public function e(...$and){
	$this->e = $and;
	return $this;
}//and

public function ou(...$or){
	$this->ou = $or;
	return $this;
}//or

public function order(...$order){
	$this->order = $order;
	return $this;
}//order

public function group(...$group){
	$this->group = $group;
	return $this;
}//group

public function limit(...$limit){
	$this->limit = $limit;
	return $this;
}//limit

public function join(...$join){
	$this->join = $join;
	return $this;
}//join

public function on(...$on){

if(count($on[0]) <=2 ){
	throw new \Exception("O método on precisa de 3 parametros");		
}
	$this->on = $on;
	return $this;
}//on

public function first(){
 $this->first = true;
 return $this;
}//first

public function debug(){
$this->debug = true;

return $this;
}//debug





public function total(){
	$this->total = true;
	return $this;
}//rowCount

public function paginate(...$paginate){
$this->paginate = $paginate;
return $this;
}//paginate


/***************************************/
/*Monta a estrutura e mostra ao usuario*/
/***************************************/

public function get(){
$ob = new Ormbuilder();
$this->sql = $ob->selectCamposTabela($this->select, $this->tabela);

$conditionsPaginate = (object) array(
'isSearch'=>false,
'select'=>'',
'where'=>'',
'search'=>'',
'orSearch'=>''
);

if(isset($this->join)){
  $this->sql .= $this->montaJoin($this->join, $this->on);
}//join


if(isset($this->search)){
 $this->sql .=$this->montaSearch($this->search);
 $conditionsPaginate->isSearch = true;
}//Search

if(isset($this->orSearch)){
  $this->sql .= $this->montaOrSearch($this->orSearch);
   $conditionsPaginate->isSearch = true;
}//orSearch


//aqui começo a verificar se existe os outros comandos
if(isset($this->where)){	
 $this->sql .= $this->montaWhere($this->where);	
}//se existe o where

if(isset($this->wherecustom)){	
 $this->sql .= $this->montaWhereCustom($this->wherecustom);	
}//se existe o where

if(isset($this->e)){
  $this->sql .= $this->montaAnd($this->e);
}//se existe o and



if(isset($this->ou)){
  $this->sql .= $this->montaOr($this->ou);
}//se existe or

if(isset($this->order)){
  $this->sql .= $this->montaOrder($this->order);
}//se existe order

if(isset($this->group)){
  $this->sql .= $this->montaGroup($this->group);
}//group

if(isset($this->limit)){
 $this->sql .= $this->montalimit($this->limit);
}//limit

if(isset($this->debug)){
	var_dump($this->sql);
	exit;		
}

/*******************************/
/*Preparar o ORM para paginação*/
/*******************************/
// se existe a paginação
if(isset($this->paginate)){
$conditionsPaginate->select = $this->sql;

if(isset($this->where)){
  $conditionsPaginate->where = $this->where; 
}  

if(isset($this->search)){	
  $conditionsPaginate->search = $this->search;   
}  

if(isset($this->orSearch)){		
  $conditionsPaginate->orSearch = $this->orSearch;  
}

//caso nao ha search, orSearch ou where
if(!isset($this->search) && !isset($this->orSearch) && !isset($this->where)){	
 $query = $this->sqlToPaginate($this->paginate, $conditionsPaginate);  
 $this->sql .= $query; 	
}

if(isset($this->where) && !empty($this->where)){	
 $query = $this->sqlToPaginate($this->paginate, $conditionsPaginate);
 $this->sql .= $query;
}

//caso exista search, orSearch ou where
if(isset($this->search) && !isset($this->orSearch)){		
 $query = $this->sqlToPaginate($this->paginate, $conditionsPaginate);
 $this->sql .= $query;
}else if(isset($this->search) && isset($this->orSearch)){
 $query = $this->sqlToPaginate($this->paginate, $conditionsPaginate);
 $this->sql .= $query;
}
// $conditionsPaginate->isSearch = true;	
}//se existe a paginação

/************************/
//preparando para executar


$select = $this->db->prepare($this->sql);

if(isset($this->where)){ 
  $ob = new Ormbuilder();
  $select = $ob->whereBind($this->where, $select);
}//where

if(isset($this->e)){ 
  $ob = new Ormbuilder();
  $select = $ob->andBind($this->e, $select);
}//and

if(isset($this->ou)){ 
  $ob = new Ormbuilder();
  $select = $ob->orBind($this->ou, $select);
}//or

if(isset($this->search)){
$ob = new Ormbuilder();
$select = $ob->searchBind($this->search, $select);

if(isset($selectPaginate)):
  $selectPaginate = $searchBind->search($this->search, $selectPaginate);
endif;
}//se existe o search

if(isset($this->orSearch)){
$ob = new Ormbuilder();

$select = $ob->orSearchBind($this->orSearch, $select);


if(isset($selectPaginate)):	
	echo "aqui dentro";exit;
//$selectPaginate = $orSearchBind->orSearch($this->orSearch, $selectPaginate);
endif;
}//se existe o orSearch

/*********************/
/*executando o codigo*/
/*********************/

try{
$select->execute();

if(isset($this->first)){	
  $this->dados =  $select->fetch();
}else{
  $this->dados =  $select->fetchAll();
}

if(isset($this->total)){	 
 return $select->rowCount();
}

return $this->dados;
}catch(\Exception $e){
	var_dump($e->getMessage().' no arquivo '.$e->getFile().' na linha: '.$e->getLine());
}//bloco try cacth

}//get


/************************/
/*Instrução para deletar*/
/************************/
public function del($del, $e = null){	
$this->sql = "delete from {$this->tabela} where {$del[0]} = :{$del[0]}";

$delete = $this->db->prepare($this->sql);
$delete->bindValue(":{$del[0]}", $del[1]);
	
return $delete->execute();
}//del

/*******************************/
/*comando para inserir no banco*/
/*******************************/
public function set($set){
$campos = new \stdClass();

foreach($set as $key=>$item){
  $campos->$key = $item;	
}

$this->set = $campos;	
return $this;
}//set

public function save(){
if($this->set != ''){

$array = json_decode(json_encode($this->set), true);
$campos = implode(", ", array_keys($array));
$valores = ":".implode(", :", array_keys($array));

$this->sql = "insert into {$this->tabela} ({$campos}) VALUES ({$valores}) ";
$stmt = $this->db->prepare($this->sql);
foreach($this->set as $chave => $valor):
$tipo = (is_int($valor)) ? PDO::PARAM_INT : PDO::PARAM_STR;
$stmt->bindValue(":$chave", $valor, $tipo);
endforeach;

try{

if($stmt->execute()){
	return $this->db->lastInsertId();
}

}catch(\Exception $e){
	echo "Erro: ".$e->getMessage().' linha '.$e->getLine();
}


}else{
	echo "Primeiro é preciso setar os campos";exit;
}

}//save
/****************************/
/*Fim Instrução para insert*/
/****************************/

public function update(){
if($this->set != ''){

$this->sql = "update {$this->tabela} set";

foreach($this->set as $key => $value){
	$this->sql .= " {$key} = :{$key},";
}
$this->sql = rtrim($this->sql,',');

if(!empty($this->where)){
  $this->sql .= " where ".$this->where[0][0]."= :".$this->where[0][0];
}

/*Preparando a query*/
$update = $this->db->prepare($this->sql);
foreach ($this->set as $key => $value) {
	$update->bindValue(":".$key, $value);
}

if(!empty($this->where)){
  $update->bindValue(":".$this->where[0][0], $this->where[0][1]);
}


try{
if($update->execute()){
	return $update->rowCount();
}

}catch(\Exception $e){
	echo "Erro: ".$e->getMessage().' linha '.$e->getLine();
}

}else{
	echo "Primeiro é preciso setar os campos";exit;
}
}//update



/****************************/
/*Fim Instrução para deletar*/
/****************************/

public function montaWhere($wheres){

if(count($wheres) > 1){
$sql = null;

foreach($wheres as $key => $value):

$paramSql = $value[0];

$valueSql = $value[0];

$sinalSql = $value[1];

if(count($value) == 2){
$sinalSql = '=';
}

$sql .= " {$paramSql} {$sinalSql} :{$valueSql}";
$sql .= ' and';
endforeach;

$sql = trim($sql, 'and');

return " where {$sql}";
}

if(count($wheres[0]) == 2){
	foreach($wheres as $where){
       $param = $where[0];
       $sinal = '=';
       $value = $where[0];	
     }
    }

if(count($wheres[0]) == 3){
	foreach($wheres as $where){
       $param = $where[0];
       $sinal = $where[1];
       $value = $where[0];	
     }
    }

return " where {$param} {$sinal} :{$value}";
}//montaWhere

public function montaWhereCustom($where){
 return " where $where";
}//montawherecustom


public function montaAnd($ands){
$operador = 'and';

if($this->search == '' && $this->where == ''){
  $operador = 'where';	
}	

$conta = COUNT($ands);
$saida = '';

if($conta > 1){

foreach($ands as $and){
$parametros = COUNT($and);	

if($parametros == 2){
 $param = $and[0];
 $sinal = '=';
 $value = $and[0];

 $saida .= " ".$operador." {$param} {$sinal} :{$value}";
 if($operador == 'where'){
 	$operador = 'and';
 }
}//quando tem 2 parametros

if($parametros == 3){	
 $param = $and[0];
 $sinal = $and[1];
 $value = $and[0];

 $saida .= " ".$operador." {$param} {$sinal} :{$value}";
 if($operador == 'where'){
 	$operador = 'and';
 }
}

}

return $saida;

}else{
//se teve apenas um AND	
if(count($ands[0]) == 2){
	foreach($ands as $and){
	   $param = $and[0];
	   $sinal = '=';
	   $value = $and[0];	
	 }
}

if(count($ands[0]) == 3){
		foreach($ands as $and){
	       $param = $and[0];
	       $sinal = $and[1];
	       $value = $and[0];	
	     }
}

return " and {$param} {$sinal} :{$value}";
}
}//montaAnd

public function montaOr($ors){

if(count($ors[0]) == 2){
 foreach($ors as $or){
	$param = $or[0];
	$sinal = '=';
	$value = $or[0];	
  }
}

if(count($ors[0]) == 3){
		foreach($ors as $or){
	       $param = $or[0];
	       $sinal = $or[1];
	       $value = $or[0];	
	     }
     }

	return " or {$param} {$sinal} :{$value}";

}//montaAnd
public function montaJoin($joins, $on = null){
$sql = null;

foreach($joins as $key => $join):

$sql .= " inner join {$join['0']}";

if(!is_null($on)){
	$sql .= " on ";
	$sql .= implode(" ", $on[$key])." ";
}

endforeach;

return $sql;

}//join


public function montaOrder($order){
  return " order by ".implode(" ", $order[0]);
}//montaOrder


public function montaGroup($group){
  return " group by {$group[0][0]}";
}//montaGroup

public function montaLimit($limit){
   return " limit {$limit[0][0]}";	
}//montalimit

public function montaSearch($search){
  if($search[0][1] != ''){	
    return " where ".$search[0][0]." LIKE :".$search[0][0];
  }
}//search


public function montaOrSearch($orsearch){
$operador = 'or';

if($this->search == ''){
  $operador = 'where';	
}	

$qr = '';

foreach ($orsearch as $key => $value) {	
 $qr .= " ".$operador." ".$value[0]." LIKE :".$value[0];
 if($operador == 'where'){
		$operador = 'or';
	}
}

return $qr;
}//orsearch



/********************************/
/*começando a parte de paginação*/
/********************************/
public function sqlToPaginate($perPage, $conditionsPaginate){
$this->selectPaginateBuilder = new SelectPaginateBuilder($conditionsPaginate);
$this->selectPaginateBuilder->perPage($perPage);

return $this->selectPaginateBuilder->sqlToPaginate();
}//sqlToPaginate

public function render($link){	
	return $this->selectPaginateBuilder->render($link);
}//render


}