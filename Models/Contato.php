<?php
namespace Models;
use \Core\Model;
use PDO;

class Contato extends Model{

public function getContato($id_contato){
$sql = "SELECT * FROM contato c, estado e, cidade ci WHERE 
c.id_estado = e.id_estado AND 
c.id_cidade = ci.id_cidade AND 
c.id_contato = :id";

$sql = $this->db->prepare($sql);
$sql->bindValue(":id", $id_contato);
$sql->execute();

return $sql->fetch();
}//listaPorIdPedido

}