<?php
namespace models;
use core\Model;
use Models\Ormbuilder;
use PDO;

class Selectpaginatebuilder extends Model{

private $maxLinks = 4;
private $conditionsPaginate;
private $perPage;
private $link;


public function __construct($conditionsPaginate){	
 parent::__construct();
 $this->conditionsPaginate = $conditionsPaginate; 
}//construtor


private function currentPage(){
$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);

if(isset($page) && $page >0){
	return $page;
}

return 1;

}//currentPage

public function perPage($perPage){	
  $this->perPage = $perPage[0];
}//perPage

public function offset(){		
 return ($this->currentPage() * $this->perPage) -$this->perPage;
}//offset

public function allRecords(){	
$ob = new Ormbuilder();	
$select = $this->conditionsPaginate->select;
$stmt = $this->db->prepare($select);


if(isset($this->conditionsPaginate->where) && !empty($this->conditionsPaginate->where)){ 	
  $stmt = $ob->whereBind($this->conditionsPaginate->where, $stmt);
}//where

if(isset($this->conditionsPaginate->search) && !empty($this->conditionsPaginate->search)){ 

  $stmt = $ob->searchBind($this->conditionsPaginate->search, $stmt);
}//search

if(isset($this->conditionsPaginate->orSearch) && !empty($this->conditionsPaginate->orSearch)){	
  $stmt = $ob->orSearchBind($this->conditionsPaginate->orSearch, $stmt);
}//search

$stmt->execute();

// echo $stmt->rowCount();exit;

return $stmt->rowCount();
}//allRecords

private function totalPages(){
 return ceil($this->allRecords() / $this->perPage);
}//totalPages


public function link(){
$uri = BASE_URL.$this->link;
$monta_search = '';

if($this->conditionsPaginate->isSearch == true){

if($_GET){
$param = $_GET;
array_shift($param);

foreach($param as $ind=>$item){
   $monta_search .= $ind."=".$item."&";
}
$monta_search = rtrim($monta_search, '&');
}

return $uri.'?'.$monta_search.'&page=';	
}

 return $uri.'?page=';
}//link


private function before(){
$links = '';

if($this->currentPage() != 1){

$before = ($this->currentPage() -1);

$links = '<li></a href="'.$this->link().'1"> [1] </a></li>';

$links .= '<li><a href="'.$this->link().$before.'" aria-label="Previous"> <span aria-hidden="true">&laquo;</span></a></li>';
}//se for diferente de 1

return $links;
}//before


private function next(){
$links = '';

if($this->currentPage() != $this->totalPages()){

	$next = ($this->currentPage() +1);

	$links = '<li><a href="'.$this->link().$next.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';

	$links .= '<li><a href="'.$this->link().$this->totalPages().'">['.$this->totalPages().']</a></li>';
}

return $links;
}//next


public function showLinks($i){
$class = ($i == $this->currentPage()) ? 'actual' : '';

if($i > 0 && $i <= $this->totalPages()){
	return "<li><a href='".$this->link().$i."' class=".$class.">{$i}</a></li>";
}

}//showLinks

public function render($link){
$this->link = $link;

if($this->totalPages() > 0){
	$links = "<ul class='pagination'>";

	$links.= $this->before();

	 for($i = $this->currentPage() - $this->maxLinks; $i <= $this->currentPage() + $this->maxLinks;$i++){
	 	$links .= $this->showLinks($i);
	 }//fim do for

	$links .= $this->next();

	$links .= "</ul>";	

	return $links;

}//fim do if que verifica se teve paginas maior que 0
}//render


public function sqlToPaginate(){	
return " limit {$this->perPage} offset {$this->offset()}";
}//sqlToPagina

}