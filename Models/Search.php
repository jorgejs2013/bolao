<?php
namespace Models;
use \Core\Model;
use PDO;

class Search extends Model{

public function buildGetFilterSql($filter){
$sqlFilter = array();

if(!empty($filter['nome'])){
	$sqlFilter[] = 'nome LIKE :nome';
}

if(!empty($filter['nome_turma'])){
	$sqlFilter[] = 'nome_turma LIKE :nome_turma';
}

if(!empty($filter['produto'])){
	$sqlFilter[] = 'produto LIKE :produto';
}

if(!empty($filter['categoria'])){
	$sqlFilter[] = 'id_categoria LIKE :categoria';
}

if(!empty($filter['turno'])){
	$sqlFilter[] = 'turno LIKE :turno';
}

if(!empty($filter['id_curso'])){
	$sqlFilter[] = 'id_curso LIKE :id_curso';
}

if(!empty($filter['id_serie'])){
	$sqlFilter[] = 'id_serie LIKE :id_serie';
}

return $sqlFilter;
}//buildGetFilterSql

//& significa que tudo que fizer dentro tem ação fora
public function buildGetFilterBind($filter, &$sql){		
if(!empty($filter['nome'])){
	$sql->bindValue(':nome', '%'.$filter['nome'].'%');
}

if(!empty($filter['nome_turma'])){
	$sql->bindValue(':nome_turma', '%'.$filter['nome_turma'].'%');
}

if(!empty($filter['produto'])){
	$sql->bindValue(':produto', '%'.$filter['produto'].'%');
}


if(!empty($filter['categoria'])){	
	$sql->bindValue(':categoria', '%'.$filter['categoria'].'%');	
}

if(!empty($filter['turno'])){	
	$sql->bindValue(':turno', '%'.$filter['turno'].'%');	
}

if(!empty($filter['id_curso'])){	
	$sql->bindValue(':id_curso', '%'.$filter['id_curso'].'%');	
}

if(!empty($filter['id_serie'])){	
	$sql->bindValue(':id_serie', '%'.$filter['id_serie'].'%');	
}

}//buildGetFilterBind


public function getAllSearch($tabela, $filter = array(), $cond =  null){
$array = array();

$sqlfilter = $this->buildGetFilterSql($filter);

$sql = "SELECT * FROM {$tabela}";
if($cond != null){
	$sql .= " $cond";
}


if(count($sqlfilter) >0){
	$sql .= " WHERE ".implode(' AND ', $sqlfilter);
}

//$sql .= " ORDER BY id DESC";

$sql = $this->db->prepare($sql);

$this->buildGetFilterBind($filter, $sql);

$sql->execute();

if($sql->rowCount() >0){
	$array = $sql->fetchAll();
}

return $array;
}//getSearchAll

}