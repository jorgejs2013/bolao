<?php
namespace models;
use core\Model;
use PDO;

class Ormbuilder{

public function selectCamposTabela($select, $tabela){
$sql = null;

if(!is_array($select)){
	$sql = $select;
}else{

	foreach ($select as $field) 
		$sql .= $field.',';	
}

$fields = rtrim($sql, ',');
return "select {$fields} from {$tabela}";
}//selectCamposTabela


public function whereBind($where, $select){

if(is_array($where) && !empty($where)){

//se teve mais de um array
if(count($where) > 1){

foreach($where as $key => $sql):

 $param = $this->ambiguous($sql[0]); 
 $value = $this->ambiguous($sql[1]); 
 $select->bindValue(":{$param}", $value);

endforeach;

return $select;
}


foreach($where as $sql){
 $param = $this->ambiguous($sql[0]);
 $value = $this->ambiguous($sql[1]); 
 $param = $sql[0];
}

if(count($where[0]) == 3){

 foreach($where as $sql):	
	 $param = $sql[0];
	 $value = $this->ambiguous($sql[2]);
     $param = $sql[0];
 endforeach; 
}

$select->bindValue(":{$param}", $value);
return $select;

}else{
	return '';
}

}//wherebind

public function andBind($and, $select){
$conta = COUNT($and);

if($conta > 1){

foreach($and as $sql){	
 $param = $sql[0]; 
 $value = $this->ambiguous($sql[2]); 
 $select->bindValue(":{$param}", $value);
}

return $select;
}else{

//quando é passado apenas um AND
foreach($and as $sql){
 
 $param = $sql[0]; 
 $value = $this->ambiguous($sql[1]);
}

$select->bindValue(":{$param}", $value);

return $select;
}

}//andBind

public function orBind($or, $select){

foreach($or as $sql){
 $param = $sql[0];
 $value = $sql[1]; 
 $param = $sql[0];
}

if(count($or[0]) == 3){

 foreach($or as $sql):	
	 $param = $sql[0];
	 $value = $this->ambiguous($sql[2]);

$param = $sql[0];
 endforeach; 
}

$select->bindValue(":{$param}", $value);
return $select;
}//orBind

public function searchBind($search, $select){
//definindo o campo que quer buscar ou filter_input(INPUT_GET, $search[0][1], FILTER_SANITIZE_STRING);	
$searchFiltered = $search[0][1];

$select->bindValue(":{$search[0][0]}", "%" .$searchFiltered. "%");

return $select;
}//searchBind


public function orSearchBind($orSearch, $select){
if(!empty($orSearch)){
$searchFiltered = $orSearch[0][1];

for($i = 0; $i < COUNT($orSearch); $i++):	
  $select->bindValue(":{$orSearch[$i][0]}", "%" .$orSearch[$i][1]. "%");
endfor;

return $select;
}
return $select;
}//orSearch


private  function ambiguous($sql){	
   if(substr_count($sql, '.') > 0){
      $sql = str_replace('.', '', $sql);
    }  

    return $sql;
}//ambiguous

}