<?php
namespace Models;
use Core\Model;
use Models\Crud;
use Models\Faturas;
use PDO;

Class Retorno extends Model{

private $dados = array();
	
public function processar($SetArquivo){
$array = array();
$array = file($SetArquivo);

$msg = array();


$header = $this->processarHeader($array[0]);

for($i = 1;$i< count($array); $i++){	
	
$this->dados['detalhe']  = $this->processarDetalhe($array[$i]);
$this->dados['trillher'] = $this->processarTrailer($array[$i]);		
 
$id_ocorrencia = $this->dados['detalhe']['id_ocorrencia']; 
$num_documento = $this->dados['detalhe']['num_documento'];
$motivo_rejeicoes = $this->dados['detalhe']['motivo_rejeicoes'];
$valor_titulo =  $this->dados['detalhe']['valor_titulo'];
$nosso_numero = $this->dados['detalhe']['nosso_numero'];
$valor_recebido = $this->dados['detalhe']['valor_recebido'];

$rejeicoes =  substr($motivo_rejeicoes, 0 ,2);
$split_nosso_numero = substr($nosso_numero, 0, 11);

//numero da fatura para atualizar no banco caso for pago
$num_fatura = (int) $split_nosso_numero;

if($id_ocorrencia == '06' OR $id_ocorrencia == '17'){	
	if($motivo_rejeicoes == '00'){
 
 $msg[$i] = 'Linha '.$i.' - Liquidacao Titulo Pago - '.$motivo_rejeicoes.' - '.$split_nosso_numero.' - '.$valor_recebido.PHP_EOL;

//comando sql para atualizar boleto
$sql = "UPDATE fatura_parcelas SET status_pgto = 'S' WHERE id_fatura_parcela = '$num_fatura'";
$sql = $this->db->query($sql);

	}else{
     $msg[$i] = 'Linha '.$i.' - Cod. ( '.$rejeicoes.' ) ainda nao cadastrado - informar suporte'.' - '.$split_nosso_numero."<br>";
   };
}//se id da ocorrencia for = 06 ou 17 e motivo da rejeição for == 00 

if($id_ocorrencia == '02'){

  if($rejeicoes == '00'){
	$msg[$i] = 'Linha '.$i.' - Confirmacao de Entrada de Boleto - '.$motivo_rejeicoes.' - '.$split_nosso_numero."<br>";
  }else if($rejeicoes == '76'){
  	$msg[$i] = 'Linha '.$i.' - Confirmacao de Entrada de Boleto - Pagador Eletronico DDA - '.$motivo_rejeicoes.' - '.$split_nosso_numero."<br>";
  }else{
  	 $msg[$i] = 'Linha '.$i.' - Cod. ( '.$motivo_rejeicoes.' ) ainda nao cadastrado - informar suporte'.' - '.$split_nosso_numero."<br>";
  }
}//se id da ocorrencia == 02


if($id_ocorrencia == '03'){

if($rejeicoes == '46'){
$msg[$i] = 'Linha '.$i.' - Comando Recusado ( Tipo/num de inscricao do Pagador invalidos )'.' - '.$split_nosso_numero."<br>";
}

if($rejeicoes == '71'){
$msg[$i] = 'Linha '.$i.' - Comando Recusado ( Debito nao agendado - Beneficiario nao parcipa do debito automatico )'.' - '.$split_nosso_numero."<br>";
}

if($rejeicoes == '74'){
$msg[$i] = 'Linha '.$i.' - Comando Recusado ( Debito nao agendado - Conforme seu pedido titulo nao registrado )'.' - '.$split_nosso_numero."<br>";
}

if($rejeicoes != '00' && $rejeicoes != '46' && $rejeicoes != '71' && $rejeicoes != '74'){
$msg[$i] = 'Linha '.$i.' - Cod. ( '.$rejeicoes.' ) ainda nao cadastrado - informar suporte'.' - '.$split_nosso_numero."<br>";
}

}//se id da ocorrencia == 03

}


return $msg;
//$objeto = fopen($SetArquivo, "r");
//ini_set('auto_detect_line_endings', TRUE);
//Comando feof: Indica ao comando while quando chegou ao final do arquivo;
//while(!feof($objeto)){

 
}//metodo processar
	
public function formataValor($set){
//Formata valor em real
$valor = self::limpaCaracteres($set);
$valor = ltrim($valor, "0");
$valor = $valor / 100;
$valor = number_format($valor, 2, ',', '.');
return $valor;
}
	
public function limpaCaracteres($set){
//Limpa caracteres especiais
$caracter = str_replace('.','',$set);
$caracter = str_replace(',','',$caracter);
return $caracter;
}
	
public function formataData($set){
 	return date('d/m/Y',strtotime($set));
}


public function formataNumero($valor, $numCasasDecimais = 2) {
if ($valor == "") {
    return 0;
}
        
$casas = $numCasasDecimais;
if ($casas > 0) {
    $valor = substr($valor, 0, strlen($valor) - $casas) . "." . substr($valor, strlen($valor) - $casas, $casas);
    return (float)$valor;
}
        
   return (int)$valor;
}//formataNumero


private function processarHeader($linha) {    
$vetor = array();
//X = ALFANUMÉRICO 9 = NUMÉRICO V = VÍRGULA DECIMAL ASSUMIDA
$vetor['id_registro'] = substr($linha, 0, 1);
$vetor["id_retorno"] = substr($linha, 1, 1); //9 Identificação do Registro Header: “0”
$vetor["literal_retorno"] = substr($linha, 2, 7); //X Identificação Tipo de Operação “RETORNO”
$vetor["cod_servico"] = substr($linha, 9, 2); //9 Identificação do Tipo de Serviço: “01”
$vetor["literal_servico"] = substr($linha, 11, 15); //X Identificação por Extenso do Tipo de Serviço: “COBRANCA”
$vetor["cod_empresa"] = substr($linha, 26, 20);
$vetor["nome_empresa"] = substr($linha, 46, 30); //razao social
$vetor["num_banco"] = substr($linha, 76, 3); //237 (Código do bradesco)
$vetor["nome_banco"] = substr($linha, 79, 15); //Nome do banco (BRADESCO)
$vetor["data_gravacao"] = $this->formataData(substr($linha, 94, 6)); //9 Data da Gravação: Informe no formado “DDMMAA”
$vetor["densidade_gravacao"] = substr($linha, 100, 8); //01600000 
$vetor["num_aviso_bancario"] = substr($linha, 108, 5);// esse né
$vetor["data_credito"] = $this->formataData(substr($linha, 379, 6)); // “DDMMAA”
$vetor["sequencial_reg"] = substr($linha, 394, 6); //9 Seqüencial do Registro: ”000001”

   return $vetor;
}//processarHeaderArquivo

private function processarDetalhe($linha) {
$vetor = array();
//X = ALFANUMÉRICO 9 = NUMÉRICO V = VÍRGULA DECIMAL ASSUMIDA
$vetor["registro"] = substr($linha, 0, 1);  //9  Id do Registro Detalhe: 1 
$vetor["tipo_inscr_empresa"] = substr($linha, 1, 2);  //9  01-CPF | 02-CNPJ | 03-PIS/PASEP | 98-Não tem | 99-Outro
$vetor["num_inscr_empresa"] = substr($linha, 3, 14);  //9  CNPJ/CPF, Número, Filial ou Controle
$vetor["id_empresa_banco"] = substr($linha, 20, 17); //9  Identificação da Empresa Cedente no Banco
//Zero, Carteira (size=3), Agência (size=5) e Conta Corrente (size=8)
$vetor["num_controle_part"] = substr($linha, 37, 25); //No Controle do Participante | Uso da Empresa 
$vetor["nosso_numero"] = substr($linha, 70, 12); //Identificação do Título no Banco
$vetor["id_rateio_credito"] = substr($linha, 104, 1); //Indicador de Rateio Crédito “R” 
$vetor["carteira"] = substr($linha, 107, 1);  //Carteira
$vetor["id_ocorrencia"] = substr($linha, 108, 2);  //Identificação de Ocorrência (vide pg 47)
$vetor["data_pagamento"] = $this->formataData(substr($linha, 110, 6)); //X  data_ocorrencia = Data da Entrada/Liquidação (DDMMAA)        
$vetor["num_documento"] = substr($linha, 116, 10);  //A  Número título dado pelo cedente
$vetor["id_titulo_banco"]   = substr($linha, 126,  20);  //mesmo valor que o campo nosso_numero (indicado anteriormente)
$vetor["data_vencimento"] = $this->formataData(substr($linha, 146, 6));  //9  Data de vencimento (DDMMAA) 
$vetor["valor_titulo"] = $this->formataNumero(substr($linha, 152, 13)); //9  v99 Valor do título
$vetor["cod_banco"] = substr($linha, 165, 3);  //9  Código do banco recebedor 
$vetor["agencia"] = substr($linha, 168, 5);  //9  Código da agência recebedora 
$vetor["desp_cobranca"] = $this->formataNumero(substr($linha, 175, 13)); // Despesas de cobrança para
        //os Códigos de Ocorrência 
        //02 - Entrada Confirmada 
        //28 - Débito de Tarifas

$vetor["outras_despesas"] = $this->formataNumero(substr($linha, 188, 13)); //9  v99 Outras despesas
$vetor["juros_atraso"] = $this->formataNumero(substr($linha, 201, 13)); //9  v99 Juros atraso
$vetor["iof"] = $this->formataNumero(substr($linha, 214, 13)); //9  v99 IOF 
$vetor["abatimento_concedido"] = $this->formataNumero(substr($linha, 227, 13)); 
$vetor["desconto_concedido"] = $this->formataNumero(substr($linha, 240, 13)); //9  v99 Desconto concedido 
$vetor["valor_recebido"] = $this->formataNumero(substr($linha, 253, 13)); //9  v99 Valor pago
$vetor["juros_mora"] = $this->formataNumero(substr($linha, 266, 13)); //9  v99 Juros de mora
$vetor["outros_recebimentos"] = $this->formataNumero(substr($linha, 279, 13)); //9  v99 Outros recebimentos

$vetor["motivo_cod_ocorrencia"] = substr($linha, 294, 1);  //Motivos das Rejeições para 
//os Códigos de Ocorrência da Posição 109 a 110 
$vetor["data_do_credito"] = substr_compare($linha, 265, 6);//ddmmaa
$vetor["origem_pagamento"] = substr($linha,  301, 3);
$vetor["quando_cheque_bradesco"] = substr($linha, 314, 4);
$vetor["motivo_rejeicoes"] = substr($linha, 318, 10);
$vetor["num_cartorio"] = substr($linha, 368, 2);
$vetor["num_protocolo"] = substr($linha, 370, 10);

// $vetor["valor_abatimento"] = $this->formataNumero(substr($linha, 227, 13)); //9  v99 Valor do abatimento
// $vetor["abatimento_nao_aprov"] = $this->formataNumero(substr($linha, 292, 13)); //9  v99 Abatimento não aproveitado pelo sacado
// $vetor["valor_pagamento"] = $this->formataNumero(substr($linha, 305, 13)); //9  v99 Valor do lançamento
// $vetor["indicativo_dc"] = substr($linha, 318, 1); //9  Indicativo de débito/crédito - ver nota 11
// $vetor["indicador_valor"] = substr($linha, 319, 1); //9  Indicador de valor -ver  nota 12
// $vetor["valor_ajuste"] = $this->formataNumero(substr($linha, 320, 12)); //9  v99 Valor do ajuste - ver nota 13

$vetor["sequencial"] = substr($linha, 394, 6); //9 Seqüencial do registro
   return $vetor;
}//processarDetalhe


private function processarTrailer($linha) {
       
$vetor = array();
//X = ALFANUMÉRICO 9 = NUMÉRICO V = VÍRGULA DECIMAL ASSUMIDA
$vetor["registro"] = substr($linha, 1, 1);  //9  Identificação do Registro Trailer: “9”
$vetor["retorno"] = substr($linha, 1, 1);  //9  “2”
$vetor["tipo_registro"] = substr($linha, 2, 2);  //9  “01”
$vetor["cod_banco"] = substr($linha, 4, 3);
$vetor["cob_simples_qtd_titulos"] = substr($linha, 17, 8);  //9  Cobrança Simples - quantidade de títulos em cobranca
$vetor["cob_simples_vlr_total"] = $this->formataNumero(substr($linha, 25, 14)); //9  v99 Cobrança Simples - valor total
$vetor["cob_simples_num_aviso"] = substr($linha, 39, 8);  //9  Cobrança Simples - Número do aviso
$vetor["qtd_regs02"] = substr($linha, 57, 5);  //Quantidade  de Registros- Ocorrência 02 – Confirmação de Entradas
$vetor["valor_regs02"] = $this->formataNumero(substr($linha, 62, 12)); //Valor dos Registros- Ocorrência 02 – Confirmação de Entradas
$vetor["valor_regs06liq"] = $this->formataNumero(substr($linha, 74, 12)); //Valor dos Registros- Ocorrência 06 liquidacao
$vetor["qtd_regs06"] = substr($linha, 86, 5);  //Quantidade  de Registros- Ocorrência 06 – liquidacao
$vetor["valor_regs06"] = $this->formataNumero(substr($linha, 91, 12)); //Valor dos Registros- Ocorrência 06
$vetor["qtd_regs09"] = substr($linha, 103, 5);  //Quantidade  de Registros- Ocorrência 09 e 10
$vetor["valor_regs02"] = $this->formataNumero(substr($linha, 108, 12)); //Valor dos  Registros- Ocorrência 09 e 10
$vetor["qtd_regs13"] = substr($linha, 120, 5);  //Quantidade  de Registros- Ocorrência 13
$vetor["valor_regs13"] = $this->formataNumero(substr($linha, 125, 12)); //Valor dos  Registros- Ocorrência 13
$vetor["qtd_regs14"] = substr($linha, 137, 5);  //Quantidade  de Registros- Ocorrência 14
$vetor["valor_regs14"] = $this->formataNumero(substr($linha, 142, 12)); //Valor dos  Registros- Ocorrência 14
$vetor["qtd_regs12"] = substr($linha, 154, 5);  //Quantidade  de Registros- Ocorrência 12
$vetor["valor_regs12"] = $this->formataNumero(substr($linha, 159, 12)); //Valor dos  Registros- Ocorrência 12
$vetor["qtd_regs19"] = substr($linha, 171, 5);  //Quantidade  de Registros- Ocorrência 19
$vetor["valor_regs19"] = $this->formataNumero(substr($linha, 176, 12)); //Valor dos  Registros- Ocorrência 19
$vetor["valor_total_rateios"] = $this->formataNumero(substr($linha, 362, 15));
$vetor["qtd_rateios"] = substr($linha, 377, 8);
$vetor["sequencial"] = substr($linha, 394, 6);  //9  Seqüencial do registro

return $vetor;
}//processarTrailerArquivo

}//fim da classe

## EXEMPLO DE USO
//new Retorno('472950809.CRT');