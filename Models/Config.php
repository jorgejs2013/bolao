<?php
namespace Models;
use \Core\Model;
use PDO;

class Config extends Model{

public function lista(){
$sql = "SELECT * FROM config";
$qry = $this->db->query($sql);

return $qry->fetchAll();
}//lista

public function getConfig($id){
$sql = "SELECT * FROM config WHERE id_config = 1";
$sql = $this->db->query($sql);

$resultado = $sql->fetch();
$_SESSION['config'] = $resultado;
return $resultado;
}//getConfig


public function mudarLayout($layout){
$layout = "tema-".$layout;
$sql = "UPDATE config SET layout = '$layout' WHERE id_config = 1";
$sql = $this->db->query($sql);
$this->getConfig();
}//mudarLayout

public function atualizaNnfe(){
$sql = "UPDATE config SET ultimanfe = ultimanfe +1 WHERE id_config = 1";
$sql = $this->db->query($sql);
}//atualizaNnfe

public function getProximoNFE(){
$sql = "SELECT ultimanfe FROM config WHERE id_config = 1";
$qry = $this->db->query($sql);

$codigo = $qry->fetch()->ultimanfe;

//$ultima = $codigo['ultimanfe'] +1;

return str_pad($codigo + 1, 8, '0', STR_PAD_LEFT);
}//getProximoNFe

public function backup(){
$array = array();
$sql = "SHOW TABLES";
$sql = $this->db->query($sql);

return $sql->fetchAll();	
}//backup

public function gerarBackup($tabelas){

$saida = '';

  foreach($tabelas as $tabela){
    $mostrar_query_tabela = "SHOW CREATE TABLE ".$tabela. "";
    $statement = $this->db->query($mostrar_query_tabela);
    $resultado_tabela = $statement->fetchAll(PDO::FETCH_ASSOC);

    foreach($resultado_tabela as $mostrar_linha_tabela){    	
      $saida .= "\n\n".$mostrar_linha_tabela["Create Table"]. ";\n\n";
    }

    $eleciona_query = "SELECT * FROM ".$tabela. "";
    $statement = $this->db->query($eleciona_query);
    $total_linhas = $statement->rowCount();

    for($count = 0; $count < $total_linhas; $count++){
      $single_result = $statement->fetch(PDO::FETCH_ASSOC);
      $tabela_coluna_array = array_keys($single_result);
      $tabela_valores_array = array_values($single_result);
      $saida .= "\nINSERT INTO $tabela (";
      $saida .= "".implode(", ", $tabela_coluna_array). ") VALUES(";
      $saida .= "'".implode("','", $tabela_valores_array). "');\n";
    }

  }

  return trim( $saida ); 
}//gerarBackup

}