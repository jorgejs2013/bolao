<?php
namespace Models;
use Core\Model;
use PDO;

class Rifa extends Model {


public function search($sorteio, $cambista = null, $status = null, $data_ini = null, $data_fim = null){
if($data_ini != '')
	$data_ini = date($data_ini." H:i:s");

if($data_fim != '')
	$data_fim = date($data_fim." H:i:s");

$sql = "SELECT * FROM rifa 
INNER JOIN rifa_reserva
ON rifa.id_rifa = rifa_reserva.id_rifa
INNER JOIN usuario
ON rifa_reserva.indicacao = usuario.slug_nome
WHERE rifa.id_rifa = {$sorteio}
";

if($cambista != null){
	$sql .= " AND rifa_reserva.indicacao = '{$cambista}'";
}

if($status != null){
	$sql .= " AND rifa_reserva.status_pag = {$status}";
}

if($data_ini != null && $data_fim == null){
	$sql .= " AND data_reserva >= '{$data_ini}'";
}else if($data_ini == null && $data_fim != null){
    $sql .= " AND data_reserva <= '{$data_fim}'";
}else if($data_ini != null && $data_fim != null){
   $sql .= " AND data_reserva >= '{$data_ini}' AND data_reserva <= '{$data_fim}'";
}

$sql = $this->db->query($sql);
$result = $sql->fetchAll();

return $result;
}//search

}