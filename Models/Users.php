<?php
namespace Models;
use Core\Model;
use Models\Permissao;
use PDO;

/** Classe para Usuários */
class Users extends Model {

	/**Guarda os dados(informação) do usuário*/
	private $info;
	private $uid;	
	private $userName;
	private $slugNome;
	private $userImage;
	private $isAdmin;
	private $getCompany;

public function isLogged(){

if(!empty($_SESSION['token'])){
	$token = $_SESSION['token'];
	$sql = "SELECT codigo, nome, senha,status FROM usuarios WHERE codigo = :token";
	$sql = $this->db->prepare($sql);
	$sql->bindValue(":token", $token);
	$sql->execute();

	if($sql->rowCount() >0){
       $p = new Permissao();

		$data = $sql->fetch();
		$this->uid = $data->codigo;
		$this->userName = $data->nome;				
		return true;
	}
}

return false;

}//isLogged

public function validateLogin($email, $senha){	
$sql = "SELECT codigo, senha FROM usuarios WHERE nome = :nome AND senha = :senha ";

$sql = $this->db->prepare($sql);
$sql->bindValue(':nome', $email);
$sql->bindValue(':senha', $senha);
$sql->execute();

if($sql->rowCount() >0){
 $data = $sql->fetch();

  $_SESSION['token'] = $data->codigo;
  return true; 
}
return false;
}//validateLogin


public function getName(){
   return $this->userName;
}//getName

public function getSlugNome(){
   return $this->slugNome;
}//getName

public function getCompany(){
   return $this->getCompany;
}//getCompany


public function isAdmin(){
	
	if($this->isAdmin == '1'){
     return true;
	}else{
		return false;
	}

}//isAdmin

public function getImage(){
return $this->userImage;
}//getImage


public function getId(){
 return $this->uid;
}//getId


public function userExists($u){

$sql = "SELECT * FROM usuarios WHERE nome = :u";
$sql = $this->db->prepare($sql);
$sql->bindValue(":u",$u);
$sql->execute();

if($sql->rowCount() >0){
   return true;
}else{
   return false;
}

}//userExists

public function setLoggedUser(){
if(isset($_SESSION['user']) && !empty($_SESSION['user'])){
 $id = $_SESSION['user'];

 $sql = $this->db->prepare("SELECT * FROM usuario WHERE email = :email");
 $sql->bindValue(":email", $id);
 $sql->execute();

 if($sql->rowCount()>0){
  $this->userInfo = $sql->fetch();
  $this->permissions = new Permissions();
  $this->permissions->setGroup($this->userInfo['id_group'], $this->userInfo['id_company']);
 }
}
}//function setLoggedUser

public function getInfo($id){
 $array = array();

$sql = $this->db->prepare("SELECT * FROM usuarios WHERE nome = :email ");
$sql->bindValue(':email', $id);
$sql->execute();

if($sql->rowCount()>0){
	$array = $sql->fetch();
}

  return $array;
}// function getInfo


}