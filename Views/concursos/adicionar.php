<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>concursos"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
<form method="post" id="form_operador" class="form_box" action="<?php echo BASE_URL;?>concursos/add_action">

   
    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper-->   

        <div class="input-wrapper w50">
      <span>Data:</span>
      <input type="text" name="data" class="date calendario" data-date-format="d/m/Y" value="<?php echo date('d/m/Y');?>" />
    </div><!-- input wrapper--> 

      <div class="input-wrapper w50">
      <span>Data Limite:</span>
      <input type="text" name="data_limite" data-date-format="d/m/Y" class="date calendario" />
    </div><!-- input wrapper-->  

      <div class="input-wrapper w50">
      <span>Hora Limite:</span>
      <input type="text" name="hora_limite" class="horario" />
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Valor aposta:</span>
      <input type="text" name="valor_aposta" class="money" placeholder="Não pode ser igual a 0" />
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Valor acumulado:</span>
      <input type="text" name="valor_acumulado" class="money" />
    </div><!-- input wrapper-->    

      <div class="input-wrapper w50">
      <span>Taxa administração:</span>
      <input type="text" name="taxa_administracao" class="money"  value="0" />
    </div><!-- input wrapper--> 

      <div class="input-wrapper w50">
      <span>Comissão vendedor:</span>
      <input type="text" name="comissao_vendedor" class="money" value="0" />
    </div><!-- input wrapper-->    

     

    <div class="input-wrapper w100">
      <span>Observação:</span>
     <textarea name="observacao"></textarea>
    </div><!-- input wrapper--> 
  
 
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_operador').parsley();
 }); 
</script>