<?php
$nome = (isset($info->NOME)) ? $info->NOME: '';
$data = (isset($info->DATA)) ? $info->DATA: '';
$data_limite = (isset($info->DATA_LIMITE)) ? $info->DATA_LIMITE: '';
$hora_limite = (isset($info->HORA_LIMITE)) ? $info->HORA_LIMITE: '';
$valor_aposta = (isset($info->VALOR_APOSTA)) ? $info->VALOR_APOSTA: '';
$valor_acumulado = (isset($info->VALOR_ACUMULADO)) ? $info->VALOR_ACUMULADO: '';
$taxa_administracao = (isset($info->TAXA_ADMINISTRACAO)) ? $info->TAXA_ADMINISTRACAO: '';
$comissao_vendedor = (isset($info->COMISSAO_VENDEDOR)) ? $info->COMISSAO_VENDEDOR: '';
$observacao = (isset($info->OBSERVACAO)) ? $info->OBSERVACAO: '';

?>
<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Editar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>concursos"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
<form method="post" id="form_concurso" class="form_box" action="<?php echo BASE_URL;?>concursos/edit_action/<?php echo $id_concurso;?>">

   
    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true" value="<?php echo $nome;?>" />
    </div><!-- input wrapper-->   

        <div class="input-wrapper w50">
      <span>Data:</span>
      <input type="text" name="data" class="calendario" data-date-format="d/m/Y" value="<?php echo date('d/m/Y', strtotime($data));?>" />
    </div><!-- input wrapper--> 

      <div class="input-wrapper w50">
      <span>Data Limite:</span>
      <input type="text" name="data_limite" class="calendario" data-date-format="d/m/Y" value="<?php echo date('d-m-Y', strtotime($data_limite));?>" />
    </div><!-- input wrapper-->  

      <div class="input-wrapper w50">
      <span>Hora Limite:</span>
      <input type="text" name="hora_limite" class="horario" value="<?php echo date('H:i', strtotime($hora_limite));?>" />
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Valor aposta:</span>
      <input type="text" name="valor_aposta" class="money" value="<?php echo $valor_aposta;?>" />
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Valor acumulado:</span>
      <input type="text" name="valor_acumulado" class="money" value="<?php echo $valor_acumulado;?>" />
    </div><!-- input wrapper-->    

      <div class="input-wrapper w50">
      <span>Taxa administração:</span>
      <input type="text" name="taxa_administracao" class="money" value="<?php echo $taxa_administracao;?>" />
    </div><!-- input wrapper--> 

      <div class="input-wrapper w50">
      <span>Comissão vendedor:</span>
      <input type="text" name="comissao_vendedor" class="money" value="<?php echo $comissao_vendedor;?>" />
    </div><!-- input wrapper-->    

     

    <div class="input-wrapper w100">
      <span>Observação:</span>
     <textarea name="observacao"><?php echo $observacao;?></textarea>
    </div><!-- input wrapper--> 
  
 
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_concurso').parsley();
 }); 
</script>


<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
});
});
</script>
<?php }?>