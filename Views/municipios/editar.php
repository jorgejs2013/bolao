<?php
$nome = (isset($info->NOME)) ? $info->NOME: '';
$uf = (isset($info->UF)) ? $info->UF: '';
$sef = (isset($info->SEF)) ? $info->SEF: '';
$srf = (isset($info->SRF)) ? $info->SRF: '';
$ibge = (isset($info->IBGE)) ? $info->IBGE: '';
?>

<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>municipios"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
<form method="post" id="form_municipio" class="form_box" action="<?php echo BASE_URL;?>municipios/edit_action/<?php echo $id_municipio;?>">

   
    <div class="input-wrapper w100">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true" value="<?php echo $nome;?>" />
    </div><!-- input wrapper-->     

     <div class="input-wrapper w50">
      <span>Uf:</span>
      <input type="text" name="uf" value="<?php echo $uf;?>"/>
    </div><!-- input wrapper-->    

    <div class="input-wrapper w50">
      <span>Sef:</span>
      <input type="text" name="sef" value="<?php echo $sef;?>"/>
    </div><!-- input wrapper-->   

      <div class="input-wrapper w50">
      <span>Srf:</span>
      <input type="text" name="srf" value="<?php echo $srf;?>"/>
    </div><!-- input wrapper--> 

      <div class="input-wrapper w50">
      <span>Ibge:</span>
      <input type="text" name="ibge" value="<?php echo $ibge;?>"/>
    </div><!-- input wrapper-->      
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->

<script>
 $(document).ready(function(){   
   $('#form_municipio').parsley();
 }); 
</script>