<?php
$cod_banco = (isset($info->cod_banco)) ? $info->cod_banco: '';
$nome_banco = (isset($info->nome_banco)) ? $info->nome_banco: '';
$agencia = (isset($info->agencia)) ? $info->agencia: '';
$conta = (isset($info->conta)) ? $info->conta: '';
$tipo_conta = (isset($info->tipo_conta)) ? $info->tipo_conta: '';
$titular = (isset($info->titular)) ? $info->titular: '';
$img_bandeira = (isset($info->img_bandeira)) ? $info->img_bandeira: '';
$cpf = (isset($info->cpf)) ? $info->cpf: '';
?>

<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Editar</h3>
  <span class="min_box"><a href="<?php echo BASE_URL;?>contas"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_conta" class="form_box">

    <div class="input-wrapper w30">
      <span>Agencia:</span>
      <input type="text" name="agencia" placeholder="Informe o numero da agencia" value="<?php echo $agencia;?>" />
    </div><!-- input wrapper--> 

     <div class="input-wrapper w30">
      <span>Conta :</span>
      <input type="text" name="conta" placeholder="Informe o numero da conta" value="<?php echo $conta;?>" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>Nome do banco :</span>
      <input type="text" name="nome_banco" placeholder="Informe o nome do banco" value="<?php echo $nome_banco;?>" />
    </div><!-- input wrapper-->       

    <div class="input-wrapper w100" style="margin: 15px 0;">
     <div class="drop-zone">
       <span class="drop-zone__prompt">Arraste a imagem ou clique para fazer upload</span>  
       <input type="file" name="myFile" class="drop-zone__input" value="" />
       <input type="hidden" name="foto" class="fotobase64" value=""/>
     </div>
    </div><!-- input wrapper--> 
   
    <div class="input-wrapper w50">
      <span>Cód banco:</span>
      <input type="text" name="cod_banco" value="<?php echo $cod_banco;?>" />
    </div><!-- input wrapper-->     

    <div class="input-wrapper w50">
      <span>Tipo da Conta :</span>
      <input type="text" name="tipo_conta" placeholder="Ex. corrente, poupança" value="<?php echo $tipo_conta;?>" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Titular da conta:</span>
      <input type="text" name="titular"  value="<?php echo $titular;?>" />
    </div><!-- input wrapper--> 

     <div class="input-wrapper w50">
      <span>Cpf do titular :</span>
      <input type="text" name="cpf" class="cpf" placeholder="Apenas numeros" value="<?php echo $cpf;?>" />
    </div><!-- input wrapper-->  


    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Atualizar" />     
     </div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->