<section class="content_page">	
<h2>Contas</h2>

<div class="btn_topo">
<a href="<?php echo BASE_URL;?>contas/add" class="add_pagina">
  <button type="button" class="btn" id="adicionar"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</button>
</a>

</div><!-- btn topo-->

<div class="filtros_forma">
<div class="box_filtro">

<div class="box_filtro_titulo">
<h3>Buscar por conta</h3>
</div><!-- box filtro titulo -->

<div class="box_form_content">
	<form method="post" class="form_box">
      
    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper--> 
    

<div class="input-wrapper w50 singleButton">
<input type="submit" name="submit" class="btn" value="Pesquisar" />
</div><!-- input wrapper--> 
     
	</form>
</div><!--box form content -->

</div><!-- box filtro-->
</div><!-- filtros_forma -->

<div class="container_shrinker">
<table class="table shrink">
<thead>
<tr>
	<th>Imagem</th>
	<th class="shrink-xs">Agencia</th>
	<th class="shrink-xs">Conta</th>
	<th class="shrink-xs">Titular</th>
	<th class="shrink-xs">Ação</th>	
</tr>	
</thead>	

<tbody>
<?php
if(!empty($lista_contas)): 
foreach ($lista_contas as $conta): ?>
<tr>
<td><img src="<?php echo BASE_URL;?>medias/<?php echo $conta->img_bandeira;?>" width="50" height="50"/></td>	
<td><?php echo $conta->agencia;?></td>	
<td><?php echo $conta->conta;?></td>
<td><?php echo $conta->titular;?></td>	
<td>
<a href="<?php echo BASE_URL;?>contas/edit/<?php echo $conta->id_conta;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  | 
<a href="<?php echo BASE_URL;?>contas/del/<?php echo $conta->id_conta;?>"><i class="fa fa-trash" aria-hidden="true"></i></a>	
</td>
</tr>

<?php endforeach;
endif;
?>
		
</tbody>
</table>
</div><!-- table listagem-->

</section><!-- content page -->


<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {    
    window.location.href = base_url+"contas";   
     
  }
});
}) 
</script>
<?php }?>