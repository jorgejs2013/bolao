<aside>

<form method="post" action="<?php echo BASE_URL;?>usuarioTabela/inserir">	
<label for="id_tabela" style="text-align: left;float: left;font-weight: bold;">Tabela:</label>
<select name="id_tabela" style="width: 100%;height: 30px;margin-bottom: 5px;">
<option value="0" selected="selected" disabled="disabled">Selecione a tabela</option>
<?php foreach($tabelas as $tab):
$selecionado = ($tab['id_tabela'] == $tabela['id_tabela']) ? "selected='selected'": '';
?>	

<option <?php echo $selecionado;?> value="<?php echo $tab['id_tabela'];?>"><?php echo $tab['nome_tabela'];?></option>
<?php endforeach;?>
</select>

<label for="id_acao" style="text-align: left;float: left;font-weight: bold;margin-top: 10px;">Ação:</label>
<select name="id_acao" style="width: 100%;height: 30px;margin-bottom: 5px;">
<option value="0" selected="selected" disabled="disabled">Selecione a ação</option>
<?php foreach($acoes as $acao):?>	
<option value="<?php echo $acao['id_acao'];?>"><?php echo $acao['acao'];?></option>
<?php endforeach;?>
</select>
<br>
<input type="submit" name="submit" value="Inserir" class="btn btnSalvar" />

</form>
<br>


<a href="<?php echo BASE_URL;?>usuarios">
  <button type="button" class="btnVoltar">Voltar</button>
</a>
	
<a href="<?php echo BASE_URL;?>usuarioTabela/add">
  <button type="button" class="btnCadastro">Cadastrar Tabela</button>
</a>
</aside>

<section id="content">

<section id="tabela_resp">
	<table border="1" id="tabela_resp">
	
<thead>
<tr>
<th>Id</th>
<th>Tabela</th>
<th>Ação</th>
<th>Ação</th>
</tr>
	
</thead>

<tbody>
<?php 
if(isset($lista) && $lista != "" ):
foreach($lista as $tabela): 
?>

<tr>
<td><?php echo $tabela['id_tabela'];?></td>
<td><?php echo $tabela['nome_tabela'];?></td>
<td><?php echo $tabela['acao'];?></td>
<td>

<button type="button" class="btn btn-danger">
<a href="<?php echo BASE_URL;?>usuarioTabela/del/<?php echo $tabela['id_tabela'];?>/<?php echo $tabela['id_acao'];?>">Apagar</a>
</button>


</td>
</tr>

<?php 
endforeach;
endif;
?>
	
</tbody>	

</table>
</section><!-- tabela resp -->

</section>