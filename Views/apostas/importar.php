<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Importar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>contato"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->


<div id="message"></div>
<br>
<a style="margin-left: 10px;" download href="<?php echo BASE_URL;?>downloads/modelo_alunos.csv">Clique aqui para Baixar modelo</a>

<div class="box_form_content">

<form method="post" id="form_import" enctype="multipart/form-data" class="form_box">

<div class="input-wrapper w50 inputFile">
<span>CSV:</span>

<div class="btn-upload">
  <i class="fa fa-cloud-upload"aria-hidden="true"></i>	
</div>

<input type="file" name="file" id="file" accept=".csv"/>
<input type="hidden" name="hidden_field" value="1">
</div><!-- input wrapper-->    


<div class="input-wrapper w100">
<div id="process" style="display: none;">

<div class="progress">
    <div class="progress-bar"></div>
    <div class="estatiscas_upload"> 
    	<span id="process_data">0</span> <span>-</span> <span id="total_data">0</span> 
    </div>
</div><!-- progress-->


</div><!-- process -->
</div><!-- input wrapper-->  



    <div class="input-wrapper w100">
      <input type="submit" name="submit" id="import" class="btn btn-success" value="Importar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 

$(document).ready(function(){

Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {    
    window.location.href = base_url+"contato/importar";       
  }
});
}); 
</script>
<?php }?>