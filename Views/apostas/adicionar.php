<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>apostas"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->


<div class="box_form_content">



<form method="post" id="form_aposta" class="form_box" action="<?php echo BASE_URL;?>apostas/add_action">


    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper-->     

     <div class="input-wrapper w50">
      <span>Telefone:</span>
      <input type="text" name="telefone" class="phone" />
    </div><!-- input wrapper-->      



    <div class="input-wrapper w100">
      <span>Números:</span>
      <input type="text" name="aposta" class="aposta" placeholder="Digite os números para a aposta" data-parsley-trigger="keyup" data-parsley-required="true" data-parsley-pattern="^[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}[.]?[0-9]{2}$"/>
    </div><!-- input wrapper--> 

    
  

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_aposta').parsley();
 }); 
</script>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
});
});
</script>
<?php }?>