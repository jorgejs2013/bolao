<section class="content_page">
<h2>Usuário Tabelas</h2>

<div class="btn_topo">
<a href="<?php echo BASE_URL;?>usuariotabela/add" class="add_pagina">
  <button type="button" class="btn">Cadastrar Tabela</button>
</a>
</div>


<div class="separator"></div><!-- separator-->

<div class="container_shrinker">

<table class="table shrink">
	
<thead>
<tr>
<th>Id</th>
<th>Nome</th>
<th class="shrink-xs" align="right">Ação</th>
</tr>
	
</thead>

<tbody>
<?php 
if(isset($list) && $list != "" ):
foreach($list as $tabela): 
?>

<tr>
<td><?php echo $tabela->id_tabela;?></td>
<td><?php echo $tabela->nome_tabela;?></td>
<td align="right">

<button type="button" class="btn btn-info"><a href="<?php echo BASE_URL;?>usuarioTabela/edit/<?php echo $tabela->id_tabela;?>">Editar</a></button>
<button type="button" class="btn btn-error">
<a href="<?php echo BASE_URL;?>usuarioTabela/del/<?php echo $tabela->id_tabela;?>">Apagar</a>
</button>

<button type="button" class="btn btn-alert">
<a href="<?php echo BASE_URL;?>usuarioTabela/acoes/<?php echo $tabela->id_tabela;?>">Ações</a>
</button>
</td>
</tr>

<?php 
endforeach;
endif;
?>
	
</tbody>	

</table>
</div><!-- tabela shirinker -->

</section>