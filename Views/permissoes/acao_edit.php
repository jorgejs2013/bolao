<?php
$nome = (isset($info->acao)) ? $info->acao : null;
?>

<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Informe a Ação</h3>
  <span class="min_box">-</span>
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_acao" class="form_box">

    <div class="input-wrapper w100">
      <span>Nome:</span>
      <input type="text" name="nome" placeholder="Ex: inserir, editar, visualizar..."  value="<?php echo $nome;?>" />
    </div><!-- input wrapper--> 
   
<div class="input-wrapper w100">
<input type="submit" name="submit" value="Atualizar" class="btn btn-success" />
</div><!-- input wrapper-->   

</form>
</div><!-- box form content -->
</div><!-- box form -->  

</section>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
swal({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {
    $('#form_acao')[0].reset();
    window.location.href = "/system/usuarioAcao";   
     
  }
}); 
</script>
<?php }?>


<script type="text/javascript">
$(document).ready(function(){
var form = $('#form_acao');  
$('#submitForm').click(function(e){
e.preventDefault();
form.submit();
});
}); 
</script>
