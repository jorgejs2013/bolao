<?php
$nome = (isset($info->nome_tabela)) ? $info->nome_tabela : null;
$acao = (isset($info->ativo_tabela)) ? $info->ativo_tabela : null;
?>
<section class="content_page">


<div class="box_form">
<div class="box_form_title">
  <h3>Informe os dados do bairro</h3>
  <span class="min_box">-</span>
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_tabela" class="form_box">

    <div class="input-wrapper w100">
      <span>Nome:</span>
      <input type="text" name="nome" placeholder="Informe o Nome" value="<?php echo $nome;?>" />
    </div><!-- input wrapper-->

    <div class="input-wrapper w100">
      <span>Ação:</span>
      <select name="acao">
        <option value="S" <?php echo ($acao == 'S') ? 'selected="selected"' : null;?> >Sim</option>
        <option value="N" <?php echo ($acao == 'N') ? 'selected="selected"' : null;?>>Não</option>
      </select>
    </div><!-- input wrapper-->

   <div class="input-wrapper w100">
    <input type="submit" class="btn btn-success" name="submit" value="Salvar" />  
  </div><!-- input wrapper-->
  
   </form>

</div><!-- box form content -->
</div><!-- box form -->  

</section>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
swal({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {
    $('#form_tabela')[0].reset();
    window.location.href = "/system/usuarioTabela";   
     
  }
}); 
</script>
<?php }?>