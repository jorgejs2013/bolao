<section class="content_page">


<div class="lista_permissoes">	
<?php 
foreach($lista as $tabela):
?>

<div class="box_permissao">
<div class="box_permissao_title">
  <h3><?php echo $tabela->tabela->nome_tabela;?></h3>	
</div><!-- box permissao title-->	

<div class="box_permissao_opc">

	<?php foreach($tabela->acoes as $acao):?>	
	<div class="box_checkbox">
      <input type="checkbox" onclick="inserirPermissao(<?php echo $usuario->id_usuario.",".$tabela->tabela->id_tabela.",".$acao->id_acao.",". $acao->id_tabela_acao;?>)" name="acao<?php echo $acao->id_tabela_acao;?>" id="acao<?php echo $acao->id_tabela_acao;?>" <?php echo ($acao->marcado == '1') ? 'checked="checked"' : '';?>/>
      <label for="acao<?php echo $acao->id_tabela_acao;?>"><?php echo $acao->acao;?></label>
   </div><!-- box checkbox-->
   <?php endforeach;?>

</div><!-- box permissao opc -->
</div>


<?php endforeach;?>
</div><!-- lista permissoes-->
</section>


<script src="<?php echo BASE_URL;?>assets/js/js_permissao.js"></script>