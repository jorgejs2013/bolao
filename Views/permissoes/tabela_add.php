<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Informe os dados do bairro</h3>
  <span class="min_box">-</span>
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_tabela" class="form_box">

    <div class="input-wrapper w100">
      <span>Nome:</span>
      <input type="text" name="nome" placeholder="Informe o Nome" />
    </div><!-- input wrapper-->

    <div class="input-wrapper w100">
      <span>Ativa:</span>
      <select name="acao">
        <option value="S" selected="selected">Sim</option>
        <option value="N">Não</option>
      </select>
    </div><!-- input wrapper-->

   
  <div class="input-wrapper w100">
   <input type="submit" class="btn" name="submit" value="Salvar" />
  </div>
   

</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- form cad -->

</section>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
swal({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {
    $('#form_tabela')[0].reset();
    window.location.href = base_url+"usuarioTabela";   
     
  }
});
}); 
</script>
<?php }?>

<script>
 $(document).ready(function(){
  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  $('.cep').mask('00000-000', {placeholder: "99999-999"});
 }) 
</script>