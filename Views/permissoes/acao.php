<section class="content_page">	
<h2>Ações</h2>

<div class="btn_topo">
<a href="<?php echo BASE_URL;?>usuarios" class="add_pagina">
  <button type="button" class="btn">Voltar</button>
</a>
	
<a href="<?php echo BASE_URL;?>usuarioAcao/add" class="add_pagina">
  <button type="button" class="btn btn-success">Cadastrar Ação</button>
</a>
</div>

<div class="separator"></div><!-- separator-->

<div class="container_shrinker">
<table class="table shrink">
	
<thead>
<tr>
<th>Id</th>	
<th>Nome</th>
<th class="shrink-xs" align="right">Ação</th>
</tr>
	
</thead>

<tbody>
<?php 
if(isset($list) && $list != "" ):
foreach($list as $acao): 
?>

<tr>
<td><?php echo $acao->id_acao;?></td>
<td><?php echo $acao->acao;?></td>

<td align="right">

<button type="button" class="btn btn-info"><a href="<?php echo BASE_URL;?>usuarioAcao/edit/<?php echo $acao->id_acao;?>">Editar</a></button>
<button type="button" class="btn btn-error">
<a href="<?php echo BASE_URL;?>usuarioAcao/del/<?php echo $acao->id_acao;?>">Apagar</a>
</button>
</td>
</tr>

<?php 
endforeach;
endif;
?>
	
</tbody>	

</table>
</div><!-- container_shrinker -->

</section>