<?php
$nome = (isset($info->nome_razao)) ? $info->nome_razao: '';
$endereco = (isset($info->endereco)) ? $info->endereco: '';
$numero_endereco = (isset($info->numero_endereco)) ? $info->numero_endereco: '';
$bairro = (isset($info->bairro)) ? $info->bairro: '';
$codigo_cidade = (isset($info->codigo_cidade)) ? $info->codigo_cidade: '';
$cpf_cnpj = (isset($info->cpf_cnpj)) ? $info->cpf_cnpj: '';
$foto = (isset($info->imagem)) ? BASE_URL."medias/".$info->imagem->FOTO: '';
?>
<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Editar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>empresa"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
<form method="post" id="form_empresa" enctype="multipart/form-data" class="form_box" action="<?php echo BASE_URL;?>empresa/edit_action/<?php echo $id_empresa;?>">

   
    <div class="input-wrapper w50">
      <span>Razao Sosial:</span>
      <input type="text" name="razao_social" data-parsley-minlength="3" data-parsley-required="true" value="<?php echo $nome;?>" />
    </div><!-- input wrapper-->   

    <div class="input-wrapper w50">
      <span>Endereço:</span>
      <input type="text" name="endereco" value="<?php echo $endereco;?>"/>
    </div><!-- input wrapper--> 

      <div class="input-wrapper w50">
      <span>Núm endereco:</span>
      <input type="text" name="numero_endereco" value="<?php echo $numero_endereco;?>"/>
    </div><!-- input wrapper-->  

      <div class="input-wrapper w50">
      <span>Bairro:</span>
      <input type="text" name="bairro" value="<?php echo $bairro;?>"/>
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Código cidade:</span>
      <select name="codigo_cidade" class="codigo_cidade" style="height: 40px;">
        <?php foreach($cidades as $cidade):
            $sel = ($codigo_cidade == $cidade->CODIGO) ? 'selected': '';
          ?>
        <option <?php echo $sel;?> value="<?php echo $cidade->CODIGO;?>"><?php echo $cidade->NOME." - ".$cidade->UF;?></option>
      <?php endforeach;?>
      </select>
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Cnpj/Cpf:</span>
      <input type="text" name="cpf_cnpj" value="<?php echo $cpf_cnpj;?>"/>
    </div><!-- input wrapper-->  

<div class="input-wrapper w100 inputFile">
<span>Imagem:</span>
<div class="container-preview">
  <a href="javascript:;" class="remove_image">X</a>
  <i class="fa fa-cloud-upload"aria-hidden="true"></i>
  <img src="<?php echo $foto;?>" id="image" style="display: block;">
</div><!--  container preview-->
<!-- preview trigger -->
<input type="file" name="imagem" id="imageUser" onchange="showImage.call(this)" />
</div><!-- input wrapper-->  
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_empresa').parsley();

   $('.codigo_cidade').chosen();
 }); 
</script>


<script>
 function showImage(obj){
  if(this.files && this.files[0]){
    var obj = new FileReader();
    obj.onload = function(data){
      var image  = document.getElementById('image');
      image.src = data.target.result;
      image.style.display = 'block'; 
      document.querySelector('.remove_image').style.display = 'block';
    }
    obj.readAsDataURL(this.files[0]);
  }
 } 

let remove_image = document.querySelector('.remove_image');
remove_image.addEventListener('click', function(){ 
   
document.getElementById('imageUser').value = '';
document.getElementById('image').src = '#';
document.getElementById('image').style.display = 'none'; 
 document.querySelector('.remove_image').style.display = 'none';
});


document.querySelector('.fa-cloud-upload').addEventListener('click', function(){
  document.getElementById('imageUser').click();
});
</script>