<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>empresa"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
<form method="post" id="form_empresa" enctype="multipart/form-data" class="form_box" action="<?php echo BASE_URL;?>empresa/add_action">

   
    <div class="input-wrapper w50">
      <span>Razao Sosial:</span>
      <input type="text" name="razao_social" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper-->   

    <div class="input-wrapper w50">
      <span>Endereço:</span>
      <input type="text" name="endereco" />
    </div><!-- input wrapper--> 

      <div class="input-wrapper w50">
      <span>Núm endereco:</span>
      <input type="text" name="numero_endereco" />
    </div><!-- input wrapper-->  

      <div class="input-wrapper w50">
      <span>Bairro:</span>
      <input type="text" name="bairro" />
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Código cidade:</span>
      <select name="codigo_cidade" class="codigo_cidade" style="height: 40px;">
        <?php foreach($cidades as $cidade):?>
        <option value="<?php echo $cidade->CODIGO;?>"><?php echo $cidade->NOME." - ".$cidade->UF;?></option>
      <?php endforeach;?>
      </select>
    </div><!-- input wrapper-->

      <div class="input-wrapper w50">
      <span>Cnpj/Cpf:</span>
      <input type="text" name="cpf_cnpj" />
    </div><!-- input wrapper-->  

<div class="input-wrapper w100 inputFile">
<span>Imagem:</span>
<div class="container-preview">
  <a href="javascript:;" class="remove_image">X</a>
  <i class="fa fa-cloud-upload"aria-hidden="true"></i>
  <img src="" id="image">
</div><!--  container preview-->
<!-- preview trigger -->
<input type="file" name="imagem" id="imageUser" onchange="showImage.call(this)" />
</div><!-- input wrapper-->  
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_empresa').parsley();

   $('.codigo_cidade').chosen();
 }); 
</script>


<script>
 function showImage(obj){
  if(this.files && this.files[0]){
    var obj = new FileReader();
    obj.onload = function(data){
      var image  = document.getElementById('image');
      image.src = data.target.result;
      image.style.display = 'block'; 
      document.querySelector('.remove_image').style.display = 'block';
    }
    obj.readAsDataURL(this.files[0]);
  }
 } 

let remove_image = document.querySelector('.remove_image');
remove_image.addEventListener('click', function(){ 
   
document.getElementById('imageUser').value = '';
document.getElementById('image').src = '#';
document.getElementById('image').style.display = 'none'; 
 document.querySelector('.remove_image').style.display = 'none';
});


document.querySelector('.fa-cloud-upload').addEventListener('click', function(){
  document.getElementById('imageUser').click();
});
</script>