<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Bolão v1.0</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="robots" content="noindex, nofollow"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no" />	

  <?php 
    require_once("arquivos_css.php");
  ?>
	    
	<link rel="icon" href="<?php echo BASE_URL;?>assets/images/fennix.png" sizes="32x32">
	<link rel="canonical" url="<?php echo BASE_URL;?>">    
	
	<script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
	<script src="<?php echo BASE_URL;?>assets/js/system.js"></script>  
	<script>var base_url = '<?php echo BASE_URL;?>'</script>
  <script src="<?php echo BASE_URL;?>assets/js/chosen.jquery.min.js"></script>

</head>
<body>

<header>
<span id="button-menu" class="fa fa-close"></span>

<div class="logomarca">
	<a href="<?php echo BASE_URL;?>">
     <h2>Bolão v1.0</h2>	
    </a>
</div>


<div class="perfil">
<i class="fa fa-user-circle-o" aria-hidden="true"></i>Usuário <i class="fa fa-angle-down" aria-hidden="true"></i> 

<div class="sub_perfil">

<!-- <a href="<?php //echo BASE_URL;?>usuarios/perfil"><i class="fa fa-id-card-o" aria-hidden="true"></i>Meu perfil</a> -->

<a href="<?php echo BASE_URL;?>usuarios/reset"><i class="fa fa-key" aria-hidden="true"></i>Alterar senha</a>
<a href="<?php echo BASE_URL;?>usuarios/logout"><i class="fa fa-power-off" aria-hidden="true"></i>Sair</a>
</div>

</div><!-- perfil -->


<nav class="navegacao">
<ul class="menu">

<li class="title-menu">Menu de navegação</li>	

<li><a href="<?php echo BASE_URL;?>"><span class="fa fa-home icon-menu"></span>Inicio</a></li>



<li class="item-submenu" menu="1">
<a href="#"><span class="fa fa-folder-open icon-menu"></span>Cadastros</a>

<ul class="submenu">
  <li class="title-menu"><span class="fa fa-folder-open icon-menu"></span>Cadastros</li>	
  <li class="go-back">Voltar</li>  
  <li><a href="<?php echo BASE_URL;?>apostas">Aposta</a></li> 
  <li><a href="<?php echo BASE_URL;?>operadores">Operadores</a></li>
  <li><a href="<?php echo BASE_URL;?>concursos">Concursos</a></li>
  <li><a href="<?php echo BASE_URL;?>municipios">Municipios</a></li>
  <li><a href="<?php echo BASE_URL;?>empresa">Empresa</a></li>
 
</ul>
</li>


<li class="item-submenu" menu="4"><a href="#"><span class="fa fa-area-chart icon-menu"></span>Relatórios</a>
<ul class="submenu">
  <li class="title-menu"><span class="fa fa-area-chart icon-menu"></span>Relatórios</li>	
  <li class="go-back">Voltar</li>
  <li><a href="<?php echo BASE_URL;?>relatorios/concursos">Concursos</a></li> 
  <li><a href="<?php echo BASE_URL;?>relatorios/concursos">Operadores</a></li>  
</ul>
</li>

<li class="item-submenu" menu="5"><a href="#"><span class="fa fa-paper-plane-o icon-menu"></span>Campanhas</a>
<ul class="submenu">
  <li class="title-menu"><span class="fa fa-paper-plane-o icon-menu"></span>Campanhas</li>  
  <li class="go-back">Voltar</li>

  <li><a href="<?php echo BASE_URL;?>campanhas/add">Enviar Mensagem SMS</a></li> 
  <li><a href="<?php echo BASE_URL;?>campanhas/addLote">Enviar Mensagem em lote</a></li>   
  <!-- <li><a href="<?php //echo BASE_URL;?>campanhas/addZap">Enviar Whatsapp</a></li> -->
</ul>
</li>


<li><a href="<?php echo BASE_URL;?>consulta"><span class="fa fa-search icon-menu"></span>Consultar aposta</a></li>


<li class="item-submenu" menu="7">
<a href="#"><span class="fa fa-cogs icon-menu"></span>Configurações</a>
<ul class="submenu">
  <li class="title-menu"><span class="fa fa-cogs icon-menu"></span>Configurações</li>	
  <li class="go-back">Voltar</li>  
  
  <li><a href="<?php echo BASE_URL;?>usuarios">Usuários</a></li>
  <li><a href="<?php echo BASE_URL;?>usuariotabela">Permissões Tabelas</a></li>
  <li><a href="<?php echo BASE_URL;?>usuarioacao">Permissões Ações</a></li>  
  
</ul>
</li>



</ul>
</nav>	
</header>
 
<main>	

<section id="content-principal">

<?php 
   $this->loadViewInTemplate($viewName, $viewData); 
?>    

</section><!-- content principal-->	
</main>



<script>
var subPerfil = document.querySelector('.perfil');
subPerfil.addEventListener('click', abrir);
var submenu = false;

function abrir(){
	if(submenu == false){
		document.querySelector('.sub_perfil').style.display = 'block';
		submenu = true;
	}else{
        document.querySelector('.sub_perfil').style.display = 'none';
		submenu = false;
	}
}	
</script>
<?php
if(isset($viewData['list_js']) && !empty($viewData['list_js'])):
  foreach($viewData['list_js'] as $value):
     echo '<script src="'.BASE_URL.'assets/js/'.$value.'.js"></script>'.PHP_EOL;
  endforeach;
endif;
?>

<!-- Fullcalendar script -->
  <script src="<?php echo BASE_URL;?>assets/fullcalendar/js/core/main.min.js"></script>
  <script src="<?php echo BASE_URL;?>assets/fullcalendar/js/interaction/main.min.js"></script>
  <script src="<?php echo BASE_URL;?>assets/fullcalendar/js/daygrid/main.min.js"></script>

  <script src="<?php echo BASE_URL;?>assets/fullcalendar/js/timegrid/main.min.js"></script>
  <script src="<?php echo BASE_URL;?>assets/fullcalendar/js/core/locales-all.min.js"></script>
</body>
</html>