<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Gerador de recibo</h3>  
</div><!-- box form title -->


<div class="box_form_content">
<form method="post" id="form_recibo" class="form_box">

<div class="input-wrapper w50">
      <span>Recebido de:</span>
      <input type="text" name="recebido_de" data-parsley-required="true">
</div><!-- input wrapper-->

<div class="input-wrapper w50">
      <span>Importancia de:</span>
      <input type="text" class="money" name="importancia" data-parsley-required="true">
</div><!-- input wrapper-->

<div class="input-wrapper w100">
      <span>Valor por extenso:</span>
      <input type="text" name="importancia_ext" data-parsley-required="true">
</div><!-- input wrapper-->

<div class="input-wrapper w50">
      <span>Referente a:</span>
      <input type="text" name="referente" data-parsley-required="true">
</div><!-- input wrapper-->

<div class="input-wrapper w50">
      <span>Data:</span>
      <input type="text" name="data" class="date" placeholder="99/99/9999" data-parsley-required="true">
</div><!-- input wrapper-->
     

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Gerar recibo" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_recibo').parsley();
 }); 
</script>