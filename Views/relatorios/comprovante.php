<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Gerador de comprovante</h3>  
</div><!-- box form title -->


<div class="box_form_content">
<form method="post" id="form_comprovante" class="form_box">

<div class="input-wrapper w100">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-required="true">
</div><!-- input wrapper-->

<div class="input-wrapper w100">
      <span>Número do bilhete:</span>
      <input type="text" name="numero" data-parsley-required="true">
</div><!-- input wrapper-->

<div class="input-wrapper w100">
      <span>Contato:</span>
      <input type="text" name="contato" class="phone" data-parsley-required="true">
</div><!-- input wrapper-->


    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Gerar comprovante" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_comprovante').parsley();
 }); 
</script>