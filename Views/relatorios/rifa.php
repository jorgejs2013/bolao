<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Relatorio de rifas</h3>  
</div><!-- box form title -->


<div class="box_form_content">
<form method="post" id="form_relatorio" class="form_box">

    <div class="input-wrapper w30">
      <span>Sorteios:</span>
      <select name="sorteios">
        <option value="0" selected="selected" disabled="disabled">Selecione o sorteio</option>
        <?php foreach($rifas as $rifa):?>
        <option value="<?php echo $rifa->id_rifa;?>"><?php echo $rifa->titulo_rifa;?></option>
      <?php endforeach;?>
        
      </select>
    </div><!-- input wrapper-->

     <div class="input-wrapper w30">
      <span>Cambistas:</span>
      <select name="cambista">
        <option value="0" selected="selected" disabled="disabled">Selecione o cambista</option>        
        <?php foreach($cambistas as $cambista):?>
        <option value="<?php echo $cambista->slug_nome;?>"><?php echo $cambista->nome;?></option>
      <?php endforeach;?>        
      </select>
    </div><!-- input wrapper-->

    <div class="input-wrapper w30">
      <span>Status pagamento:</span>
      <select name="status_pag">
        <option value="0" selected="selected" disabled="disabled">Selecione o status</option>        
        <option value="1">Aguardando pagamento</option>
        <option value="2">Pago</option>          
      </select>
    </div><!-- input wrapper-->

    <div class="input-wrapper w50">
      <span>Data Inicial:</span>
       <input type="text" class="date" name="data_inicial" placeholder="99/99/9999">
    </div><!-- input wrapper--> 

     <div class="input-wrapper w50">
      <span>Data Final:</span>
      <input type="text" class="date" name="data_final" placeholder="99/99/9999">
    </div><!-- input wrapper-->  

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Gerar relatorio" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_relatorio').parsley();
 }); 
</script>