<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Relatorio de alunos</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>categorias"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->


<div class="box_form_content">
<form method="post" id="form_relatorio" class="form_box">

    <div class="input-wrapper w50">
      <span>Turno:</span>
      <select name="turno">
        <option value="0" selected="selected" disabled="disabled">Selecione o turno</option>
        <option value="matutino">Matutino</option>
        <option value="vespertino">Vespertino</option>
        <option value="noturno">Noturno</option>
      </select>
    </div><!-- input wrapper-->

    <div class="input-wrapper w50">
      <span>Curso:</span>
     <select name="curso">
       <option value="0" selected="selected">Selecione o curso</option>
       <?php foreach($cursos as $curso):?>
        <option value="<?php echo $curso->id_curso;?>"><?php echo $curso->nome;?></option>
       <?php endforeach;?>
     </select>
    </div><!-- input wrapper-->   

    <div class="input-wrapper w50">
      <span>Série:</span>
      <select name="serie">
        <option value="0" selected="selected" disabled="disabled">Selecione a série</option>
        <?php foreach($series as $serie):?>        
        <option value="<?php echo $serie->id_serie;?>"><?php echo $serie->nome_serie;?></option>
        <?php endforeach;?>      
      </select>
    </div><!-- input wrapper--> 

   <!--  <div class="input-wrapper w50">
      <span>Bimestre:</span>
      <select name="bimestre">
        <option value="0" selected="selected" disabled="disabled">Selecione o bimestre</option>        
        <option value="1">1º Bimestre</option>
        <option value="2">2º Bimestre</option>
        <option value="3">3º Bimestre</option>
        <option value="4">4º Bimestre</option>  
      </select>
    </div>    
    -->
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Gerar relatorio" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_relatorio').parsley();
 }); 
</script>