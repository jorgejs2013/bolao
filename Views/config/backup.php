<section class="content_page">
<div class="box_form_content"> 


<h2>Selecione as tabelas para efetuaro backup</h2>
<br>
<label for="checkTodos">
  <input type="checkbox" id="checkTodos" name="checkTodos"> Marcar todos
</label>
<br>
<hr>
<br>

<form method="post" id="export_form" class="form_box">

<?php 
foreach($lista as $table):  
// echo "<pre>";
// print_r($table);
// exit;
?>

<div class="input-wrapper w100"> 
<label>
  <!-- online é Tables_in_unixsist_system / Offline é Tables_in_system / online Tables_in_unixsist_system -->
<input type="checkbox" class="checkbox_table" class="marcar" name="table[]" value="<?php echo $table->Tables_in_unixsist_boletosmil;?>" /><?php echo $table->Tables_in_unixsist_boletosmil;?>
</label>
</div>

<?php endforeach;?>

<div class="input-wrapper w100">
   <button type="button" id="submit" class="btn btn-alert">Exportar</button>
</div>

</form>
</div>

</section>


<script>
$(document).ready(function(){
 var form = $('#export_form'); 

	$('#submit').click(function(e){       
      var count = 0;
      $('.checkbox_table').each(function(){
        if($(this).is(':checked')){
        	count = count + 1;
        }
      });

      if(count > 0){ 
        
        $.ajax({
          url:base_url+"backup/gerar",
          type:'POST',
          data:form.serialize(),
          dataType:'json',
          success:function(data){
            console.log(data);
          }
        });       
      }else{
      	alert('por favor selecione uma tabela para exportar');
      	return false;
      }		
	});

});	
</script>