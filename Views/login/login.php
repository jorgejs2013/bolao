<html>
  <head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script> 
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/login.css">
  </head>
  <body>
    <div class="containertotal">

      <div class="flex">
        <form method="POST" action="<?php echo BASE_URL;?>login/index_action">
          <center><img class="logo" src="<?php echo BASE_URL;?>assets/images/logo.jpg" width="250" /></center>
          <br>
          <div class="input-field">           
            <input class="size" type="text" name="email" id="email" autofocus required maxlength="80" placeholder="E-mail" />
          </div><br/>

          <div class="input-field">           
            <input style="padding-right: 30px;" id="uePassowrd" class="size" type="password" id="password" name="senha" required maxlength="48" placeholder="Senha" />
            <div id="c">
              <img id="ueEyePass" style="cursor: pointer;width: 20px;" src="<?php echo BASE_URL; ?>assets/images/eye.png">
            </div>
          </div>

          <div class="lembrar">
            <input type="submit" value="Entrar" />
          </div>
          
          <center><a href="<?php echo BASE_URL; ?>login/restore" class="recovery">Recupere sua senha?</a></center><br/>
          
        </form>

        <center>          
        <?php         
         if(isset($errorMsg) && !empty($errorMsg)):
           echo '<strong>'.$errorMsg.'</strong';
         endif;
        ?>       
      </center>

      </div><!-- flex-->

      <div class="background">        
          <h2 class="h2-2">Cada novo dia é uma nova oportunidade</h2>
          <h4>Pequenas melhorias diárias criam resultados incríveis</h4>        
      </div><!--background -->
    </div>

    <?php if (!empty($alertmsg)): ?>
      <script>swal("Atenção!", "<?php echo $alertmsg; ?>", "warning", {timer: 4000, buttons: false}).then(() => {window.location.href = '<?php echo BASE_URL; ?>login/esqueciMinhaSenha'});</script>
    <?php elseif (!empty($alertmsg2)): ?>
      <script>swal("Atenção!", "<?php echo $alertmsg2; ?>", "warning", {timer: 2000, buttons: false}).then(() => {window.location.href = '<?php echo BASE_URL; ?>login'});</script>
    <?php elseif (!empty($msgBQ)): ?>
      <script>swal("Atenção!", "<?php echo $msgBQ; ?>", "warning", {timer: 3000, buttons: false}).then(() => {window.location.href = '<?php echo BASE_URL; ?>login'});</script>
    <?php elseif (!empty($msgES)): ?>
      <script>swal("ERRO!", "<?php echo $msgES; ?>", "error", {timer: 3000, buttons: false}).then(() => {window.location.href = '<?php echo BASE_URL; ?>login'});</script>
    <?php endif; ?>
    


    <script type="text/javascript">
      $("#c").mousedown(function () {
        if ($("#uePassowrd").attr("type") == 'text') {
            $("#uePassowrd").attr("type", "password");          
          }else{
            $("#uePassowrd").attr("type", "text");
          }
      });
    </script>
  </body>
</html>