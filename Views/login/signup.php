<!DOCTYPE html>
<html>
<head>
	<title>Login Dashboard</title>
	<meta charset="utf-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link href="<?php echo BASE_URL;?>assets/css/login.css" rel="stylesheet" />
	<link href="<?php echo BASE_URL;?>assets/css/font-awesome.min.css" rel="stylesheet" />
	<link href="<?php echo BASE_URL;?>assets/css/tabela_resp.css" rel="stylesheet" />

	

	<script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
	<script src="<?php echo BASE_URL;?>assets/js/sweetalert2.all.min.js"></script>

</head>
<body>

<header>

<div class="company">Dashboard</div><!-- company -->

<div class="perfil">
	<a href="<?php echo BASE_URL;?>login">Login</a>
</div><!-- perfil  -->

</header>


<main>

<div class="form_login">

<h2>Crie uma conta</h2>
<form method="post">
<input type="text" name="nome" placeholder="Seu Nome Completo" />
<input type="text" name="apelido" placeholder="Como deseja ser chamado" />
<input type="text" name="cidade" placeholder="Informe sua cidade" />
<input type="text" name="bairro" list="bairros" placeholder="Informe o bairro" />
<datalist id="bairros">
        <?php foreach($lista_bairros as $bairro):?>
        <option value="<?php echo $bairro['nome'];?>">
        <?php endforeach;?>
      </datalist>
<input type="text" name="cep"  placeholder="Informe o Cep" />     
<input type="email" name="email" placeholder="Seu E-mail" />
<input type="text" name="telefone" placeholder="Número de telefone válido" />
<select name="tipo">	
	<option value="cliente" selected="selected">Cliente</option>
</select>
<input type="password" name="password" placeholder="Senha">
<input type="submit" name="submit" value="Criar uma conta" />
</form>


</div>


</main>

<?php if(isset($msg) && $msg != ""):?>

<script>
$(document).ready(function(){
let timerInterval
var msg = '<?php echo $msg;?>';
swal({
  title: msg,
  html: 'Voltando para tela de login em <strong></strong> segundos.',
  timer: 4000,
  onOpen: () => {
    swal.showLoading()
    timerInterval = setInterval(() => {
      swal.getContent().querySelector('strong')
        .textContent = swal.getTimerLeft()
    }, 300)
  },
  onClose: () => {
    clearInterval(timerInterval)
    window.location.href = "/mypizza/login";
  }
}).then((result) => {
  if (
    // Read more about handling dismissals
    result.dismiss === swal.DismissReason.timer
  ) {
    //console.log('I was closed by the timer')
  }
})
});	
</script>

<?php endif;?>


</body>
</html>