<link href="https://fonts.googleapis.com/css?family=Exo:300,400,600" rel="stylesheet">
<link href="<?php echo BASE_URL;?>assets/css/dashboard.css" rel="stylesheet" />	
<link href="<?php echo BASE_URL;?>assets/css/system.css" rel="stylesheet" />		
<link href="<?php echo BASE_URL;?>assets/css/font-awesome.min.css" rel="stylesheet">
<!-- Tabela shrinker -->
<link href="<?php echo BASE_URL;?>assets/css/jquery.table-shrinker.css" rel="stylesheet">
<link href="<?php echo BASE_URL;?>assets/css/table_custom.css" rel="stylesheet">
<link href="<?php echo BASE_URL;?>/assets/css/select2.min.css" rel="stylesheet" /> 

<!-- Estilização para Formulario -->
<link href="<?php echo BASE_URL;?>assets/css/forms.css" rel="stylesheet">
<link href="<?php echo BASE_URL;?>assets/css/switch_button.css" rel="stylesheet">

<link href="<?php echo BASE_URL;?>assets/css/tabs.css" rel="stylesheet">
<link href="<?php echo BASE_URL;?>assets/css/custom_checkbox.css" rel="stylesheet">
<link href="<?php echo BASE_URL;?>assets/css/parsley.css" rel="stylesheet">
<link href="<?php echo BASE_URL;?>assets/css/flatpickr.min.css" rel="stylesheet">

<!-- Janela modal com dialogify -->
<link href="<?php echo BASE_URL;?>assets/css/dialogify.css" rel="stylesheet">
  
<!-- FullCalendar -->
<link href="<?php echo BASE_URL;?>assets/fullcalendar/css/core/main.min.css" rel="stylesheet" />
<link href="<?php echo BASE_URL;?>assets/fullcalendar/css/daygrid/main.min.css" rel="stylesheet" />
<link href="<?php echo BASE_URL;?>/assets/fullcalendar/css/timegrid/main.min.css" rel="stylesheet" /> 
<!-- Jquery modal -->
<link href="<?php echo BASE_URL;?>/assets/css/jquery.modal.css" rel="stylesheet" /> 

<link href="<?php echo BASE_URL;?>/assets/css/chosen.min.css" rel="stylesheet" />