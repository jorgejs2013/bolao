<section class="content_page">	
<h2>Operadores</h2>

<div class="btn_topo">

<a href="<?php echo BASE_URL;?>operadores/add" class="add_pagina">
  <button type="button" class="btn" id="adicionar"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</button>
</a>

</div><!-- btn topo-->


<div class="filtros_forma">
<div class="box_filtro">

<div class="box_filtro_titulo">
<h3>Buscar por operador</h3>
</div><!-- box filtro titulo -->

<div class="box_form_content">
	<form method="get" class="form_box">
      
    <div class="input-wrapper w30">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper--> 


    <div class="input-wrapper w30">
      <span>E-mail:</span>
      <input type="text" name="email" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper--> 

<div class="input-wrapper w30 singleButton">
  <input type="reset" id="limpar" class="btn btn-info" value="Resetar" />
<input type="submit" name="submit" class="btn btn-alert" value="Pesquisar" />
</div><!-- input wrapper--> 
     
	</form>
</div><!--box form content -->

</div><!-- box filtro-->
</div><!-- filtros_forma -->



<div class="container_shrinker">
<table class="table shrink">
<thead>
<tr>
	<th>Código</th>	
	<th>Nome</th>
	<th class="shrink-xs shrinkable">Situação</th>	
  <th class="shrink-xs shrinkable">Telefone</th>
	<th class="shrink-md">Ação</th>	
</tr>	
</thead>	

<tbody>
	
<?php foreach ($lista as $op): ?>
<tr>
<td><?php echo str_pad($op->ID, 5, "0", STR_PAD_LEFT) ;?></td>	
<td><?php echo $op->NOME;?></td>	
<td><?php echo $op->SITUACAO;?></td>
<td><?php echo $op->TELEFONE;?></td>
<td>
<a href="<?php echo BASE_URL;?>operadores/edit/<?php echo $op->ID;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  | 
<a href="<?php echo BASE_URL;?>operadores/del/<?php echo $op->ID;?>"><i class="fa fa-trash" aria-hidden="true"></i></a>	
</td>
</tr>

<?php endforeach;?>
		
</tbody>
</table>
</div><!-- container shrinker -->

<?php
if(isset($paginacao)):
echo $paginacao;
endif;
?>

</section><!-- content page -->

<script>
$(document).ready(function(){
$('#limpar').click(function(){
window.location.href = base_url+"operadores";   
});
});	
</script>
