<?php
$nome = (isset($info->NOME)) ? $info->NOME: '';
$situacao = (isset($info->SITUACAO)) ? $info->SITUACAO: '';
$senha = (isset($info->SENHA)) ? $info->SENHA: '';
$aposta_automatica = (isset($info->APOSTA_AUTOMATICA)) ? $info->APOSTA_AUTOMATICA: '';
$cambista = (isset($info->CAMBISTA)) ? $info->CAMBISTA: '';
$codigo_hash = (isset($info->CODIGO_HASH)) ? $info->CODIGO_HASH: '';
$telefone = (isset($info->TELEFONE)) ? $info->TELEFONE: '';
$promotor = (isset($info->PROMOTOR)) ? $info->PROMOTOR: '';
$gerente = (isset($info->GERENTE)) ? $info->GERENTE: '';
?>

<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Editar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>operadores"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
<form method="post" id="form_operador" class="form_box" action="<?php echo BASE_URL;?>operadores/edit_action/<?php echo $id_operador;?>">

   
    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-required="true" value="<?php echo $nome;?>" />
    </div><!-- input wrapper-->   

        <div class="input-wrapper w50">
      <span>Promotor:</span>
      <input type="text" name="promotor" value="<?php echo $promotor;?>"/>
    </div><!-- input wrapper-->   

     <div class="input-wrapper w50">
      <span>Senha:</span>
      <input type="password" name="senha" maxlength="6" value="<?php echo $senha;?>"/>
    </div><!-- input wrapper-->    

    <div class="input-wrapper w50">
      <span>Telefone:</span>
      <input type="text" name="telefone" class="phone" data-parsley-minlength="3" value="<?php echo $telefone;?>"/>
    </div><!-- input wrapper--> 

 

<div class="input-wrapper w100">

<div class="input-wrapper w50"> 
<div class="input-wrapper w25">
    <span>Ativo:</span>
     <input id="checkbox1" class="custom_checkbox" <?php echo ($situacao == 'A') ? 'checked': '';?> name="situacao" type="checkbox">
      <label for="checkbox1" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper--> 

<div class="input-wrapper w25">
    <span>A. automatica:</span>
     <input id="checkbox2" class="custom_checkbox" <?php echo ($aposta_automatica == 'S') ? 'checked': '';?> name="aposta_automatica" type="checkbox">
      <label for="checkbox2" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->


<div class="input-wrapper w25">
    <span>Cambista:</span>
     <input id="checkbox5" class="custom_checkbox" name="cambista" <?php echo ($cambista == 'S') ? 'checked': '';?> type="checkbox">
      <label for="checkbox5" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->

<div class="input-wrapper w25">
    <span>Gerente:</span>
     <input id="checkbox4" class="custom_checkbox" <?php echo ($gerente == 'S') ? 'checked': '';?> name="gerente" type="checkbox">
      <label for="checkbox4" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->
</div><!-- input wrapper-->


</div><!-- input wrapper-->      
 
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->

<script>
 $(document).ready(function(){   
   $('#form_operador').parsley();
 }); 
</script>