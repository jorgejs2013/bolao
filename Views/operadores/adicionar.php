<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>operadores"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
<form method="post" id="form_operador" class="form_box" action="<?php echo BASE_URL;?>operadores/add_action">

   
    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper-->  

    <div class="input-wrapper w50">
      <span>Telefone:</span>
      <input type="text" name="telefone" class="phone" data-parsley-minlength="3" />
    </div><!-- input wrapper-->  

        <div class="input-wrapper w50">
      <span>Promotor:</span>
     <select name="promotor">
       <option value="">Selecione...</option>
       <?php foreach($lista as $promotor):?>
        <option value="<?php echo $promotor->ID;?>"><?php echo $promotor->NOME;?></option>
       <?php endforeach;?>
     </select>
    </div><!-- input wrapper-->   

     <div class="input-wrapper w50">
      <span>Senha:</span>
      <input type="password" name="senha" maxlength="6" />
    </div><!-- input wrapper-->    



 

<div class="input-wrapper w100">

<div class="input-wrapper w50"> 
<div class="input-wrapper w25">
    <span>Ativo:</span>
     <input id="checkbox1" class="custom_checkbox" checked="checked" name="situacao" type="checkbox">
      <label for="checkbox1" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper--> 

<div class="input-wrapper w25">
    <span>A. automatica:</span>
     <input id="checkbox2" class="custom_checkbox" name="aposta_automatica" type="checkbox">
      <label for="checkbox2" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->


<div class="input-wrapper w25">
    <span>Cambista:</span>
     <input id="checkbox5" class="custom_checkbox" name="promotor" type="checkbox">
      <label for="checkbox5" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->

<div class="input-wrapper w25">
    <span>Gerente:</span>
     <input id="checkbox4" class="custom_checkbox" name="gerente" type="checkbox">
      <label for="checkbox4" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->
</div><!-- input wrapper-->


</div><!-- input wrapper-->      
 
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_operador').parsley();
 }); 
</script>