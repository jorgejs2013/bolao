<?php
$eh_cliente = (isset($info->eh_cliente)) ? $info->eh_cliente: '';
$eh_cambista = (isset($info->eh_cambista)) ? $info->eh_cambista: '';
$nome = (isset($info->nome)) ? $info->nome: '';
$data_nasc = (isset($info->data_nasc)) ? $info->data_nasc: '';
$cpf = (isset($info->cpf)) ? $info->cpf: '';
$data_cadastro = (isset($info->data_cadastro)) ? $info->data_cadastro: '';
$ddd = (isset($info->ddd)) ? $info->ddd: '';
$fone = (isset($info->fone)) ? $info->fone: '';
$celular = (isset($info->celular)) ? $info->celular: '';
$email = (isset($info->email)) ? $info->email: '';

$cep = (isset($info->cep)) ? $info->cep: '';
$logradouro = (isset($info->logradouro)) ? $info->logradouro: '';
$numero = (isset($info->numero)) ? $info->numero: '';
$id_estado = (isset($info->id_estado)) ? $info->id_estado: '';
$id_cidade = (isset($info->id_cidade)) ? $info->id_cidade: '';
$complemento = (isset($info->complemento)) ? $info->complemento: '';
$bairro = (isset($info->bairro)) ? $info->bairro: '';
?>
<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>contato"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->


<div class="box_form_content">

<ul class="js-tabmenu">
  <li>Geral</li>
  <li>Endereço</li> 
</ul>


<form method="post" id="form_contato" class="form_box" action="<?php echo BASE_URL;?>contato/edit_action/<?php echo $id_contato;?>">

<div class="js-tabcontent">
<section>    
<div class="input-wrapper w100">   
<div class="input-wrapper w50">

<div class="input-wrapper w25">
    <span>Cliente:</span>
     <input id="checkbox1" class="custom_checkbox" <?php echo ($eh_cliente == 'S') ? 'checked="checked"': '';?> name="eh_cliente" type="checkbox">
      <label for="checkbox1" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper--> 

<div class="input-wrapper w25">
    <span>Cambista:</span>
     <input id="checkbox2" class="custom_checkbox" <?php echo ($eh_cambista == 'S') ? 'checked="checked"': '';?> name="eh_fornecedor" type="checkbox">
      <label for="checkbox2" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->

</div><!-- input wrapper-->
</div><!-- input wrapper-->



    <div class="input-wrapper w100">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true" value="<?php echo $nome;?>" />
    </div><!-- input wrapper-->    

     <div class="input-wrapper w50">
      <span>CPF:</span>
      <input type="text" name="cpf" class="cpf" data-parsley-minlength="3" value="<?php echo $cpf;?>"/>
    </div><!-- input wrapper-->     

    <div class="input-wrapper w50">
      <span>Data Nascimento:</span>
      <input type="text" name="data_nascimento" data-parsley-minlength="3" data-parsley-required="true" placeholder="dd/mm/yyyy" date-id="datetime" class="calendario" data-date-format="d/m/Y" value="<?php echo $data_nasc;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>DDD:</span>
      <input type="text" name="ddd" data-parsley-minlength="2" data-parsley-maxlength="4" value="<?php echo $ddd;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>Telefone:</span>
      <input type="text" name="telefone" class="telefone" value="<?php echo $fone;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>Celular:</span>
      <input type="text" name="celular" class="celular" data-parsley-minlength="3" value="<?php echo $celular;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>E-mail:</span>
      <input type="email" name="email" data-parsley-minlength="3" value="<?php echo $email;?>"/>
    </div><!-- input wrapper-->

    <div class="input-wrapper w50">
      <span>Senha:</span>
      <input type="password" name="senha" data-parsley-minlength="3" />
    </div><!-- input wrapper-->     
</section><!--geral -->

<section>

<div class="input-wrapper w50">  
    <span>Cep:</span>
      <input type="text" name="cep" class="cep" data-parsley-minlength="3" value="<?php echo $cep;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Logradouro:</span>
      <input type="text" name="logradouro" data-parsley-minlength="3" value="<?php echo $logradouro;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w30">  
    <span>Número:</span>
      <input type="text" name="numero" value="<?php echo $numero;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w30">  
    <span>UF:</span>
      <select name="uf">
        <option value="0" disabled="disabled">Selecione</option>
        <?php 
         foreach($estados as $estado):
         ?>
          <option value="<?php echo $estado->id_estado;?>" <?php echo ($id_estado == $estado->id_estado) ? 'selected="selected"': '';?>><?php echo $estado->nome_estado;?></option>
         
         <?php
         endforeach;
        ?>
      </select>
</div><!-- input wrapper-->

<div class="input-wrapper w30">  
    <span>Cidade:</span>
      <select name="cidade">
        <option value="0" disabled="disabled">Selecione</option>
        <?php 
         foreach($cidades as $cidade):
          ?>
         <option <?php echo ($id_cidade == $cidade->id_cidade) ? 'selected="selected"' : "";?> value="<?php echo $cidade->id_cidade;?>"><?php echo $cidade->nome_cidade;?></option>

         <?php
         endforeach;
        ?>
      </select>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Complemento:</span>
      <input type="text" name="complemento" data-parsley-minlength="3" value="<?php echo $complemento;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Bairro:</span>
      <input type="text" name="bairro" data-parsley-minlength="3" value="<?php echo $bairro;?>"/>
</div><!-- input wrapper-->

</section>  <!-- Endereço -->



<section>
<div class="input-wrapper w50">  
    <span>Insc. Estadual:</span>
      <input type="text" name="ie" data-parsley-minlength="3" value="<?php echo $ie;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Insc. Municipal:</span>
      <input type="text" name="im" data-parsley-minlength="3" value="<?php echo $im;?>" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>RG:</span>
      <input type="text" name="rg" data-parsley-minlength="3" value="<?php echo $rg;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Suframa:</span>
      <input type="text" name="suframa" data-parsley-minlength="3" value="<?php echo $suframa;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Cód. Estrangeiro:</span>
      <input type="text" name="cod_estrangeiro" data-parsley-minlength="2" value="<?php echo $cod_estrangeiro;?>"/>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Subs. Tributária:</span>
      <input type="text" name="ie_subt_trib" data-parsley-minlength="2" value="<?php echo $ie_subt_trib;?>"/>
</div><!-- input wrapper-->
</section>

</div><!-- jb-content-->   
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Atualizar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
swal({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {   
    
     
  }
}); 
</script>
<?php }?>

<script>
 $(document).ready(function(){   
   $('#form_contato').parsley();
 }); 
</script>