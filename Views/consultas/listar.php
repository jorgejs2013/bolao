<section class="content_page">	
<h2>Consultas</h2>

<div class="btn_topo">

<a href="<?php echo BASE_URL;?>consultas/add" class="add_pagina">
  <button type="button" class="btn" id="adicionar"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</button>
</a>

</div><!-- btn topo-->


<div class="filtros_forma">
<div class="box_filtro">

<div class="box_filtro_titulo">
<h3>Buscar por concurso</h3>
</div><!-- box filtro titulo -->

<div class="box_form_content">
	<form method="get" id="form_consulta" class="form_box">
      
    <div class="input-wrapper w25">
      <span>Concurso:</span>
      <select name="concurso" class="concurso" required>
        <option value="">Selecione...</option>
        <?php foreach($concursos as $concurso):?>
        <option value="<?php echo $concurso->ID;?>"><?php echo $concurso->NOME;?></option>
      <?php endforeach;?>
      </select>
    </div><!-- input wrapper--> 

      <div class="input-wrapper w25">
      <span>Operador:</span>
      <select name="operador" class="operador">
        <option value="">Selecione...</option>
        <?php foreach($operadores as $operador):?>
        <option value="<?php echo $operador->ID;?>"><?php echo $operador->NOME;?></option>
      <?php endforeach;?>
      </select>
    </div><!-- input wrapper--> 

      <div class="input-wrapper w25">
      <span>Apostador:</span>
      <select name="apostador" class="apostador">
        <option value="">Selecione...</option>
        <?php foreach($apostadores as $apostador):?>
        <option value="<?php echo $apostador->NOME;?>"><?php echo $apostador->NOME;?></option>
      <?php endforeach;?>
      </select>
    </div><!-- input wrapper--> 

<div class="input-wrapper w25 singleButton">
  <input type="reset" id="limpar" class="btn btn-info" value="Resetar" />
<input type="submit" class="btn btn-alert" value="Pesquisar" />
</div><!-- input wrapper--> 
     
	</form>
</div><!--box form content -->

</div><!-- box filtro-->
</div><!-- filtros_forma -->



<div class="container_shrinker">
<table class="table shrink">
<thead>
<tr>
	<th>Pontos</th>	
	<th>Aposta</th>
	<th class="shrink-xs shrinkable">Números</th>	
  <th class="shrink-xs shrinkable">vlr aposta</th>
	<th class="shrink-md">Taxa Adm</th>	
  <th class="shrink-md">Comissão</th> 
  <th class="shrink-md">Data hora</th>
  <th class="shrink-md">Operador</th>
  <th class="shrink-md">Apostador</th>
</tr>	
</thead>	

<tbody>
	
<?php 
foreach ($lista as $concurso): 
$taxa = ($concurso->TAXA_ADMINISTRACAO / 100) * $concurso->VALOR_APOSTA;
$comissao = ($concurso->COMISSAO_VENDEDOR / 100) * $concurso->VALOR_APOSTA;
?>
<tr>
<td><?php echo $concurso->PONTUACAO;?></td>
<td><?php echo str_pad($concurso->id_concurso, 5, "0", STR_PAD_LEFT) ;?></td>	
<td><?php echo $concurso->NUMEROS_APOSTADOS;?></td>
<td><?php echo $concurso->VALOR_APOSTA;?></td>
<td><?php echo "R$ ".number_format($taxa,2,',','.');;?></td>
<td><?php echo "R$ ".number_format($comissao,2,',','.');?></td>
<td><?php echo $concurso->DATA. " ".$concurso->HORA_LIMITE;?></td>
<td><?php echo $concurso->nome_operador;?></td>
<td><?php echo $concurso->APOSTADOR;?></td>
</tr>

<?php endforeach;?>
		
</tbody>
</table>
</div><!-- container shrinker -->

<?php
if(isset($paginacao)):
echo $paginacao;
endif;
?>

</section><!-- content page -->

<script>
$(document).ready(function(){
  $('.concurso').chosen();
  $('.operador').chosen();
  $('.apostador').chosen();
  $('#form_consulta').parsley();

$('#limpar').click(function(){
window.location.href = base_url+"consulta";   
});
});	
</script>
