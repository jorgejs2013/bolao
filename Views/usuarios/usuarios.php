<section class="content_page">
<h2>Usuários</h2>

<div class="btn_topo">
<a href="<?php echo BASE_URL;?>usuarios/add" class="add_pagina">
<button type="button" class="btn" id="adicionar"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</button>
</a>

</div>

<div class="filtros_forma">
<div class="box_filtro">

<div class="box_filtro_titulo">
<h3>Buscar por usuário</h3>
</div><!-- box filtro titulo -->

<div class="box_form_content">
	<form method="post" class="form_box">
      
    <div class="input-wrapper w30">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper--> 


    <div class="input-wrapper w30">
      <span>E-mail:</span>
      <input type="text" name="email" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper--> 

<div class="input-wrapper w30 singleButton">
<input type="submit" name="submit" class="btn" value="Pesquisar" />
</div><!-- input wrapper--> 
     
	</form>
</div><!--box form content -->

</div><!-- box filtro-->
</div><!-- filtros_forma -->


<div class="container_shrinker">

<table class="table shrink">
<thead>
<tr>
	<th class="shrink-xs">Nome</th>
	<th class="shrink-xs shrinkable">E-mail</th>
  <th class="shrink-xs">Nível</th>	
	<th class="shrink-xs">Ações</th>
</tr>	
</thead>	

<tbody>
<?php 
if(!empty($listaUsers)):
foreach($listaUsers as $usuario):?>	
<tr>
<td><?php echo $usuario->nome;?></td>	
<td><?php echo $usuario->email;?></td>
<td><?php echo ($usuario->cambista == 'S') ? 'Cambista': 'Admin';?></td>
<td>
<a href="<?php echo BASE_URL;?>usuarios/edit/<?php echo $usuario->id;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  | 
<a href="<?php echo BASE_URL;?>usuarios/del/<?php echo $usuario->id;?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
|
<a href="<?php echo BASE_URL;?>permissao/index/<?php echo $usuario->id;?>">Permissão</a>		
</td>
</tr>
<?php endforeach;
endif;
?>
	
</tbody>
</table>
</div><!-- table listagem-->

</section><!-- content page -->


<script>
$(document).ready(function(){
$('#limpar').click(function(){
window.location.href = BASE_URL+"usuarios";   
});
});	
</script>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {   
    
     
  }
}); 
});
</script>
<?php }?>
