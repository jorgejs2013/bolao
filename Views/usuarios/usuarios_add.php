<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box"><a href="<?php echo BASE_URL;?>usuarios"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_usuario" enctype="multipart/form-data" class="form_box" action="<?php echo BASE_URL."usuarios/add_action";?>">
    
    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Contato:</span>
      <input type="text" name="contato" class="phone" placeholder="(99) 9999-9999" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>E-mail:</span>
      <input type="email" name="email" required="required" placeholder="example@example.com" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Senha:</span>
      <input type="password" name="password" minlength="4" maxlength="12" placeholder="******" />
    </div><!-- input wrapper-->     

<div class="input-wrapper w100 inputFile">
<span>Imagem:</span>
<div class="container-preview">
  <a href="javascript:;" class="remove_image">X</a>
  <i class="fa fa-cloud-upload"aria-hidden="true"></i>
  <img src="" id="image">
</div><!--  container preview-->
<!-- preview trigger -->
<input type="file" name="imagem" id="imageUser" onchange="showImage.call(this)" />
</div><!-- input wrapper-->  
    
    
     <div class="input-wrapper w100">
      <div class="input-wrapper w25">
      <span>Comissão:</span>
      <input type="number" name="comissao" placeholder="%" />
    </div><!-- input wrapper--> 
    </div><!-- input wrapper-->        

     <div class="input-wrapper w100">      
     <span>Gerente:</span>
    <select name="gerente">
      <option value="admin">Admin</option>
      <?php foreach($gerentes as $gerente):?>
         <option value="<?php echo $gerente->slug_nome;?>"><?php echo $gerente->nome;?></option>
    <?php endforeach;?>
    </select>
     </div><!-- input wrapper--> 

    <div class="input-wrapper w50"> 

     <div class="input-wrapper w25">      
     <span>Admin:</span>
     <input id="checkbox1" class="custom_checkbox" name="admin" checked="checked" type="checkbox">
     <label for="checkbox1" data-text-true="Sim" data-text-false="Não"><i></i></label>
     </div><!-- input wrapper--> 

     <div class="input-wrapper w25">
     <span>Gerente:</span>
     <input id="checkbox2" class="custom_checkbox" name="eh_gerente" checked="checked" type="checkbox">
     <label for="checkbox2" data-text-true="Sim" data-text-false="Não"><i></i></label>
     </div><!-- input wrapper--> 

     
    <div class="input-wrapper w25">
     <span>Cambista:</span>
     <input id="checkbox3" class="custom_checkbox" name="cambista" checked="checked" type="checkbox">
     <label for="checkbox3" data-text-true="Sim" data-text-false="Não"><i></i></label>
     </div><!-- input wrapper--> 

    </div><!-- input wrapper--> 
     


    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Salvar" />
     
     </div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 function showImage(obj){
  if(this.files && this.files[0]){
    var obj = new FileReader();
    obj.onload = function(data){
      var image  = document.getElementById('image');
      image.src = data.target.result;
      image.style.display = 'block'; 
      document.querySelector('.remove_image').style.display = 'block';
    }
    obj.readAsDataURL(this.files[0]);
  }
 } 

let remove_image = document.querySelector('.remove_image');
remove_image.addEventListener('click', function(){ 
   
document.getElementById('imageUser').value = '';
document.getElementById('image').src = '#';
document.getElementById('image').style.display = 'none'; 
 document.querySelector('.remove_image').style.display = 'none';
});


document.querySelector('.fa-cloud-upload').addEventListener('click', function(){
  document.getElementById('imageUser').click();
});
</script>