<?php
$razao_social = (isset($info->razao_social)) ? $info->razao_social : null;
$nome_fantasia = (isset($info->nome_fantasia)) ? $info->nome_fantasia : null;
$cnpj = (isset($info->cnpj)) ? $info->cnpj : null;
$ddd = (isset($info->ddd)) ? $info->ddd : null;
$fone = (isset($info->fone)) ? $info->fone : null;
$celular = (isset($info->celular)) ? $info->celular : null;
$email = (isset($info->email)) ? $info->email : null;
$email_secundario = (isset($info->email_secundario)) ? $info->email_secundario : null;
$email_contabilidade = (isset($info->email_contabilidade)) ? $info->email_contabilidade : null;
$cep = (isset($info->cep)) ? $info->cep : null;
$logradouro = (isset($info->logradouro)) ? $info->logradouro : null;
$complemento = (isset($info->complemento)) ? $info->complemento : null;
$numero = (isset($info->numero)) ? $info->numero : null;
?>
<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Dados da empresa</h3>
  <span class="min_box"><a href="<?php echo BASE_URL;?>usuarios"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_cliente" enctype="multipart/form-data" class="form_box">    
 
    <div class="input-wrapper w50">
      <span>Razão social:</span>
      <input type="text" name="razao_social" value="<?php echo $razao_social;?>" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Nome fantasia:</span>
      <input type="text" name="nome_fantasia" value="<?php echo $nome_fantasia;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Cnpj:</span>
      <input type="text" name="cnpj" class="cnpj" value="<?php echo $cnpj;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Ddd:</span>
      <input type="text" name="ddd" required="required" value="<?php echo $ddd;?>"/>
    </div><!-- input wrapper-->

     <div class="input-wrapper w50">
      <span>Fone:</span>
      <input type="text" name="fone" class="telefone" required="required" value="<?php echo $fone;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Celular:</span>
      <input type="text" name="cel" class="phone" required="required" value="<?php echo $celular;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>E-mail:</span>
      <input type="text" name="email" required="required" value="<?php echo $email;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>E-mail secundario:</span>
      <input type="text" name="email_secundario" required="required" value="<?php echo $email_secundario;?>"/>
    </div><!-- input wrapper-->

    <div class="input-wrapper w30">
      <span>E-mail contabilidade:</span>
      <input type="text" name="email_contabilidade" required="required" value="<?php echo $email_secundario;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Cep:</span>
      <input type="text" name="cep" required="required" value="<?php echo $cep;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Logradouro:</span>
      <input type="text" name="logradouro" required="required" value="<?php echo $logradouro;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Complemento:</span>
      <input type="text" name="complemento" required="required" value="<?php echo $complemento;?>"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Núm:</span>
      <input type="text" name="numero" required="required" value="<?php echo $numero;?>"/>
    </div><!-- input wrapper--> 

    


    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-success" value="Atualizar" />
     
     </div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->



<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){

Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {    
    window.location.href = base_url+"usuarios/perfil";   
     
  }
});
}); 
</script>
<?php }?>