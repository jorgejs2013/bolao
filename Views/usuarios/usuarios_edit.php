<?php
$contato = (isset($info_usuario->contato)) ? $info_usuario->contato : '';
$nome = (isset($info_usuario->nome)) ? $info_usuario->nome : '';
$email = (isset($info_usuario->email)) ? $info_usuario->email : '';
$imagem = (isset($info_usuario->imagem)) ? $info_usuario->imagem : '';
$admin = (isset($info_usuario->admin)) ? $info_usuario->admin : '';
$cambista = (isset($info_usuario->cambista)) ? $info_usuario->cambista: '';
$gerentebd = (isset($info_usuario->gerente)) ? $info_usuario->gerente: '';
$eh_gerente = (isset($info_usuario->eh_gerente)) ? $info_usuario->eh_gerente: '';
$comissao = (isset($info_usuario->comissao)) ? $info_usuario->comissao: '';
?>
<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Editar</h3>
  <span class="min_box"><a href="<?php echo BASE_URL;?>usuarios"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_usuario" enctype="multipart/form-data" class="form_box" action="<?php echo BASE_URL;?>usuarios/edit_action/<?php echo $id_user;?>">
    
    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" value="<?php echo $nome;?>" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>Contato:</span>
      <input type="text" name="contato" class="phone" value="<?php echo $contato;?>" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>E-mail:</span>
      <input type="email" name="email" required="required" placeholder="example@example.com" value="<?php echo $email;?>" />
    </div><!-- input wrapper-->       

    <div class="input-wrapper w50">
      <span>Senha:</span>
      <input type="password" name="password" minlength="4" maxlength="12" placeholder="******" />
    </div><!-- input wrapper-->     


<div class="input-wrapper w100 inputFile">
<span>Imagem:</span>
<div class="container-preview">
  <a href="javascript:;" class="remove_image">X</a>
  <i class="fa fa-cloud-upload"aria-hidden="true"></i>
  <img src="<?php echo BASE_URL.$imagem;?>" width="80px" height="80px" id="image" style="display:block;">
</div><!--  container preview-->
<!-- preview trigger -->
<input type="file" name="imagem" id="imageUser" onchange="showImage.call(this)" />
</div><!-- input wrapper--> 

<div class="input-wrapper w100">
      <div class="input-wrapper w25">
      <span>Comissão:</span>
      <input type="number" name="comissao" placeholder="%"  value="<?php echo $comissao;?>" />
    </div><!-- input wrapper--> 
    </div><!-- input wrapper-->   

    
        

      <div class="input-wrapper w100">      
     <span>Gerente:</span>
    <select name="gerente">
      <option value="admin">Admin</option>
      <?php 
      foreach($gerentes as $gerente):
       $selected = ($gerente->slug_nome == $gerentebd) ? 'selected="selected"': '';
      ?>
         <option <?php echo $selected;?> value="<?php echo $gerente->slug_nome;?>"><?php echo $gerente->nome;?></option>
       <?php endforeach;?>
    </select>
     </div><!-- input wrapper--> 

     <div class="input-wrapper w50"> 

    <div class="input-wrapper w25">
     <span>Admin:</span>
     <input id="checkbox1" class="custom_checkbox" name="admin" <?php echo ($admin == '1') ? 'checked="checked"': '';?>  type="checkbox">
     <label for="checkbox1" data-text-true="Sim" data-text-false="Não"><i></i></label>
     </div><!-- input wrapper--> 

     <div class="input-wrapper w25">
     <span>Gerente:</span>
     <input id="checkbox2" class="custom_checkbox" name="eh_gerente" <?php echo ($eh_gerente == 'S') ? 'checked="checked"': '';?> type="checkbox">
     <label for="checkbox2" data-text-true="Sim" data-text-false="Não"><i></i></label>
     </div><!-- input wrapper--> 

     
    <div class="input-wrapper w25">
     <span>Cambista:</span>
     <input id="checkbox3" class="custom_checkbox" <?php echo ($cambista == 'S') ? 'checked="checked"': '';?>  name="cambista" checked="checked" type="checkbox">
     <label for="checkbox3" data-text-true="Sim" data-text-false="Não"><i></i></label>
     </div><!-- input wrapper--> 

    </div><!-- input wrapper--> 
  

     


    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-alert" value="Atualizar" />
     
     </div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_usuario').parsley();
 }); 
</script>

<script>
 function showImage(obj){
  if(this.files && this.files[0]){
    var obj = new FileReader();
    obj.onload = function(data){
      var image  = document.getElementById('image');
      image.src = data.target.result;
      image.style.display = 'block'; 
      document.querySelector('.remove_image').style.display = 'block';
    }
    obj.readAsDataURL(this.files[0]);
  }
 } 

let remove_image = document.querySelector('.remove_image');
remove_image.addEventListener('click', function(){ 
   
document.getElementById('imageUser').value = '';
document.getElementById('image').src = '#';
document.getElementById('image').style.display = 'none'; 
 document.querySelector('.remove_image').style.display = 'none';
});


document.querySelector('.fa-cloud-upload').addEventListener('click', function(){
  document.getElementById('imageUser').click();
});
</script>