<section class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Editar</h3>
  
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_reset" enctype="multipart/form-data" class="form_box">
    
    <div class="input-wrapper w100">
      <span>Senha anterior:</span>
      <input type="password" name="old_senha" placeholder="*****" data-parsley-required="true" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w100">
      <span>Nove senha:</span>
      <input type="password" name="senha" id="password" data-parsley-length="[6,12]"  placeholder="*****"/>
    </div><!-- input wrapper--> 

    <div class="input-wrapper w100">
      <span>Confirmar senha:</span>
      <input type="password" name="csenha" data-parsley-length="[6,12]" data-parsley-equalto="#password" placeholder="*****" />
    </div><!-- input wrapper--> 
  


    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn btn-alert" value="Atualizar" />
     
     </div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_reset').parsley();
 }); 
</script>
<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>';
$(document).ready(function(){
Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {    
    window.location.href = base_url+"usuarios/reset";   
     
  }
});

}); 
</script>
<?php }?>