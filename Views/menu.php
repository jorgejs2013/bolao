<dl class="js-accordion">

<dt class="title_menu"><i class="fa fa-bar-chart-o icon-left" aria-hidden="true" style="color: #fff;"></i><a href="<?php echo BASE_URL;?>" style="color: #fff;">Painel de controle</a></dt>


<?php if($viewData['user']->hasPermission('funcionario')):?>
<dt><i class="fa fa-folder-open-o icon-left" aria-hidden="true"></i>Cadastros <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php echo BASE_URL;?>unidade">Unidade</a>
<a href="<?php echo BASE_URL;?>categorias">Categoria</a>
<a href="<?php echo BASE_URL;?>grupo">Grupos</a>
<a href="<?php echo BASE_URL;?>contato">Contatos</a>
<a href="<?php echo BASE_URL;?>empresa">Empresa</a>
<a href="<?php echo BASE_URL;?>produtos">Produtos</a>
<a href="<?php echo BASE_URL;?>formasPagamento">Formas pagamento</a>
<a href="<?php echo BASE_URL;?>contas">contas</a>
</dd>


<dt><i class="fa fa-search icon-left" aria-hidden="true"></i>Consultas <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php echo BASE_URL;?>produtos/embaixa">Produtos com baixa</a>
</dd>
<!-- 
<dt><i class="fa fa-shopping-cart icon-left" aria-hidden="true"></i>Compras <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php //echo BASE_URL;?>solicitacao">Solicitação</a>	
<a href="<?php //echo BASE_URL;?>cotacao">Cotação</a>
<a href="<?php //echo BASE_URL;?>ordemcompra">Ordem de compra</a>
</dd> -->

<dt><i class="fa fa-upload icon-left" aria-hidden="true"></i>Entradas <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php echo BASE_URL;?>ordemcompra/avulsa">Avulsa</a>
<!-- <a href="<?php //echo BASE_URL;?>entradaordem">Ordem entrada</a> -->
</dd>

<dt><i class="fa fa-briefcase icon-left" aria-hidden="true"></i>Estoque <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php echo BASE_URL;?>estoqueproduto">Produto</a>	
<a href="<?php echo BASE_URL;?>tipomovimento">Tipo de movimento</a>
</dd>

<!-- <dt><i class="fa fa-sign-out icon-left" aria-hidden="true"></i>Saídas <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php //echo BASE_URL;?>saida">Lista</a>
</dd> -->

<?php endif;?>

<?php if($viewData['user']->hasPermission('cliente')):?>
<!-- <dt><i class="fa fa-clone icon-left" aria-hidden="true"></i>Pedidos <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php //echo BASE_URL;?>pedido">Pedidos</a>
</dd>
<?php //endif;?>

<?php //if($viewData['user']->hasPermission('funcionario')):?>
<dt><i class="fa fa-shopping-cart icon-left" aria-hidden="true"></i>Pedidos<i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php //echo BASE_URL;?>pedido/pedidos">Pedidos</a>
<a href="<?php //echo BASE_URL;?>compras">Compras</a>	
<a href="<?php //echo BASE_URL;?>compras">Importar Nota</a>
</dd>
 -->

<dt><i class="fa fa-cc icon-left" aria-hidden="true"></i>Vendas <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php echo BASE_URL;?>vendas">PDV</a>		
</dd>
<?php endif;?>


<?php if($viewData['user']->hasPermission('admin')):?>
<dt><i class="fa fa-usd icon-left" aria-hidden="true"></i>NFE <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php echo BASE_URL;?>config/edit">Configurações da nota</a>
<a href="<?php echo BASE_URL;?>nfe/create">Nova Nota</a>
<a href="<?php echo BASE_URL;?>notafiscal">Listar Notas</a>
</dd>

<dt><i class="fa fa-usd icon-left" aria-hidden="true"></i>Financeiro <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
 <!-- <a href="<?php //echo BASE_URL;?>pedidofinanceiro">Pedidos</a> -->
 <a href="<?php echo BASE_URL;?>financeiro/receber">A receber</a>
 <!-- <a href="<?php //echo BASE_URL;?>historicofinanceiro">Histórico Financeiro</a> -->
 <a href="<?php echo BASE_URL;?>contasareceber">Contas a receber</a>
 <a href="<?php echo BASE_URL;?>contasapagar">Contas a pagar</a>
</dd>

<?php endif;?>

<dt><i class="fa fa-area-chart icon-left" aria-hidden="true"></i>Relatórios <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<!-- <a href="<?php //echo BASE_URL;?>report/compras">Compras</a> -->
</dd>

<?php if($viewData['user']->hasPermission('admin')):?>
<dt><i class="fa fa-gears icon-left" aria-hidden="true"></i>Configurações <i class="fa fa-angle-down icon-right" aria-hidden="true"></i></dt>
<dd>
<a href="<?php echo BASE_URL;?>usuarios">Usuários</a>
<a href="<?php echo BASE_URL;?>permissions">Permissoes</a>
</dd>
<?php endif;?>
<dt>
<a href="<?php echo BASE_URL;?>usuarios/logout"><i class="fa fa-power-off icon-left" aria-hidden="true"></i>Sair</a>
</dt>

</dl>	