<?php
namespace Core;

class Core {

	public function run() {	

       $url = '/'.(isset($_GET['q'])? $_GET['q']: '');

        $url = $this->limparUrl($url);//limpa caracteres e acentos 
        $url = $this->writeSpace($url);  //remove espaços em branco da url 

        $url = $this->checkRoutes($url); 
       
		$params = array();
		if(!empty($url) && $url != '/') {
			$url = explode('/', $url);			
			array_shift($url);

			$currentController = $url[0].'Controller';
			array_shift($url);

			if(isset($url[0]) && !empty($url[0])) {
				$currentAction = $url[0];
				array_shift($url);
			} else {
				$currentAction = 'index';
			}

			if(count($url) > 0) {
				$params = $url;
			}

		} else {
			$currentController = 'HomeController';
			$currentAction = 'index';
		}

		$currentController = ucfirst($currentController);

		if(!file_exists('Controllers/'.$currentController.'.php') || !method_exists($currentController, $currentAction)){
			$currentController = 'NotFoundController';
			$currentAction = 'index';
		}
		
        $newController = $currentController;
		$c = new $newController();

		call_user_func_array(array($c, $currentAction), $params);
}//RUN

public function writeSpace($url){
$newUrl = lcfirst(str_replace(" ", "", strtolower($url)));
return $newUrl;
}//writeSpace


public function limparUrl($url){
$newUrl = strip_tags(trim($url));

$format = array();
$format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()-+={[}]?;:.,\\\'<>°ºª';	
$format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                ';
$newUrl = strtr(utf8_decode($newUrl), utf8_decode($format['a']), $format['b']);

return $newUrl;
}//limparUrl



public function checkRoutes($url){
global $routes;

foreach($routes as $pt => $newurl){
//identifica os argumento e substitui por regex
	$pattern  = preg_replace('(\{[a-z0-9]{1,}\})', '([a-z0-9-]{1,})', $pt);
//faz o match da URL
    if(preg_match('#^('.$pattern.')*$#i', $url, $matches) === 1){
      array_shift($matches);
      array_shift($matches);

 //pega todos os argumentos para associar
      $itens = array();
      if(preg_match_all('(\{[a-z0-9]{1,}\})', $pt, $m)){
       $itens = preg_replace('(\{|\})', '', $m[0]);
      }
   //Faz a associação
   $arg = array();
   foreach ($matches as $key => $match) {
   	$arg[$itens[$key]] = $match;
   }

    //monta a nova url
   foreach($arg as $argkey => $argvalue){
   	$newurl = str_replace(':'.$argkey, $argvalue, $newurl);
   }


   $url = $newurl;
   break; 

    }

}
return $url;

}//checkRoutes

}