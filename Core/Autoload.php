<?php
namespace Core;

class Autoloader{

public function registrar(){
spl_autoload_register(array($this, "carregar"));
}//registrar

public function carregar($nomeDaClasse){
$classe = str_replace("\\", DIRECTORY_SEPARATOR, ltrim($nomeDaClasse,"\\")).".php";

if(file_exists($classe)):
return include_once($classe);
endif;

//strem_resolve_include_path pega o caminho completo da classe
if($caminhoCompleto =  stream_resolve_include_path($classe)):
 return include_once($caminhoCompleto);	
endif;
}//carregar

}