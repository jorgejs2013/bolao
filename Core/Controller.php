<?php
namespace Core;

class Controller {

	protected $db;		
	
	public function loadView($viewName, $viewData = array()) {
		extract($viewData, EXTR_PREFIX_SAME, '_');		
		include 'Views/'.$viewName.'.php';
	}

	public function loadTemplate($viewName, $viewData = array()) {
		include 'Views/template.php';
	}

	public function loadViewInTemplate($viewName, $viewData) {	
		extract($viewData, EXTR_PREFIX_SAME, '_');		
		include 'Views/'.$viewName.'.php';
	}	

	public function loadLibrary($lib, $viewData){
		if(file_exists('libraries/'. $lib.'.php')){
		  extract($viewData, EXTR_PREFIX_SAME, '_');
          include 'libraries/'. $lib.'.php';
		}		
	}//loadLibrary


public function limpaCampo($campo = null){

if($campo != null){

 if(isset($campo) && !empty($campo)){
  $novoCampo = addslashes(trim($campo));
  return $novoCampo;
}
}else{
  return null;
}
}//limpaCampo


public function pesquisaPermissao($array, $tabela, $acao){ 
 if(is_array($array)){

  foreach($array as $key => $permissoes){ 
  
    if($permissoes['tabela'] == $tabela && $permissoes['acao'] == $acao){
    return true;
    }
    continue;
    
  } 
  return false;
 }//verifica se é array
}//pesquisaPermissao  

public function limpaString($string){
$arrray = array();
$arr1 = '/-_.,';
$arr2 = '     ';

$data = strtr(utf8_decode($string), utf8_decode($arr1), utf8_decode($arr2));
$data = strip_tags($data); 
$data = str_replace(' ', '', $data);
$data = str_replace(array('-----', '----', '---','--'),'-', $data);

return strtolower(utf8_encode($data));
}//limpaString

	public function debug($array){
     echo "<pre>";
     print_r($array);
     exit;
	}//debug

	public function redirect($target){
       return header("Location:{$target}");
     }//redirect

    public function back(){
	$previous = "javascript:history.go(-1)";
	if(isset($_SERVER['HTTP_REFERER'])){
		$previous = $_SERVER['HTTP_REFERER'];
	}

	return header("Location: {$previous}");
 
    }//back


    public function converterMoeda($valor){     
    	$newValor = number_format( (float) $valor,2,',','.');  
      
      return $newValor;   
     }//converterMoeda


     public function converterData($data, $pattern = '-'){     
       $novaData = explode($pattern, $data);
       $novaData = $novaData[2].'-'.$novaData[1].'-'.$novaData[0];
       return $novaData;
     }//converterData


     public function valor($valor) {
       $verificaPonto = ".";
       if(strpos("[".$valor."]", "$verificaPonto")):      
           $valor = str_replace('.','', $valor);
           $valor = str_replace(',','.', $valor);
           else:           	
             $valor = str_replace(',','.', $valor);   
       endif;

       return $valor;
    }//valor

	public function gerarNome($total_caracteres){

     $caracteres = 'ABCDEFGHIJKLMNOPQRSTUWXYZ';
     $caracteres .= 'abcdefghijklmnopqrstuwxyz';
     $caracteres .= '0123456789';
     $max = strlen($caracteres)-1;
     $senha = null;
     for($i=0; $i < $total_caracteres; $i++){
        $senha .= $caracteres{mt_rand(0, $max)};
    }
    return $senha;
}//function gerarNome


public function geraCodigo($tamanho = 10, $maiusculas = false, $numeros = true, $simbolos = false)
{
// Caracteres de cada tipo
$lmin = 'abcdefghijklmnopqrstuvwxyz';
$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$num = '1234567890';
$simb = '!@#$%*-';
// Variáveis internas
$retorno = '';
$caracteres = '';
// Agrupamos todos os caracteres que poderão ser utilizados
//descomente a linha de baixao para gerar letras misturadas
//$caracteres .= $lmin; 
if ($maiusculas) $caracteres .= $lmai;
if ($numeros) $caracteres .= $num;
if ($simbolos) $caracteres .= $simb;
// Calculamos o total de caracteres possíveis
$len = strlen($caracteres);
for ($n = 1; $n <= $tamanho; $n++) {
// Criamos um número aleatório de 1 até $len para pegar um dos caracteres
$rand = mt_rand(1, $len);
// Concatenamos um dos caracteres na variável $retorno
$retorno .= $caracteres[$rand-1];
}
return $retorno;
}//geraSenha
}