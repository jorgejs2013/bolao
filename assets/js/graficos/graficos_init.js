//Primeiro grafico
google.charts.load('current', {packages:['corechart']});
google.charts.setOnLoadCallback(drawChartPie)

function drawChartPie(){

$.getJSON(base_url+'home/alunosPorTurno/', function(resposta){

var valores = Object.values(resposta);

 var arrayTurno = [
  ['Turno', 'Qtde'],  
];

  for(let i in valores){
  let chaves  = Object.keys(valores[i]);
  let values  = Object.values(valores[i]);

  var arr = new Array(String(chaves), parseInt(values[0]));
  arrayTurno.push(arr);
  }

let container = document.querySelector("#pieChart");
let data = new google.visualization.arrayToDataTable(arrayTurno);

let options = {
  title: 'Alunos por turno',
  height:240  
}

let chart = new google.visualization.PieChart(container);//Column - Bar - Line - Pie
chart.draw(data, options);

});//getJSON
}//drawChartPie

/******************/
//Fim do grafico 01
/******************/


/**************/
//Gráfico 02
/***************/
//Grafico numero 2
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawTopX);

function drawTopX() {

var data = new google.visualization.DataTable();
data.addColumn('timeofday', 'Hora do dia');
data.addColumn('number', 'Aluno');
data.addColumn('number', 'Curso');

data.addRows([
  [{v: [8, 0, 0], f: '8 am'}, 1, .25],
  [{v: [9, 0, 0], f: '9 am'}, 2, .5],
  [{v: [10, 0, 0], f:'10 am'}, 3, 1],
  [{v: [11, 0, 0], f: '11 am'}, 4, 2.25],
  [{v: [12, 0, 0], f: '12 pm'}, 5, 2.25],
  [{v: [13, 0, 0], f: '1 pm'}, 6, 3],        
]);

var options = {
  chart: {
    title: 'Alunos por curso',
    subtitle: 'baseados de Janeiro a Dezembro'
},

axes: {
  x: {
    0: {side: 'top'}
  }
},

hAxis: {
  title: 'Hora do dia',
  format: 'h:mm a',
  viewWindow: {
    min: [7, 30, 0],
    max: [17, 30, 0]
  }
},

vAxis: {
    title: 'Rating (Escala de 1-10)'
  }
};

let materialChart = new google.charts.Bar(document.getElementById('chart2'));
materialChart.draw(data, options);

}
/*****************/
//Fim do grafico 02
/****************/


/**************/
//Grafico 03
/*************/
google.charts.load('current', {'packages':['table']});
google.charts.setOnLoadCallback(drawTable);

function drawTable() {

$.getJSON(base_url+'home/aniversariantes/', function(resposta){

let valores = Object.values(resposta);
var aniver = new Array();

for(let i  in valores){
let vlr = Object.values(valores[i]);
aniver.push(vlr);
}

  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Aluno');
  data.addColumn('string', 'Data');  
  data.addRows(aniver);

 var options = {
 width:'100%',
 height:'100%',
 showRowNumber:true,
 pageSize:10,
 pagingButtons:'auto'  
 };

var table = new google.visualization.Table(document.getElementById('chart3'));
table.draw(data,options);

});//getJson
}

/*******************/
// Quarto Grafico
/*******************/
google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawChart2);

function drawChart2() {

$.getJSON(base_url+'home/alunosPorCurso/', function(resposta){

let valores = Object.values(resposta);
var cursos = new Array();

for(let i in valores){
  let chaves  = Object.keys(valores[i]);
  let values  = Object.values(valores[i]);

  var arr = new Array(String(chaves), parseInt(values));
  cursos.push(arr); 
}

var data = new google.visualization.DataTable();
data.addColumn('string', 'Curso');
data.addColumn('number', 'Qtde');
data.addRows(cursos);

 var columnChart = new google.visualization.ColumnChart(document.getElementById("chart4"));
 columnChart.draw(data, null);

});//getJson
}


/*****************/
//Quinto grafico
/****************/
google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawChart5);

function drawChart5(){
var data = new google.visualization.arrayToDataTable([
 ['Ano', 'Venda', 'Expectativa', 'profit', { role: 'annotation' }],
 ['2014', 1000, 400,200, 'cu'],
 ['2015', 1170, 460, 250, 'dd'],
 ['2016', 600, 1120, 300, 'pt'],
 ['2017', 1030, 540, 350, 'us']
]);

var options = {
 chat:{
  title:'Company perfomance',
  subtitle:'Sales, Expenses, and profit: 2017-2017'
 }
};


let chart = new google.charts.Bar(document.getElementById('columnChart'));
chart.draw(data, options);
}


/******************/
//Fim do grafico 05
/******************/


/**********************/
//Grafico numero 6
/*********************/
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Ano', 'Vendas', 'Expectativa'],
    ['2013',  1000,      400],
    ['2014',  1170,      460],
    ['2015',  660,       1120],
    ['2016',  1030,      540]
]);

var options = {
  title: 'Perfomance da escola ',
  hAxis: {title: 'Ano',  titleTextStyle: {color: '#333'}},
  vAxis: {minValue: 0}
};

let chart = new google.visualization.AreaChart(document.getElementById('areaChart'));
chart.draw(data, options);
}
/******************/
//Fim do grafico 06
/******************/ 