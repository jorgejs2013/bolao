// Forma 1
$("#checkTodos").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});
 
// // Forma 2
// $("#checkTodos").change(function () {
//     $("input:checkbox").prop('checked', $(this).prop("checked"));
// });
 
// // Forma 3
// var checkTodos = $("#checkTodos");
// checkTodos.click(function () {
//   if ( $(this).is(':checked') ){
//     $('input:checkbox').prop("checked", true);
//   }else{
//     $('input:checkbox').prop("checked", false);
//   }
// });