$(document).ready(function(){
	$('.calendario').flatpickr({	  	
		//enableTime: true,
		//minDate:"today",
		//maxDate: new Date().fp_incr(14) // 14 days from now
		
		//se for para horas
		//enableTime: true,
        //noCalendar: true,
        //dateFormat: "H:i",
        //time_24hr: true

		"locale": "pt"			
	});


	$('.horario').flatpickr({
	   enableTime: true,
	   noCalendar: true,
       dateFormat: "H:i",
       time_24hr:true,
       "locale": "pt"
	});
})