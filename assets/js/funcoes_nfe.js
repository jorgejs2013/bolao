function gerarxml(id_nota){

$.ajax({
url:base_url+"nfe/gerarXML/"+id_nota,
type:"POST",
data:{id_nota:id_nota},
dataType:'json',
success:function(data){

 if(data.error  > 0){  	 	
 	$('.alert-box').addClass('alert-success'); 	
 	$('.alert-box').html(data.msg);	
 }else{
   $('.alert-box').css('display', 'block');	
   $('.alert-box').removeClass('alert-success');

   $('.alert-box').addClass('alert-error');   
   $('.alert-box').html(data.msg);

    //escondeBox();
 }

 },
beforeSend:function(){
 $('.alert-box').css("display", "block");
 $('.alert-box').addClass('alert-info');
 $('.alert-box').html('Carregando Aguarde...');
},
error:function(data){ 
$('.alert-box').css("display", "block");
$('.alert-box').addClass('alert-error');
$('.alert-box').html(data.msg);

//escondeBox();
}

});//ajax	

}//gerarxml

function transmitir(id_nota){
$.ajax({
url:base_url+"nfe/transmitir/"+id_nota,
type:"POST",
data:{id_nota:id_nota},
dataType:'json',
success:function(data){

 if(data.error  > 0){  	 	
 	$('.alert-box').addClass('alert-success'); 	
 	$('.alert-box').html(data.msg);	
}else{
   $('.alert-box').css('display', 'block');	
   $('.alert-box').removeClass('alert-success');

   $('.alert-box').addClass('alert-error');   
   $('.alert-box').html(data.msg);

    //escondeBox();
 }

},
beforeSend:function(){
 $('.alert-box').css("display", "block");
 $('.alert-box').addClass('alert-info');
 $('.alert-box').html('Carregando Aguarde...');
},
error:function(data){ 
$('.alert-box').css("display", "block");
$('.alert-box').addClass('alert-error');
$('.alert-box').html(data.msg);
//escondeBox();
}

});//ajax	
}//transmitir

function escondeBox(){

setTimeout(function(){
   $('.alert-box').hide();
   location.reload();
  },3000);

}//escondeBox