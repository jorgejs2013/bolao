$('#add_data').click(function(){
var pasta = $(this).attr('data-pasta');

var options = {
ajaxPrefix: '',	
};


new Dialogify(base_url+'Views/arquivos/newPasta.php', options)
.title('Adicionar nova pasta')
.buttons([
{
	text:'Cancelar',
	click:function(e){
		this.close();
	}
},
{
	text:'Criar',
	type:Dialogify.BUTTON_PRIMARY,
	click:function(e){
		
		var form_data = new FormData();		
		form_data.append('nome', $('#nome_pasta').val());
		form_data.append('rota', pasta);
		
		$.ajax({
			method:"POST",
			url:base_url+'arquivos/add',
			data:form_data,
		    dataType:'json',
		    contentType:false,
		    cache:false,
		    processData:false,
		    success:function(data){	

		  console.info(data);  	    	
              if(data.error != ''){
$('#form_response').html('<div class="alert-danger">'+data.error+'</div>');

              }else{
$('#form_response').html('<div class="alert-success">'+data.error+'</div>');
alert('Pasta Criada com sucesso!');
location.reload();
//reload a tela 
              }
		    }
		});//ajax
	}
}
]).showModal();	


});	

// Processo para renomear pasta
$('.rename_dir').click(function(){
var old_nome = $(this).attr('data-nome');
var id = $(this).attr('data-id');

var options = {
ajaxPrefix: '',	
ajaxData:{pasta:old_nome}
};


new Dialogify(base_url+'Views/arquivos/renamePasta.php', options)
.title('Renomear')
.buttons([
{
	text:'Cancelar',
	click:function(e){
		this.close();
	}
},
{
	text:'Atualizar',
	type:Dialogify.BUTTON_PRIMARY,
	click:function(e){
		
		var form_data = new FormData();		
		form_data.append('id', id);
		form_data.append('nome_old', old_nome);
		form_data.append('nome', $('#nome_arquivo').val());
		
		$.ajax({
			method:"POST",
			url:base_url+'arquivos/renomear',
			data:form_data,
		    dataType:'json',
		    contentType:false,
		    cache:false,
		    processData:false,
		    success:function(data){	
		    console.info(data);	    	
              if(data.error != ''){
$('#form_response').html('<div class="alert-danger">'+data.error+'</div>');

              }else{
$('#form_response').html('<div class="alert-success">'+data.error+'</div>');
alert('Renomeado com sucesso!');
location.reload();
//reload a tela 
              }
		    }
		});//ajax
	}
}
]).showModal();	

});



$('.btnUpload').click(function(){

$('#file').click();

$('#file').on('change', event=>{

var rota = $(this).attr('data-rota');
var files = event.target.files;

[...files].forEach(file=>{

	var formData = new FormData();
	formData.append('arquivo', file);
	formData.append('rota', rota);

	$.ajax({
      url:base_url+'arquivos/uploadArquivos',
      type:'POST',
      data:formData,
      dataType:'json',
      processData:false,
      contentType:false,
      success:function(data){
      	
      	if(data == 'ok'){
      		
      	}else{
      		alert('Erro ao enviar arquivo');
      	}
      }
	});//ajax
	
})
alert('Todos os arquivos foram enviados com sucesso');
location.reload();

});

});//quando clica no botao de upload