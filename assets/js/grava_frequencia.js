$('#adicionar').click(function(){

var val = new Array();
let conta =  $('tbody tr').length;
var diario = $(this).attr('data-diario');


for(let i = 0;i< conta; i++){

let cod     = $('tbody tr').eq(i).find('td').eq(0).text();
let cliente = $('tbody tr').eq(i).find('td').eq(1).text();
let freq    = $('tbody tr').eq(i).find('td').find('input').is(':checked');
let obs     = $('tbody tr').eq(i).find('td').find('.obs').val();

if(freq == true){
	var presenca = 'presente';	
}else{
	var presenca = 'ausente';	
}

val.push( {"diario":diario, "id": cod, "aluno":cliente, "presenca":presenca, "obs":obs} );
}//fim do laço for


$.ajax({
url:base_url+'diario/gravaFrequencia',
method:'POST',
data: {itens: val, diario:diario},
cache: false,
success:function(res){

if(res == '1'){
	alert('Frequência gravada com sucessso');
	location.href = base_url+'diario';
}else{
	alert('Erro ao gravar frequência tente novamente!');
}

},
error:function(error){
alert('Erro ao processar, tente novamente mais tarde!');
location.href = base_url+'diario';
}
});

}); 