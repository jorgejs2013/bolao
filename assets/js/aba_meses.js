var mes_ativo = 'Jan';

function mostraRemessa(mes, banco){
$.ajax({
url:base_url+"faturas/remessamensal",
type:'POST',
data:{mes:mes, banco:banco},
dataType:'json',
success:function(data){
let html = '';
html += `
<div class="container_shrinker">
<table class="table shrink">
<thead>
<tr>
<th align="left" width="80">
<input type="checkbox" name="todos" id="todos" value="todos" onclick="marcardesmarcar(this);" />Marcar todos</th>
	<th>Código</th>	
  <th>Fatura</th>
  <th>Responsavel</th>	
  <th class="shrink-xs">Vencimento</th>
  <th class="shrink-xs">Valor</th>  
</tr>	
</thead>	

<tbody>
`;	

for(let i in data){
var datac = data[i].venc_parcela.split('-');
let novaData = datac[2]+'/'+datac[1]+'/'+datac[0];	
html += `
<tr>
<td align="left">
<input type="checkbox" name="id_remessa[]" class="marcar" id="marcar" value="${data[i].id_fatura_parcela}"></td>
<td>${data[i].id_fatura_parcela}</td>	
<td>${data[i].id_fatura}</td>
<td>${data[i].nome}</td>
<td>${novaData}</td>
<td>R$ ${data[i].valor_parcela}</td> 
</tr>
`;
}

html += `
</tbody>
</table>
</div>
`;

$('section[data-m='+mes+']').html('');
$('section[data-m='+mes+']').html(html);
}

});
}//mostraRemessa


$('.link_aba').click(function(){  
   var mes = $(this).attr('data-mes');

  $('section[data-m='+mes+']').html('');
  setTimeout(function(){
    mes_ativo = mes; 
   var banco = $('#banco').val();    
   mostraRemessa(mes, banco);
  
  },1000);

   
});


function marcardesmarcar(obj){
var checado = $(obj).is(':checked');

if(checado == true){	  
marcar(true);
}else{  
	marcar(false);
}
}//marcardesmarcar

function marcar(bool){
$('.marcar').each(	
    function(){
       $('.marcar').prop("checked", bool);               
    }
);	
}//marcar

//quando muda o campo do select
$(document).on('change', "#banco", function(){
let banco = $(this).val();

if(banco == '1'){
$('.btn-brasil').fadeOut('fast');
$('.btn-caixa').fadeOut('slow');
$('.btn-bradesco').fadeIn('slow');
}else if(banco == '3'){
$('.btn-bradesco').fadeOut('fast');
$('.btn-caixa').fadeOut('slow');
$('.btn-brasil').fadeIn('slow');
}else if(banco == '4'){
  $('.btn-bradesco').fadeOut('fast');
  $('.btn-brasil').fadeOut('fast');
  $('.btn-caixa').fadeIn('slow');
}

mostraRemessa(mes_ativo, banco);
});

mostraRemessa('Jan', '1');