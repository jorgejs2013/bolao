 function showImage(obj){
  if(this.files && this.files[0]){
    var obj = new FileReader();
    obj.onload = function(data){
      var image  = document.getElementById('image');
      image.src = data.target.result;
      image.style.display = 'block'; 
      document.querySelector('.remove_image').style.display = 'block';
    }
    obj.readAsDataURL(this.files[0]);
  }
 } 

let remove_image = document.querySelector('.remove_image');
remove_image.addEventListener('click', function(){ 
   
document.getElementById('imageUser').value = '';
document.getElementById('image').src = '#';
document.getElementById('image').style.display = 'none'; 
 document.querySelector('.remove_image').style.display = 'none';
});


document.querySelector('.fa-cloud-upload').addEventListener('click', function(){
  document.getElementById('imageUser').click();
});