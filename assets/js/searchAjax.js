$(document).ready(function(){

function load_data(page, query = ''){	
	$.ajax({
        url:base_url+'turma/fetch',
        method:'POST',
        data:{page:page, query:query},
        success:function(data){
        	
        	$('.conteudo_dinamico').html(data);
        }
	});
}

load_data(1);


//se preferir que o usuario click no botao de enviar
$('#pesq_form').click(function(e){
e.preventDefault();
var query = $('#campo_nome').val();
load_data(1, query);
});

$(document).on('click', '.page-link', function(){
var page = $(this).data('page_number');
var query = $('#campo_nome').val();
load_data(page, query);
});

//se preferir ao digitar usar opção abaixo
// $('#box_pesquisa').keypress(function(){
// var query = $('#box_pesquisa').val();
// load_data(1, query);
// });


});