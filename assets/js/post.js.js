//EXEMPLO PARA TRANSMITIR E GERAR XML DE UMA NF-e
function removeSimbolos(campo) {
    campo = campo.replace(/(\.|\/|\-)/g,"");
    return campo;
}//funcion removeSimbolos

function gerarNFE(obj){
var id_nfe = $(obj).data('nfe');

$.ajax({
url:base_url+'notafiscal/pegardados',
method:'post',
data:{id_nfe:id_nfe},
dataType:'json',
success:function(data){

var arrayItens = new Array();

for(var i in data.itens){
var obj = {
"cod_barras": "SEM GTIN",
"codigo_produto": 1, 
"nome_produto": data.itens[i].xprod,
"ncm": data.itens[i].ncm,
"unidade": "UN",
"cfop": data.itens[i].cfop,
"quantidade": data.itens[i].qcom,
"valor_unitario": data.itens[i].vprod,
"compoe_valor_total": 1
}	

arrayItens.push(obj);
}

var js = {
"venda": { // atributos de identificação, podem servir como um link de sua venda com a NF-e
	"identificacao": 1, //Para indificação da sua venda
	"comentario": "" //algum comentario opcional
},

"documento": {
	"numero_nf": data.nfe.id_nfe, // numero da NF-e
	"natureza_operacao": data.config.natop_padrao, 
	"numero_serie": 1, // numero de série
	"ambiente": 1,//parseInt(data.config.nfe_ambiente), // 1 - Produção, 2 - Homologação
	"info_complementar": "", // opcional info do rodapé da nota
	"consumidor_final": 1, // 1 - sim, 0 - não
	"operacao_interestadual": 1, // 1 operacao interna, 2 - operação interestadual
	"CSC": "GPB0JBWLUR6HWFTVEAS6RJ69GPCROFPBBB8G", // Codigo CSC para emissao de NFC-e
	"CSCid": "000002" // CSCid do CSC para emissao de NFC-e
},
// campo tpNF é igual á 1, ou seja, nota de Saída,
//use um dos CSOSN autorizáveis sendo válido o 102, 103, 300, 400 ou 500.
//Emitente
"emitente": {
"codigo_uf": data.empresa.codigo_estado, // Código da UF emitente consulte https://www.oobj.com.br/bc/article/quais-os-c%C3%B3digos-de-cada-uf-no-brasil-465.html
"razao_social":data.empresa.razao_social, 
"nome_fantasia":data.empresa.nome_fantasia,
"ie": data.empresa.ie, // somente numeros
"cnpj": removeSimbolos(data.empresa.cnpj), // somente numeros
"crt": 1, //  1 - simples nacional, 2 - Empresa optante pelo Simples Nacional com excesso de sublimite de receita bruta fixado pelo UF, 3 - Empresa optante pelo Simples Nacional com excesso de sublimite de receita bruta fixado pelo UF
"csosn": "102", // consulte http://www.ccentral.com.br/web/documentos/CST-CSOSN.pdf
"logradouro":data.empresa.logradouro,
"numero": data.empresa.numero,
"complemento": data.empresa.complemento,
"bairro": data.empresa.bairro,
"nome_municipio": data.empresa.nome_cidade,
"cod_municipio_ibge": data.empresa.ibge_cidade, // código do municipio tabela IBGE
"uf": data.empresa.uf_estado,
"cep": removeSimbolos(data.empresa.cep), // somente numeros
"nome_pais": "Brasil",
"cod_pais": 1058 // código do pais Brasil
},
//Destinatario
"destinatario": {
	"nome": data.contato.nome,
	"tipo": "f",
	"cpf_cnpj": removeSimbolos(data.contato.cpf), // somente numeros
	"ie_rg": "", // somente numeros
	"contribuinte": 1,//
	"logradouro": data.contato.logradouro,
	"numero": data.contato.numero,
	"complemento": data.contato.complemento,
	"bairro": data.contato.bairro,
	"nome_municipio": data.contato.nome_cidade,
	"cod_municipio_ibge": data.contato.ibge_cidade, // código do municipio tabela IBGE
	"uf": data.contato.uf_estado,
	"cep": removeSimbolos(data.contato.cep), // somente numeros
	"nome_pais": "Brasil",
	"cod_pais": 1058 // código do pais Brasil
},

"itens": 
arrayItens
,

"frete": {
	"modelo": 0, // 0 - Contratação do Frete por conta do Remetente (CIF), 1 - Contratação do Frete por conta do Destinatário (FOB), 2 - Terceiros, 3 - Remetente, 4 - Destinatário, 9 – Sem Ocorrência de Transporte
	"valor": 1.2, // valor do frete
	"quantidade_volumes": 1.0, // quantidade de volumes
	"numero_volumes": 1.0, // numero de volumes
	"especie": "TON",
	"placa": "AZE1757", // placa do veiculo somente letras e numeros
	"uf_placa": "PR", // placa do veiculo
	"peso_liquido": 10.0, // peso liquido
	"peso_bruto": 12.0 // peso bruto
},

"responsavel_tecnico": { // dados responsavel tecnico
	"cnpj": "12317294000169", // somente numeros
	"contato": "unixsistem",
	"email": "jorgejs2013@gmail.com",
	"telefone": "7399548378"
},

"pagamento": {
	"tipo": "01", // consulte https://blog.oobj.com.br/campos-de-pagamento-nfe-4-00/
	"indicacao_pagamento": 0 // 0 - a vista, 1 - a prazo, 2 - outros
},

"fatura": {
	"desconto": 0.00,
	"total_nf": 3.5 // total da NF-e
},
	
"duplicatas": [
	{
		"data_vencimento": "2020-04-21", // vencimento da duplicata
		"valor": 3.5 // valor da duplicata
	}	// 1 ou mais duplicatas
],
	
"tributacao": {
	"icms": 0.00, // percentual do icms
	"pis": 0.00, // percentual do pis
	"cofins": 0.00, // percentual do cofins
	"ipi": 0.00 // percentual do ipi
} 

}//variavel js
	
console.log(JSON.stringify(js));	
//transmitirXML(js);	
}//fim do success
});//ajax

}//function gerarNFE

function transmitirXML(json){

console.log(JSON.stringify(json));
if(json != ''){

$.ajax({
url:'http://localhost/apinfe/gerarXml/',//base_url+'notafiscal/transmitir',
method:'POST',
data:JSON.stringify(json),
contentType:'application/json;charset=utf8',
dataType:'json',
success:function(ret){
	alert('enviou');
	console.log(ret);
},
error:function(erro){
	alert('deu erro');
	console.log(erro);
}
});

}else{
	alert('Erro ao transmitir arquivo tente novamente');
}

}//transmitirXML

// var js = {
	
// 	"venda": { // atributos de identificação, podem servir como um link de sua venda com a NF-e
// 		"identificacao": 1, //Para indificação da sua venda
// 		"comentario": "" //algum comentario opcional
// 	},
	
// 	"documento": {
// 		"numero_nf": 663, // numero da NF-e
// 		"natureza_operacao": "Venda de produto de estabelcimento", 
// 		"numero_serie": 1, // numero de série
// 		"ambiente": 2, // 1 - Produção, 2 - Homologação
// 		"info_complementar": "", // opcional info do rodapé da nota
// 		"consumidor_final": 1, // 1 - sim, 0 - não
// 		"operacao_interestadual": 1, // 1 operacao interna, 2 - operação interestadual
// 		"CSC": "GPB0JBWLUR6HWFTVEAS6RJ69GPCROFPBBB8G", // Codigo CSC para emissao de NFC-e
// 		"CSCid": "000002" // CSCid do CSC para emissao de NFC-e
// 	},
	
// 	"emitente": {
// 		"codigo_uf": 41, // Código da UF emitente consulte https://www.oobj.com.br/bc/article/quais-os-c%C3%B3digos-de-cada-uf-no-brasil-465.html
// 		"razao_social": "MANFER MUSA QASEN ME", 
// 		"nome_fantasia": "MUSA PAES",
// 		"ie": "9073142943", // somente numeros
// 		"cnpj": "26166350000116", // somente numeros
// 		"crt": 1, //  1 - simples nacional, 2 - Empresa optante pelo Simples Nacional com excesso de sublimite de receita bruta fixado pelo UF, 3 - Empresa optante pelo Simples Nacional com excesso de sublimite de receita bruta fixado pelo UF
// 		"csosn": "101", // consulte http://www.ccentral.com.br/web/documentos/CST-CSOSN.pdf
// 		"logradouro": "Aldo ribas",
// 		"numero": 125,
// 		"complemento": "",
// 		"bairro": "centro",
// 		"nome_municipio": "jaguariaiva",
// 		"cod_municipio_ibge": "4112009", // código do municipio tabela IBGE
// 		"uf": "PR",
// 		"cep": "84200000", // somente numeros
// 		"nome_pais": "Brasil",
// 		"cod_pais": 1058 // código do pais Brasil
// 	},
	
// 	"destinatario": {
// 		"nome": "Michel Bueno de Mello",
// 		"tipo": "j",
// 		"cpf_cnpj": "08543628000145", // somente numeros
// 		"ie_rg": "9041143751", // somente numeros
// 		"contribuinte": 1,
// 		"logradouro": "Aldo ribas",
// 		"numero": 125,
// 		"complemento": "",
// 		"bairro": "centro",
// 		"nome_municipio": "jaguariaiva",
// 		"cod_municipio_ibge": "4112009", // código do municipio tabela IBGE
// 		"uf": "PR",
// 		"cep": "84200000", // somente numeros
// 		"nome_pais": "Brasil",
// 		"cod_pais": 1058 // código do pais Brasil
// 	},
	
// 	"itens": [
// 		{
// 			"cod_barras": "SEM GTIN", // exemplo de barras código valido 7891000061190 [13 dgitos]
// 			"codigo_produto": 1, // código do produto do seu banco de dados
// 			"nome_produto": "Coca-cola lata 350 ml", // nome produto
// 			"ncm": "44071100", // numero ncm conulte https://enotas.com.br/blog/ncm-nomenclatura-comum-do-mercosul/
// 			"unidade": "UN", // unidade de medida
// 			"cfop": "5101", // numero de CFOP
// 			"quantidade": 1.00, // quantidade
// 			"valor_unitario": 3.5, // valor uitario
// 			"compoe_valor_total": 1 // Indica se valor do Item entra no valor total da NF-e, 0 - não, 1 - sim
// 		} // 1 ou mais itens

// 	],
	
// 	"frete": {
// 		"modelo": 0, // 0 - Contratação do Frete por conta do Remetente (CIF), 1 - Contratação do Frete por conta do Destinatário (FOB), 2 - Terceiros, 3 - Remetente, 4 - Destinatário, 9 – Sem Ocorrência de Transporte
// 		"valor": 1.2, // valor do frete
// 		"quantidade_volumes": 1.0, // quantidade de volumes
// 		"numero_volumes": 1.0, // numero de volumes
// 		"especie": "TON",
// 		"placa": "AZE1757", // placa do veiculo somente letras e numeros
// 		"uf_placa": "PR", // placa do veiculo
// 		"peso_liquido": 10.0, // peso liquido
// 		"peso_bruto": 12.0 // peso bruto
// 	},
	
// 	"responsavel_tecnico": { // dados responsavel tecnico
// 		"cnpj": "08543628000145", // somente numeros
// 		"contato": "Slym",
// 		"email": "suporte@slym.com",
// 		"telefone": "43996354674"
// 	},
	
// 	"pagamento": {
// 		"tipo": "01", // consulte https://blog.oobj.com.br/campos-de-pagamento-nfe-4-00/
// 		"indicacao_pagamento": 0 // 0 - a vista, 1 - a prazo, 2 - outros
// 	},
	
// 	"fatura": {
// 		"desconto": 0.00,
// 		"total_nf": 3.5 // total da NF-e
// 	},
	
// 	"duplicatas": [
// 		{
// 			"data_vencimento": "2020-04-21", // vencimento da duplicata
// 			"valor": 3.5 // valor da duplicata
// 		}	// 1 ou mais duplicatas
// 	],
	
// 	"tributacao": {
// 		"icms": 0.00, // percentual do icms
// 		"pis": 0.00, // percentual do pis
// 		"cofins": 0.00, // percentual do cofins
// 		"ipi": 0.00 // percentual do ipi
// 	} 
	
// };
// // *** FIM EXEMPLO PARA TRANSMITIR E GERAR XML DE UMA NF-e






// // EXEMPLO PARA CONSULTAR UMA NF-e POR NUMERO
// var js = {
// 	"numero_nf": 658
// }
// // *** FIM EXEMPLO PARA CONSULTAR UMA NF-e POR NUMERO

// // EXEMPLO PARA CONSULTAR UMA NF-e POR ID DO DOCUMENTO
// var js = {
// 	"documento_id": 13
// }
// // *** FIM EXEMPLO PARA CONSULTAR UMA NF-e POR ID DO DOCUMENTO






// // EXEMPLO PARA CORREÇÃO DE UMA NF-e POR NUMERO
// var js = {
// 	"numero_nf": 11,
// 	"correcao": "Item 1 unidade de medida KG"
// }
// // *** FIM EXEMPLO PARA CORREÇÃO DE UMA NF-e POR ID DO DOCUMENTO

// // EXEMPLO PARA CORREÇÃO DE UMA NF-e POR ID DO DOCUMENTO
// var js = {
// 	"documento_id": 11,
// 	"correcao": "Item 1 unidade de medida KG"
// }
// // *** FIM EXEMPLO PARA CORREÇÃO DE UMA NF-e POR ID DO DOCUMENTO






// // EXEMPLO PARA CANCELAMENTO DE UMA NF-e POR NUMERO
// var js = {
// 	"numero_nf": 11,
// 	"justificativa": "Desacordo no ato de entrega dos produtos"
// }
// // *** FIM EXEMPLO PARA CANCELAMENTO DE UMA NF-e POR ID DO DOCUMENTO

// // EXEMPLO PARA CANCELAMENTO DE UMA NF-e POR ID DO DOCUMENTO
// var js = {
// 	"documento_id": 11,
// 	"justificativa": "Desacordo no ato de entrega dos produtos"
// }
// *** FIM EXEMPLO PARA CANCELAMENTO DE UMA NF-e POR ID DO DOCUMENTO