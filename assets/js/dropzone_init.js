$(document).ready(function(){

Dropzone.options.dropzoneFrom = {
autoProcessQueue:false,
acceptedFiles: ".png, .jpg, .gif, .bmp, .jpeg",
init:function(){
var submitButton = document.querySelector("#submit-all");
myDropzone = this;
submitButton.addEventListener('click', function(){
myDropzone.processQueue();
});

this.on('complete', function(){
if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length ==0){
  var _this = this;
  _this.removeAllFiles();
}

list_image();

});
},
};

list_image();

function list_image(){
  $.ajax({
       url: base_url + "ajax/upload",
       method:'POST',       
       success:function(data){
        $('#preview').html(data);
       }
  });
}

$(document).on('click', '.remove_image', function(){
var name = $(this).attr('id');
var id_imagem = $(this).attr('data-id');

$.ajax({
 url: base_url+'ajax/upload',
 method:"POST",
 data:{name:name, imagem:id_imagem},
 success:function(data){
  list_image();
 }
});
});

}); 