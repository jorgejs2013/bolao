// var list = [
// 'html', 'css', 'javascript', 'jquery','json', 'php'
// ];

$('#ncm').select2({
  placeholder:"Digite para consultar",
  minimumInputLength:3,
  language:{
    inputTooShort:function(){
    	return 'Digite 3 ou mais caracteres';
    },
    noResults:function(){
    	return 'Nenhum resultado encontrado';
    },
    searching:function(){
    	return 'Pesquisando...';
    },
    errorLoading:function(){
    	return 'Erro ao ler resultados';
    },
    maximumSelected:function(args){
    	return 'Erro ler resultados';
    }
  },
  //quando n é ajax fazer da forma abaixo
  //data:list
  //quando é com ajax usar metodo abaixo:
  ajax: {
    url: base_url+'ajax/ncms',
    dataType:'json',
    type:'get',    
    delay: 250,
    data:function(params){
    	return {
    		pesq:params.term,
    	};
    },
    processResults: function (data) {    	
      return {
        results: data
      };
    },
    cache:true
  }
});//select do ncm

$('#cliente').select2({
  placeholder:"Digite para consultar",
  minimumInputLength:3,
  language:{
    inputTooShort:function(){
      return 'Digite 3 ou mais caracteres';
    },
    noResults:function(){
      return 'Nenhum resultado encontrado';
    },
    searching:function(){
      return 'Pesquisando...';
    },
    errorLoading:function(){
      return 'Erro ao ler resultados';
    },
    maximumSelected:function(args){
      return 'Erro ler resultados';
    }
  },
  //quando n é ajax fazer da forma abaixo
  //data:list
  //quando é com ajax usar metodo abaixo:
  ajax: {
    url: base_url+'ajax/clientes',
    dataType:'json',
    type:'get',    
    delay: 250,
    data:function(params){
      return {
        pesq:params.term,
      };
    },
    processResults: function (data) {     
      return {
        results: data
      };
    },
    cache:true
  }
});//select do ncm