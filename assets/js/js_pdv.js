$(document).ready(function(){

$('#codigo_produto').on('keydown', function(event){
var q = $(this).val();

if(event.keyCode == 13){
	if(q == ""){
       alert('Digite algum valor');
	}else{  
    
		$.ajax({
          url:base_url+'vendas/pesquisaPorCodigo',
          type:'POST',
          data:{q:q},
          dataType:'json',
          success:function(json){
            
            $('#qtde').val(1);
            $('#preco').val(json.preco);
            $('#total').val(json.preco);
            
            $('#id_produto').val(json.id_produto);
            $('#descricao').html(json.produto);
            
            if(json.imagem != ""){
             $('#imagem').attr("src", base_url+"upload/"+json.imagem);            
            }

            $('.box-carregar').hide();
          },
          beforeSend:function(){
          	$('.box-carregar').show();
          }
		});//ajax

   $('#qtde').focus();
} //verifica se é vazio
}//verifica se tecla digitada é 13

});//evento keydown para codigo do produto

});//leitura do documento


//pesquisando o produto pelo nome
function getProduto(obj){
let prod = $(obj).val();

if(prod.length >=3){
  setTimeout(function(){
     pesquisar(prod);     
  },500);
}

}//getProduto 


function pesquisar(prod){
$('.searchresults ul li').remove();

$.ajax({
url:base_url+'vendas/pesquisaPorNome',
type:'post',
data:{prod:prod},
dataType:'json',
success:function(data){
  let html = '';
  if(data != '' && data.length > 0){

  for(let i in data){
    html += '<li data-imagem="'+data[i].imagem+'" data-preco="'+data[i].preco+'" data-produto="'+data[i].produto+'" data-prod="'+data[i].id_produto+'" onclick="selectProd(this)">'+data[i].produto+' - R$ '+ data[i].preco+'</li>';    
  }  

  $('.searchresults ul').append(html);
  $('.searchresults').show();

 }else{

  html += '<li>Nenhum produto encontrado!</li>';
  $('.searchresults ul').append(html);
  $('.searchresults').show();
 }   
  
}
});
}//pesquisar

function selectProd(obj){
let id_produto = $(obj).data('prod');  
let produto = $(obj).data('produto');
let preco = $(obj).data('preco'); 
let imagem = $(obj).data('imagem');

$('#qtde').val(1);
$('#preco').val(preco);
$('#total').val(preco);            
$('#id_produto').val(id_produto);
$('#descricao').html(produto);
            
if(imagem != ""){
 $('#imagem').attr("src", base_url+"upload/"+imagem);            
}

$('#nome_produto').val('');
$('.searchresults').hide();
}//selectProd


//Após selecionas a quantidade adicionar na tabela dinamicamente
$('#qtde').on('keydown', function(event){
if(event.keyCode == 13){
 var id_produto = $('#id_produto').val();
 var qtde       = $('#qtde').val();
 var valor      = $('#preco').val();
 var descricao  = $('#descricao').html();

inserirItens(id_produto, qtde, valor, descricao);
// 	$.ajax({
//           url:BASE_URL+'vendas/inserir',
//           type:'POST',
//           data:{id_venda:id_venda, id_produto:id_produto, qtde:qtde, valor:valor},
//           dataType:'json',
//           success:function(json){

//           	inserirItens(json);
//           	$('.box-carregar').hide();      
           
//           },
//           beforeSend:function(){
//           	$('.box-carregar').show();
//           }
// 		});//ajax
	
}
// });//campo produto
});//quando confirma a quantidade


var qtde_itens = 0;
function inserirItens(id_produto, qtde, valor, descricao){

var produto = $('#descricao').html();
var qtde = $('#qtde').val();
var preco = $('#preco').val();

var subtotal = qtde * valor;

var tr = "<tr>"+
 "<td>"+ ++qtde_itens +" <input type='hidden' class='idProd' name='id_prod[]' value='"+id_produto+"' /></td>"+
 "<td>"+produto+"</td>"+
 "<td><input type='hidden' class='p_quant' value='"+qtde+"' />"+qtde+"</td>"+
 "<td><input type='hidden' class='p_preco' value='"+preco+"' />"+preco+"</td>"+
 "<td>"+subtotal.toFixed(2)+"</td>"+
 "<td><a href='javascript:;' data-idItemVenda='"+id_produto+"'' data-valor='"+subtotal+"' onclick='excluirProduto(this)' class='btn'>Excluir</a></td>"+
 "</tr>";

 $('#itensDaVenda').append(tr);
 atualizarTotal("+", subtotal);
 limpar();
}//inseriritens


function atualizarTotal(tipo, valor){
var resultado = 0;
if(tipo === "+"){
 resultado =  parseFloat(soma) + parseFloat(valor);
}else{
 resultado =  parseFloat(soma) - parseFloat(valor);
}

  
$('#total_geral').html(resultado.toFixed(2));
soma  = resultado.toFixed(2);
}//atualizarTotal


function excluirProduto(obj){
var id_item = $(obj).attr('data-idItemVenda');
var valor = $(obj).attr('data-valor');

$(obj).closest("tr").remove(); 
atualizarTotal("-", valor);
// $.ajax({
//           url:BASE_URL+'vendas/excluir',
//           type:'POST',
//           data:{id_item:id_item, id_venda:id_venda, valor:valor},
//           dataType:'json',
//           success:function(json){           
//            
//             atualizarTotal("-", valor);
//             $('.box-carregar').hide();
           
//           },
//           beforeSend:function(){
//             $('.box-carregar').show();
//           }
//     });//ajax
}//excluirProduto

function limpar(){
$('#codigo_produto').val("");
$('#descricao').html("");
$('#qtde').val("");
$('#preco').val("");
$('#total').val("");
$('#imagem').attr("src", base_url+"assets/images/not_image.jpg");

$('#codigo_produto').focus();
}//limpar


$('#finalizar').on('click', function(){
let product = $('.idProd');
let qtd = $('.p_quant');
let preco = $('.p_preco');

var venda = [];

for(let i = 0; i  < product.length; i++){
 let prod = $('.idProd').eq(i);
 let quant = $('.p_quant').eq(i);
 let valor = $('.p_preco').eq(i);

 venda.push({"produto":prod.val(), "quant":quant.val(), 'valor':valor.val()});
}
//JSON.stringify('variavel_aqui');
$.ajax({
url:base_url+'vendas/inserirvenda',
type:'post',
data:{dados: venda},
dataType:'json',
success:function(res){
 if(res == '1'){
  alert('venda efetuada com sucesso!');
  limpar();
  $('#itensDaVenda').html('');
 }
}
});

});

$('#limpartudo').on('click', function(){
  limpar();
  $('#itensDaVenda').html('');
  soma = 0;
  $('#total_geral').html('0');
});