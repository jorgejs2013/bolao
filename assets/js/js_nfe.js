function gerar_xml(id_nota){

var statusNota = "#statusNfe"+id_nota;

$.ajax({
url:base_url+"nfe/gerarxml/"+id_nota,
type:"POST",
data:[],
dataType:'json',
success:function(data){

if(data.erro  > 0){ 
 $(statusNota).html('Deu erro');
 $('#msg_erro').css("display", "block");
 $('#txt_erro').html(data.msg_erro);
}else{
$(statusNota).html('XML gerado!');
}


},
beforeSend:function(){
$(statusNota).html('<i class="load2">Aguarde...</i>');
},
error:function(data){ 
 $('#msg_erro').css("display", "block");
 $('#txt_erro').html(data.responseText);
}

});	

}