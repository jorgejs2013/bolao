function getProduto(obj){
let prod = $(obj).val();

if(prod.length >=3){
  setTimeout(function(){
     pesquisar(prod);
  },500);
}

}//getProduto 

function selectProd(obj){
  let id_produto = $(obj).data('prod');  
  let produto = $(obj).data('produto');
  let preco = $(obj).data('preco'); 

  var html = '';
  html += `
  <tr id="linha${id_produto}">
  <td><input type="hidden" name="id_prod[]" value="${id_produto}"/> ${id_produto}</td>
  <td><input type="text" name="prod[]" value="${produto}" readonly /></td>
  <td><input type="number" name="qtde[]" class="p_quant" min="1" value="1" onchange="updateSubtotal(this)" data-price="${preco}"/></td>
  <td><input type="text" name="preco[]" value="${preco}"/></td>
  <td><span class="subtotal">R$ ${preco}</span></td>
  <td><button type="button" onclick="removeTr(${id_produto})" class="btn btn-error"><i class=" fa fa-trash"></i> Remover</button></td>
  </tr>
  `;
  

  $('#table_dinamic').append(html);
  $('input[name=nome_prod]').val('');
 
  $('.searchresults').hide();
  updateSubtotal();
}

function removeTr(linha){
 $('#linha'+linha).remove();
  updateSubtotal();
}//removeTr

function updateSubtotal(obj){
var quant = $(obj).val();

if(quant <= 0){
 $(obj).val(1); 
 quant = 1; 
}
  var price = $(obj).attr('data-price');
  var subtotal = price * quant;
  
  $(obj).closest('tr').find('.subtotal').html('R$ '+subtotal.toFixed(2));

  updateTotal();  
}//updateSubtotal


function updateTotal(){
var total = 0;

for(var q =0;q<$('.p_quant').length;q++){
var quant = $('.p_quant').eq(q);

var price = quant.attr('data-price');
var subtotal = price * parseInt(quant.val());

total += subtotal;
}

$('.total_price').html(total.toFixed(2));

}//function updateTotal


function pesquisar(prod){
$('.searchresults ul li').remove();

$.ajax({
url:base_url+'vendas/pesquisaPorNome',
type:'post',
data:{prod:prod},
dataType:'json',
success:function(data){
  let html = '';
  if(data != '' && data.length > 0){

  for(let i in data){
    html += '<li data-preco="'+data[i].preco+'" data-produto="'+data[i].produto+'" data-prod="'+data[i].id_produto+'" onclick="selectProd(this)">'+data[i].produto+' - R$ '+ data[i].preco+'</li>';    
  }  

  $('.searchresults ul').append(html);
  $('.searchresults').show();

 }else{

  html += '<li>Nenhum produto encontrado!</li>';
  $('.searchresults ul').append(html);
  $('.searchresults').show();
 }   
  
}
});
}//pesquisar

//quando altera o campo telefone para consultar se existe o cliente na base
$(document).on('change', '#telefone', function(){
let telefone = $(this).val();

if(telefone.length > 10){
$.ajax({
url:base_url+'ajax/consultaCliente',
type:'post',
data:{telefone:telefone},
dataType:'json',
success:function(json){

$('#cliente').val(json.nome);
$('#cep').val(json.cep);  
$('#bairro').val(json.bairro);
$('#numero').val(json.numero);
$('#logradouro').val(json.logradouro);
$('#complemento').val(json.complemento);
$('#uf').val(json.uf_estado);
$('#cidade').val(json.nome_cidade); 
$('#referencia').focus(); 

}
});
}
});


//pegando cep dinamicamente
$(document).on('change', '#cep', function(){
let cep = $(this).val();

console.log(cep.length);
if(cep.length >8){

$.ajax({
url:'https://viacep.com.br/ws/'+cep+'/json/',
type:'get',
success:function(data){

$('#bairro').val(data.bairro);
$('#logradouro').val(data.logradouro);
$('#complemento').val(data.complemento);
$('#uf').val(data.uf);
$('#cidade').val(data.localidade);  

},
error:function(erro){
  alert('Erro ao requisitar tente novamente!!! ' + erro);
},
cache:true
});
}
// if(telefone.length >10 && telefone < 12){
//   console.log('vou pesquisar aqui');
// }

});

// Quando seleciona o cliente pelo nome
$(document).on('change', '#cliente', function(){
let cliente = $(this).val();

$.ajax({
url:base_url+'ajax/consultaClientePorNome',
type:'post',
data:{cliente:cliente},
dataType:'json',
success:function(json){

$('#telefone').val(json.celular);
$('#cep').val(json.cep);  
$('#bairro').val(json.bairro);
$('#numero').val(json.numero);
$('#logradouro').val(json.logradouro);
$('#complemento').val(json.complemento);
$('#uf').val(json.uf_estado);
$('#cidade').val(json.nome_cidade); 
$('#referencia').focus(); 

}
});
});

// Quando altera a situação do pedido
$(document).on('change', '#situacao', function(){
let situacao = $(this).val();
let id_delivery = $(this).attr('data-delivery');

$.ajax({
url:base_url+'ajax/atualizaSituacao',
type:'post',
data:{situacao:situacao, id_delivery:id_delivery},
dataType:'json',
success:function(json){

if(json == 1){
  alert('Status alterado com sucesso!');
  location.reload();
}else{
  alert('Ocorreu um erro ao atualizar tente novamente!');
  location.reload();
}

}
});

});