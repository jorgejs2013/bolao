$(document).ready(function(){

function load_data(page, query = '', query2 = ''){	
	$.ajax({
        url:base_url+'produtos/fetch',
        method:'POST',
        data:{page:page, query:query, query2:query2},
        success:function(data){
        	
        	$('.conteudo_dinamico').html(data);
        }
	});
}

load_data(1);


//se preferir que o usuario click no botao de enviar
$('#pesq_form').click(function(e){
e.preventDefault();
var query = $('#campo_nome').val();
var query2 = $('#campo_categoria').val();
load_data(1, query, query2);
});

$(document).on('click', '.page-link', function(){
var page = $(this).data('page_number');
var query = $('#campo_nome').val();
var query2 = $('#campo_categoria').val();
load_data(page, query, query2);
});

//se preferir ao digitar usar opção abaixo
// $('#box_pesquisa').keypress(function(){
// var query = $('#box_pesquisa').val();
// load_data(1, query);
// });


});