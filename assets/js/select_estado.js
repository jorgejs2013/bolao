$(document).on('change', '#estado', function(){
let estado = $(this).val();

$.ajax({
url:base_url+'ajax/changeUF',
method:'post',
data:{estado:estado},
dataType:'json',
beforeSend:function(){
$('#cidade').html('<option value="">Carregando...</option>');
},
success:function(res){

	 var template = '';
	 res.forEach((uf)=>{
       template += `<option value="${uf.id_cidade}">${uf.nome_cidade}</option>`;
	 });

	 $('#cidade').html(template);
	 
}
});

});