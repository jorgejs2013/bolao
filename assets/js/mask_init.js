 $(document).ready(function(){
  $('.cep').mask('00000-000');
  $('.cpf').mask('000.000.000-00', {reverse: true});
  $('.date').mask('00/00/0000'); 
  $('.time').mask('00:00:00');
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('.phone').mask('(00) 00000-0000');
  $('.telefone').mask('0000-0000');
  $('.celular').mask('(00) 00000-0000');
  $('.aposta').mask('00.00.00.00.00.00.00.00.00.00');
  $('.money').mask('000.000.000.000.000,00', {reverse: true});  
 }); 