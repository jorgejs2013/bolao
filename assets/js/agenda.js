document.addEventListener('DOMContentLoaded', function(){
var calendarEl = document.getElementById('calendar');

var calendar = new FullCalendar.Calendar(calendarEl, {
  locale:'pt-br',
  plugins:['interaction', 'dayGrid','timeGrid'],  
  header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },   
  //defaultDate: '2019-06-20',
  editable:true,  
  eventLimit:true,  
  events:'agenda/listaEvents',
  extraParams:function(){
    return {
      cachebuster:new Date().valusOf()
    };
  },

  eventClick:function(info){ 
  console.log(info);   
  let fim =  (info.event.end == null) ? info.event.start.toLocaleString() : info.event.end.toLocaleString(); 


let html = `<h2>Titulo: ${info.event.title}</h2> <br><br>
<h2>Inicio: ${info.event.start.toLocaleString()}</h2> <br><br> 
<h2>Termino: ${fim}</h2> <br> <br>
<h2>Descrição: ${info.event._def.extendedProps.desc}</h2>
` ;

let dialogify = new Dialogify(html)
.title(info.event.title)
.buttons([
        {
            text:'Fechar',
            type: Dialogify.BUTTON_DANGER,
            click: function(e){
                console.log('danger-style button click');              
                this.close();
            }
        },{

          text:'Editar',
          type:Dialogify.BUTTON_CENTER,
          click:function(e){
            let idAgenda = info.event._def.publicId;            
            console.log('editar');
            location.href = base_url+'agenda/edit/'+idAgenda;
          }
        },
        
    ], {position: Dialogify.BUTTON_CENTER});
dialogify.showModal();
// fim do modal personalizado com dialogify

    //info.jsEvent.preventDefault();
    //alert('Event:' +info.event.title);
    //info.el.style.borderColor = 'red';
    },//eventClick  
    selectable:true,
    select:function(info){
      //alert('Inicio do evento'+ info.start.toLocaleString());
      //alert('#start'+ info.start.toLocaleString());
      window.location.href = base_url+'agenda/add';
      //alert('#end' + info.end.toLocaleString());
    },
    eventResize: function(info) {
    alert(info.event.title + " end is now " + info.event.end.toLocaleString());

    // if (!confirm("is this okay?")) {
    //   info.revert();
    // }
    },
    eventDrop: function(info) {
    alert(info.event.title + " was dropped on " + info.event.start.toLocaleString());  

     if (!confirm("Deseja realmente alterar a data?")) {
       info.revert();
     }else{
      alert('ajax para atualizar o evento');
     }
  }
});

calendar.render();  

})  