$(document).ready(function(){

$('#btnInserir').on('click', function(){

var id = $('#id_produto').val();
var produto = $('#add_prod').val();
var qtde = $('#qtde').val();
var preco = $('#preco').val();


$.ajax({
url:base_url+'produtos/addavulsa',
type:'POST',
data:{id_produto:id, qtde:qtde, preco:preco},
dataType:'json',
success:function(data){	

      inserirItens();
      //lista_itens(data);

}
});//ajax

});//quando clica no botao de inserir


//usamos para pesquisar o produto
$('#add_prod').on('keyup', function(){
var pesq = $(this).val();

if(pesq.length >=3){
$.ajax({
url:base_url+'ajax/search_products',
type:'GET',
data:{pesq:pesq},
dataType:'json',
success:function(data){

if($('.listaProdutos').length === 0){
  $('#add_prod').after('<div class="listaProdutos"></div>');
}
$('.listaProdutos').css('left', $('#add_prod').offset().left+'px');
$('.listaProdutos').css('top', $('#add_prod').offset().top + $('#add_prod').height()+3+'px');


var html = "";
for(var i in data){
	html += '<div class="si"><a href="javascript:;" onclick="selecionarProduto(this)" data-id="'+data[i].id_produto+'" data-preco="'+data[i].preco+'" data-nome="'+data[i].produto+'">'+ data[i].produto +' - R$ '+ data[i].preco +'</a></div>';
}

$('.listaProdutos').html(html);
$('.listaProdutos').show();

}
});//ajax
}//se a pesquisa foi digitado mais de 3 letras

});

});//quando o documento é lido


function selecionarProduto(obj){
var id = $(obj).attr('data-id');	
var nome = $(obj).attr('data-nome');
var preco = $(obj).attr('data-preco');
$('.listaProdutos').hide();

$('#id_produto').val(id);
$('#qtde').val(1);
$("#qtde").focus();
$('#preco').val(preco);
$('#add_prod').val(nome);
}//selecionarProduto

function inserirItens(){
var id = $('#id_produto').val();
var produto = $('#add_prod').val();
var qtde = $('#qtde').val();
var preco = $('#preco').val();
var subtotal = preco * qtde;

if($('input[name="quant['+id+']"]').length ==0){
var tr = "<tr>"+
"<td>"+id+"</td>"+
"<td>03/06/2019</td>"+
"<td><input type='' name='produto[]' value='"+produto+"' /></td>"+
'<td><input type="number" min="1" name="quant[]" data-idProduto="'+ id +'"  value="'+qtde+'" /></td>'+
"<td><input type='' name='preco[]' value='"+preco+"'/></td>"+
'<td class="subtotal">R$ '+ subtotal +'</td>'+
"</tr>";

$('#lista_itens').append(tr);

limpar();
}else{
	exibeNotificacoes("info", "Produto ja inseriro", "Este produto já está na lista, você pode mudar a quantidade");
	limpar();
}

}//inserirItens

function lista_itens(data){
html = "<tr>";

for(var i in data){
var subtotal = 	data[i].valor_entrada + data[i].qtde_entrada;

html += "<td>"+i+"</td>"+
"<td>"+data[i].data_entrada+"</td>"+
"<td><input type='' name='produto[]' value='"+data[i].produto+"' /></td>"+
'<td><input readonly type="number" min="1" name="quant[]" value="'+data[i].estoque_atual+'" /></td>'+
"<td><input readonly type='' name='preco[]' value='"+data[i].valor_entrada+"'/></td>"+
'<td class="subtotal">R$ '+ subtotal +'</td>'+
"</tr>";	
}

$('#lista_itens').append(html);
limpar();
}//listaItens


function limpar(){
$('#id_produto').val("");
$('#add_prod').val("");
$('#qtde').val("");
$('#preco').val("");
}//limpar


function atualizaTotal(){
var total = 0;

for(var i = 0;i < $('.p_quant').length;i++){
var quant  = $('.p_quant').eq(i);
var preco = quant.attr("data-preco");
var subtotal = preco * parseInt(quant.val());

total += subtotal;
}

$('#total').html("R$ "+total);
atualizaTotalNoBanco(total);
}//atualizaTotal

function atualizarSubtotal(obj){
var id_produto = $(obj).attr('data-idProduto');

var quant = $(obj).val();
if(quant <=0){
	$(obj).val(1);
	quant = 1;
}

var preco = $(obj).attr('data-preco');
var subtotal = preco * quant;

$(obj).closest('tr').find('.subtotal').html("R$ "+subtotal);
atualizaQtdeItem(id_produto, quant);
atualizaTotal();
}//atualizarSubtotal



function atualizaTotalNoBanco(total){

console.log('alterar a quantidade');	
// $.ajax({
// url:BASE_URL+'pedido/atualizaTotal/',
// type:'POST',
// data:{ total:total, id_pedido:id_pedido},
// dataType:'json',
// success:function(data){
	

// }
// });//ajax
}//atualizaTotalNoBanco

function limparItens(){
$('#lista_itens').html("");
}
