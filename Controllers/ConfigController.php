<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Relatorio;
use Models\Crud;

class ConfigController extends Controller {

	private $user;
    private $arrayInfo;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $usuario = $this->user->getid();
        $alias = 'super-admin';
        $this->permissao = new Permissao(); 
        $this->permissao->temPermissao($usuario, $alias, 'listar');         
        $permissoes = $this->permissao->getPermissoes($usuario);

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'home',
         'bread'=>'Inicio',
         'permissoes'=>$permissoes 
        );          
}

public function edit(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init'
);     

$c = new Crud();
$this->arrayInfo['info'] = $c->listaPorCampo('config', "id_config=1");    
 
if(isset($_POST['ambiente']) && !empty($_POST['ambiente'])){
$ambiente = addslashes(trim($_POST['ambiente']));
$serie = addslashes(trim($_POST['serie']));
$certificado = addslashes(trim($_POST['certificado']));
$senha = addslashes(trim($_POST['senha_certificado']));
$versao = addslashes(trim($_POST['versao']));

if($senha != ''){

$form = new \stdClass();
$form->nfe_ambiente = $ambiente;
$form->nfe_serie = $serie;
$form->certificado_digital = $certificado;
$form->senha = $senha;
$form->nfe_versao = $versao;

$c->atualizar('config', $form, "id_config=1");
header("Location: ".BASE_URL."config/edit");
exit;

}else{

$form = new \stdClass();
$form->nfe_ambiente = $ambiente;
$form->nfe_serie = $serie;
$form->certificado_digital = $certificado;
$form->nfe_versao = $versao;

$c->atualizar('config', $form, "id_config=1"); 
header("Location: ".BASE_URL."config/edit");
exit; 
}

}
$this->loadTemplate('nfe/config/adicionar', $this->arrayInfo);
}//edit action


}