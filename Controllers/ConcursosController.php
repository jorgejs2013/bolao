<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class ConcursosController extends Controller {

	private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }         

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Cadastro',
         'bread'=>'Municipios',         
        );         
    }

public function index() { 
$this->arr['list_js'] = array();  

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arr['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
} 

$concursos = new Orm('concursos');
$this->arr['lista'] = $concursos->select('*')->order(['id desc'])->paginate('10')->get();
$this->arr['paginacao'] = $concursos->render('concursos');


$this->loadTemplate('concursos/listar', $this->arr);
}//function index 


public function add(){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min',
'flatpickr/flatpickr',
'flatpickr/flatpickr_init'
); 

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arr['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
} 


$this->loadTemplate('concursos/adicionar', $this->arr);
}//add

public function add_action(){

if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']);
$data = addslashes($_POST['data']); 
$data_limite = addslashes($_POST['data_limite']);   
$hora_limite = $this->limpaCampo($_POST['hora_limite']);
$valor_aposta = addslashes($_POST['valor_aposta']);  
$valor_acumulado = addslashes($_POST['valor_acumulado']); 
$taxa_administracao = addslashes($_POST['taxa_administracao']);
$comissao_vendedor = addslashes($_POST['comissao_vendedor']);
$observacao = addslashes($_POST['observacao']);


if($valor_aposta == 0){
  $_SESSION['msg'] = "Valor da aposta não pode ser 0!";
  header("Location: ".BASE_URL."concursos/edit/".$id);
  exit;
}


if($data_limite < $data){
    $_SESSION['msg'] = "Data limite não pode ser inferior a data!";
  header("Location: ".BASE_URL."concursos/edit/".$id);
  exit;
}

$insere = (new Orm('concursos'))->set([
'nome'=>$nome,
'data'=>$this->converterData($data,'/'),
'data_limite'=>$this->converterData($data_limite,'/'),
'hora_limite'=>$hora_limite,
'valor_aposta'=>$this->valor($valor_aposta),
'valor_acumulado'=>$this->valor($valor_acumulado),
'taxa_administracao'=>$this->valor($taxa_administracao),
'comissao_vendedor'=>$this->valor($comissao_vendedor),
'observacao'=>$observacao
])->save();

$_SESSION['msg'] = "Concurso cadastrado com sucesso!";
  header("Location: ".BASE_URL."concursos");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."concursos/add");
   exit; 
}

}//add action

public function edit($id){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'flatpickr/flatpickr',
'flatpickr/flatpickr_init',
'sweetalert2.all.min'
);  

$this->arr['info'] = (new Orm('concursos'))->select('*')->where(['id', $id])->first()->get();
$this->arr['id_concurso'] = $id;

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arr['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
}


$this->loadTemplate('concursos/editar', $this->arr);
}//edit


public function edit_action($id){

if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']);
$data = addslashes($_POST['data']); 
$data_limite = addslashes($_POST['data_limite']);   
$hora_limite = $this->limpaCampo($_POST['hora_limite']);
$valor_aposta = addslashes($_POST['valor_aposta']);  
$valor_acumulado = addslashes($_POST['valor_acumulado']); 
$taxa_administracao = addslashes($_POST['taxa_administracao']);
$comissao_vendedor = addslashes($_POST['comissao_vendedor']);
$observacao = addslashes($_POST['observacao']);


if($valor_aposta == 0){
  $_SESSION['msg'] = "Valor da aposta não pode ser 0!";
  header("Location: ".BASE_URL."concursos/edit/".$id);
  exit;
}

if($data_limite < $data){
    $_SESSION['msg'] = "Data limite não pode ser inferior a data!";
  header("Location: ".BASE_URL."concursos/edit/".$id);
  exit;
}


$insere = (new Orm('concursos'))->set([
'nome'=>$nome,
'data'=>$this->converterData($data,'/'),
'data_limite'=>$this->converterData($data_limite,'/'),
'hora_limite'=>$hora_limite,
'valor_aposta'=>$this->valor($valor_aposta),
'valor_acumulado'=>$this->valor($valor_acumulado),
'taxa_administracao'=>$this->valor($taxa_administracao),
'comissao_vendedor'=>$this->valor($comissao_vendedor),
'observacao'=>$observacao
])->where(['id', $id])->update();


$_SESSION['msg'] = "Concurso atualizado com sucesso!";
  header("Location: ".BASE_URL."concursos");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."concursos/edit/".$id);
   exit; 
}

}//add action



public function del($id){
$m = new Orm('concursos');
$m->del(['id', $id]);        

$_SESSION['msg'] = "Removido com sucesso!";
header("Location: ".BASE_URL."concursos");
exit;
}//del

}