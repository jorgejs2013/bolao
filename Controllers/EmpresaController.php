<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;
use Models\Helpers;

class EmpresaController extends Controller {

	private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }         

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Cadastro',
         'bread'=>'Municipios',         
        );         
    }

public function index() { 
$this->arr['list_js'] = array(

);  

$this->arr['lista'] = (new Orm('empresa'))->select('*')->get();


$this->loadTemplate('empresas/listar', $this->arr);
}//function index 



public function add(){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  

$this->arr['cidades'] = (new Orm('municipios'))->select('*')->get();


//echo $this->debug($this->arr['cidades']);


$this->loadTemplate('empresas/adicionar', $this->arr);
}//add

public function add_action(){
if(isset($_POST['razao_social']) && !empty($_POST['razao_social'])){     
$razao_social    = $this->limpaCampo($_POST['razao_social']);
$endereco        = $this->limpaCampo($_POST['endereco']); 
$numero_endereco = $this->limpaCampo($_POST['numero_endereco']);   
$bairro          = $this->limpaCampo($_POST['bairro']);  
$codigo_cidade   = $this->limpaCampo($_POST['codigo_cidade']); 
$cpf_cnpj        = $this->limpaCampo($_POST['cpf_cnpj']); 


$insere = (new Orm('empresa'))->set([
'nome_razao'=>strtoupper($razao_social),
'endereco'=>strtoupper($endereco),
'numero_endereco'=>$numero_endereco,
'bairro'=>strtoupper($bairro),
'codigo_cidade'=>$codigo_cidade,
'cpf_cnpj'=>$cpf_cnpj
])->save();

if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['tmp_name'])){
$h = new Helpers(); 
$img_capa = $h->upload($_FILES['imagem'], 'medias/', $h->Name($razao_social), 500, 350);
$imagem = $img_capa;

$cad_imagem = (new Orm('foto_empresa'))->set([
 'CODIGO_EMPRESA'=>$insere,
 'DATA_FOTO'=>date('Y-m-d'),
 'FOTO'=>$imagem
])->save();
}

$_SESSION['msg'] = "Empresa cadastrada com sucesso!";
  header("Location: ".BASE_URL."empresa");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."empresa/add");
   exit; 
}

}//add action

public function edit($id){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  


$this->arr['cidades'] = (new Orm('municipios'))->select('*')->get();
$this->arr['id_empresa'] = $id;
$this->arr['info'] = (new Orm('empresa'))->select('*')->where(['id', $id])->first()->get();
$this->arr['info']->imagem = (new Orm('foto_empresa'))->select('*')->where(['CODIGO_EMPRESA', $id])->first()->get();


//echo $this->debug($this->arr['info']);

$this->loadTemplate('empresas/editar', $this->arr);
}//edit


public function edit_action($id){
if(isset($_POST['razao_social']) && !empty($_POST['razao_social'])){     
$razao_social    = $this->limpaCampo($_POST['razao_social']);
$endereco        = $this->limpaCampo($_POST['endereco']); 
$numero_endereco = $this->limpaCampo($_POST['numero_endereco']);   
$bairro          = $this->limpaCampo($_POST['bairro']);  
$codigo_cidade   = $this->limpaCampo($_POST['codigo_cidade']); 
$cpf_cnpj        = $this->limpaCampo($_POST['cpf_cnpj']); 

$insere = (new Orm('empresa'))->set([
'nome_razao'=>strtoupper($razao_social),
'endereco'=>strtoupper($endereco),
'numero_endereco'=>$numero_endereco,
'bairro'=>strtoupper($bairro),
'codigo_cidade'=>$codigo_cidade,
'cpf_cnpj'=>$cpf_cnpj
])->where(['id', $id])->update();


if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['tmp_name'])){

$h = new Helpers(); 
$img_capa = $h->upload($_FILES['imagem'], 'medias/', $h->Name($razao_social), 500, 350);
$imagem = $img_capa;

$cad_imagem = (new Orm('foto_empresa'))->set([
 'FOTO'=>$imagem
])->where(['CODIGO_EMPRESA', $id])->update();

$this->arr['img_old'] = (new Orm('foto_empresa'))->select('*')->where(['CODIGO_EMPRESA', $id])->first()->get(); 
//se a imagem existir dentro da pasta remover antes de inserir a proxima
if(!empty($this->arr['img_old'])){
if(file_exists($this->arr['img_old']->FOTO)){  
 unlink(BASE_URL."medias/".$this->arr['img_old']['FOTO']);
}
}
}


$_SESSION['msg'] = "Empresa atualizada com sucesso!";
  header("Location: ".BASE_URL."empresa");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."empresa/edit/".$id);
   exit; 
}

}//add action



public function del($id){
$m = new Orm('empresa');
$m->del(['id', $id]);        

$_SESSION['msg'] = "Removido com sucesso!";
header("Location: ".BASE_URL."empresa");
exit;
}//del

}