<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class OperadoresController extends Controller {

	private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }         

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Cadastro',
         'bread'=>'Municipios',         
        );         
    }

public function index() { 
$this->arr['list_js'] = array(

);  

$this->arr['lista'] = (new Orm('operadores'))->select('*')->get();


$this->loadTemplate('operadores/listar', $this->arr);
}//function index 



public function add(){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  

$this->arr['lista'] = (new Orm('operadores'))->select('*')
->where(['gerente','S'])->get();


$this->loadTemplate('operadores/adicionar', $this->arr);
}//add

public function add_action(){
if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']);
$senha = addslashes($_POST['senha']); 
$codigo_hash = addslashes($_POST['codigo_hash']);   
$telefone = addslashes($_POST['telefone']);  
$promotor = addslashes($_POST['promotor']); 


$situacao = (isset($_POST['situacao'])) ? 'A': 'I';
$aposta_automatica = (isset($_POST['aposta_automatica'])) ? 'S': 'N';
$cambista = (isset($_POST['cambista'])) ? 'S': 'N';
$gerente = (isset($_POST['gerente'])) ? 'S': 'N';


$insere = (new Orm('operadores'))->set([
'nome'=>$nome,
'situacao'=>$situacao,
'senha'=>$senha,
'aposta_automatica'=>$aposta_automatica,
'cambista'=>$cambista,
'codigo_hash'=>$codigo_hash,
'telefone'=>$telefone,
'promotor'=>$promotor,
'gerente'=>$gerente
])->save();

$_SESSION['msg'] = "Operador cadastrado com sucesso!";
  header("Location: ".BASE_URL."operadores");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."operadores/add");
   exit; 
}

}//add action

public function edit($id){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  
$this->arr['info'] = (new Orm('operadores'))->select('*')->where(['id', $id])->first()->get();
$this->arr['id_operador'] = $id;


$this->loadTemplate('operadores/editar', $this->arr);
}//edit


public function edit_action($id){
if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']);
$senha = addslashes($_POST['senha']); 
$codigo_hash = addslashes($_POST['codigo_hash']);   
$telefone = addslashes($_POST['telefone']);  
$promotor = addslashes($_POST['promotor']); 


$situacao = (isset($_POST['situacao'])) ? 'A': 'I';
$aposta_automatica = (isset($_POST['aposta_automatica'])) ? 'S': 'N';
$cambista = (isset($_POST['cambista'])) ? 'S': 'N';
$gerente = (isset($_POST['gerente'])) ? 'S': 'N';


$insere = (new Orm('operadores'))->set([
'nome'=>$nome,
'situacao'=>$situacao,
'senha'=>$senha,
'aposta_automatica'=>$aposta_automatica,
'cambista'=>$cambista,
'codigo_hash'=>$codigo_hash,
'telefone'=>$telefone,
'promotor'=>$promotor,
'gerente'=>$gerente
])->where(['id', $id])->update();

$_SESSION['msg'] = "Operador atualizado com sucesso!";
  header("Location: ".BASE_URL."operadores");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."operadores/edit/".$id);
   exit; 
}

}//add action



public function del($id){
$m = new Orm('operadores');
$m->del(['id', $id]);        

$_SESSION['msg'] = "Removido com sucesso!";
header("Location: ".BASE_URL."operadores");
exit;
}//del

}