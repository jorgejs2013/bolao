<?php
use Core\Controller;
use Models\Users;
use Models\Helpers;
use Models\Permissao;
use Models\Crud;

class UsuarioacaoController extends controller {

	private $user;
    private $arrayInfo;

    public function __construct() {        
        $this->user = new Users();
        
        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }

      $usuario = $this->user->getid();
      $this->permissao = new Permissao();        
      $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 
      $permissoes = $this->permissao->getPermissoes($usuario);

       $this->arrayInfo = array(
      'user'=>$this->user,
      'menuActive'=>'contato',
      'bread'=>'Contato',
       'permissoes'=>$permissoes 
     );  

      $this->permissao = new Permissao();        
      $this->permissao->temPermissao($usuario, 'super-admin', 'listar');  
       
    }//Construtor

public function index() {        
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
'jquery.table-shrinker',
'shrinker_init',
);
$c = new Crud();    

$this->arrayInfo['list'] = $c->lista('usuario_acao', null);

$this->loadTemplate('permissoes/acao', $this->arrayInfo);
}//index 

public function add(){
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
);
$id_company  = 1;//$this->user->getCompany();
$c = new Crud();


if(isset($_POST['nome']) && !empty($_POST['nome'])):           
$nome = addslashes(trim($_POST['nome']));            

$form  = new \stdClass();
$form->acao = $nome;

$c->inserir('usuario_acao', $form);
$dados['msg'] = 'Ação cadastrada com sucesso!'; 
endif;

   $this->loadTemplate('permissoes/acao_add', $this->arrayInfo);
}//add

public function edit($id){
$id_company  = 1;//$this->user->getCompany();
 $h = new Helpers();
 $c = new Crud();

$this->arrayInfo['info'] = $c->listaPorCampo('usuario_acao', "id_acao={$id}");

if(isset($_POST['nome']) && !empty($_POST['nome'])):           
         $nome = addslashes(trim($_POST['nome']));
         $slug = $h->Name($nome);   

$form = new \stdClass();
$form->acao = $nome;
$form->alias_acao = $slug;

$atualiza = $c->atualizar("usuario_acao", $form, "id_acao={$id}");      
$this->arrayInfo['msg'] = 'Ação Atualizada com sucesso!'; 
endif;

     $this->loadTemplate('permissoes/acao_edit', $this->arrayInfo);
}//edit


public function del($id){
$c = new Crud();
$deletar = $c->remover('usuario_acao', "id_acao={$id}");
header("Location: ".BASE_URL."usuarioacao");
exit;
}//del 


}