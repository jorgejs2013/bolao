<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Helpers;
use Models\Orm;

class UsuariosController extends Controller {

	private $user;
  private $arrayInfo;

  public function __construct() {              
    $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
        exit;         
  }  


        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'usuarios',    
        );     

      
    }

public function index() {
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  
$this->arrayInfo['bread'] = "Usuários"; 


$this->arrayInfo['listaUsers'] = (new Orm('usuario'))->select('*')->get();
if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arrayInfo['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
}

$this->loadTemplate('usuarios/usuarios', $this->arrayInfo);
}//function index   



public function add(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);    
$this->arrayInfo['bread'] = "Usuários Adicionar";      
$this->arrayInfo['errorItems'] = array();
$o = new Orm('usuario');
// $this->permissao = new Permissao();        
// $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 
$this->arrayInfo['gerentes'] = $o->select('*')->where(['eh_gerente', 'S'])->get();
    
if(isset($_SESSION['formError']) && count($_SESSION['formError']) >0){
    $this->arrayInfo['errorItems'] = $_SESSION['formError'];
    unset($_SESSION['formError']);
}

$this->loadTemplate('usuarios/usuarios_add', $this->arrayInfo);
}//add

public function add_action(){
$c = new Orm('usuario');
$h = new Helpers();

if(isset($_POST['email']) && !empty($_POST['email'])){     
$nome = addslashes($_POST['nome']);
$email = addslashes($_POST['email']); 
$contato = addslashes($_POST['contato']);   
$password = addslashes($_POST['password']); 
$senha = password_hash($password, PASSWORD_DEFAULT);
$status = 1;  
$comissao = addslashes($_POST['comissao']); 
$imagem = 'assets/images/user.png'; 
    
$data_cad = date('Y-m-d');
$lastupdate = date('Y-m-d H:i:s');  
$admin = (isset($_POST['admin'])) ? '1' : '0'; 
$cambista = (isset($_POST['cambista'])) ? 'S' : 'N'; 
$gerente = (isset($_POST['gerente'])) ? $_POST['gerente'] : 'admin';
$eh_gerente =  (isset($_POST['eh_gerente'])) ? 'S': 'N';

if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['tmp_name'])){
$img_capa = $h->upload($_FILES['imagem'], 'medias/', $h->Name($nome), 500, 350);
$imagem = $img_capa;
}

$insere = $c->set([
'id_company'=>1,
'nome'=>$nome,
'slug_nome'=>$h->Name($nome),
'email'=>$email,
'contato'=>$contato,
'imagem'=>$imagem,
'senha'=>$senha,
'data_cad'=>$data_cad,
'lastactivity'=>$lastupdate,
'admin'=>$admin,
'eh_gerente'=>$eh_gerente,
'gerente'=>$gerente,
'cambista'=>$cambista,
'status'=>$status,
'comissao'=>$comissao
])->save();

$_SESSION['msg'] = "Usuário cadastrado com sucesso!";
 header("Location: ".BASE_URL."usuarios");
  exit;
}else{
   $_SESSION['formError'] = array('name');
   
   header("Location: ".BASE_URL."usuarios/add");
   exit; 
}

}//add action


public function edit($id){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);   
$this->arrayInfo['bread'] = "Usuários Editar";      
$this->arrayInfo['errorItems'] = array();

// $this->permissao = new Permissao();        
// $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 

$o = new Orm('usuario');
$this->arrayInfo['gerentes']    = $o->select('*')->where(['eh_gerente', 'S'])->get();
$this->arrayInfo['info_usuario'] = $o->select('*')->where(['id', $id])->first()->get();
$this->arrayInfo['id_user'] = $id;

if(isset($_SESSION['formError']) && count($_SESSION['formError']) >0){
    $this->arrayInfo['errorItems'] = $_SESSION['formError'];
    unset($_SESSION['formError']);
}

$this->loadTemplate('usuarios/usuarios_edit', $this->arrayInfo);
}//edit


public function edit_action($id){
$h = new Helpers();
$c = new Orm('usuario');

if(isset($_POST['email']) && !empty($_POST['email'])){     
$nome = addslashes($_POST['nome']);
$email = addslashes($_POST['email']); 
$contato = addslashes($_POST['contato']); 
$password = addslashes($_POST['password']); 
$senha = password_hash($password, PASSWORD_DEFAULT);
$status = 1; 
$comissao = addslashes($_POST['comissao']); 
$imagem = 'assets/images/user.png'; 
$admin = (isset($_POST['admin'])) ? '1' : '0'; 
$cambista = (isset($_POST['cambista'])) ? 'S' : 'N'; 
$aleatorio = md5(time().mt_rand(100, 999));
$gerente = (isset($_POST['gerente'])) ? $_POST['gerente'] : 'admin';
$eh_gerente =  (isset($_POST['eh_gerente'])) ? 'S': 'N';

if(isset($_FILES['imagem']) && !empty($_FILES['imagem']['tmp_name'])){
  $img_capa = $h->upload($_FILES['imagem'], 'medias/', $h->Name($nome), 500, 350);
  $imagem = $img_capa;

//Pega a imagem do usuario
$this->arrayInfo['img_old'] = $c->select('*')->where(['id', $id]); 
//se a imagem existir dentro da pasta remover antes de inserir a proxima
if(file_exists($this->arrayInfo['img_old']['imagem'])){  
 unlink($this->arrayInfo['img_old']['imagem']);
}  //se existe a imagem na pasta para apagar
}//se teve o post da imagem 

$lastupdate = date('Y-m-d H:i:s'); 

if(isset($_POST['password']) && !empty($_POST['password'])){

$c->set([
'id_company'=>1,
'nome'=>$nome,
'slug_nome'=>$h->Name($nome),
'email'=>$email,
'contato'=>$contato,
'imagem'=>$imagem,
'senha'=>$senha,
'data_cad'=>$data_cad,
'lastactivity'=>$lastupdate,
'admin'=>$admin,
'eh_gerente'=>$eh_gerente,
'gerente'=>$gerente,
'cambista'=>$cambista,
'status'=>$status,
'comissao'=>$comissao
])->where(['id', $id])->update();

$c->atualizar("usuario", $form, "id={$id}");  
header("Location: ".BASE_URL."usuarios");
exit;
}else{
$c->set([
'id_company'=>1,
'nome'=>$nome,
'slug_nome'=>$h->Name($nome),
'email'=>$email,
'contato'=>$contato,
'imagem'=>$imagem,
'lastactivity'=>$lastupdate,
'admin'=>$admin,
'eh_gerente'=>$eh_gerente,
'gerente'=>$gerente,
'cambista'=>$cambista,
'status'=>$status,
'comissao'=>$comissao
])->where(['id', $id])->update();

header("Location: ".BASE_URL."usuarios");
exit;
}

}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."usuarios/edit");
   exit; 
}

}//edit action


public function perfil(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'flatpickr/flatpickr',
'flatpickr/flatpickr_init',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  

$c = new Crud();
$this->arrayInfo['info'] = $c->listaPorCampo('empresa', "id_empresa=1");
$this->permissao = new Permissao();        
 $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 

if(isset($_POST['razao_social']) && !empty($_POST['razao_social'])):
$razao_social = addslashes($_POST['razao_social']);
$nome_fantasia = addslashes($_POST['nome_fantasia']);
$cnpj = addslashes($_POST['cnpj']);
$ddd = addslashes($_POST['ddd']);
$fone = addslashes($_POST['fone']);
$celular = addslashes($_POST['cel']);
$email = addslashes($_POST['email']);
$email_secundario = addslashes($_POST['email_secundario']);
$email_contabilidade = addslashes($_POST['email_contabilidade']);
$cep = addslashes($_POST['cep']);
$logradouro = addslashes($_POST['logradouro']);
$complemento = addslashes($_POST['complemento']);
$numero = addslashes($_POST['numero']);

$form = new \stdClass();
$form->razao_social = $razao_social;
$form->nome_fantasia = $nome_fantasia;
$form->cnpj = $cnpj;
$form->ddd = $ddd;
$form->fone = $fone;
$form->celular = $celular;
$form->email = $email;
$form->email_secundario = $email_secundario;
$form->email_contabilidade = $email_contabilidade;
$form->cep = $cep;
$form->logradouro = $logradouro;
$form->complemento = $complemento;
$form->numero = $numero;

$c->atualizar('empresa', $form, "id_empresa='1'");
$this->arrayInfo['msg'] = "Dados atualizados com sucesso!";
endif;



$this->loadTemplate('usuarios/perfil', $this->arrayInfo);
}//perfil


public function reset(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  

$c = new Crud();
$usuario = $this->user->getId();
$user = $c->listaPorCampo('usuario', "id={$usuario}");

if(isset($_POST['old_senha']) &&  !empty($_POST['senha']) && !empty($_POST['csenha'])){
 $senha = addslashes($_POST['old_senha']);
 $newSenha = addslashes($_POST['senha']); 
 $newpass = password_hash($newSenha, PASSWORD_DEFAULT);


if(password_verify($senha, $user->senha)){

 $form = new \stdClass();
 $form->senha = $newpass;

 $c->atualizar('usuario',$form, "id={$usuario}");
 $this->arrayInfo['msg'] = "Senha atualizada com sucesso!"; 
}else{
  $this->arrayInfo['msg'] = "senhas não conferem"; 
}

}

$this->loadTemplate('usuarios/reset', $this->arrayInfo);
}//reset

public function logout(){
session_destroy();
unset($_SESSION['token']);

header("Location: ".BASE_URL);
exit;
}//logout


public function del($id){
$this->permissao = new Permissao();        
$this->permissao->temPermissao($usuario, 'super-admin', 'listar');   
$c = new Orm('usuario');
$utc = new Orm('usuario_tabela_acao_cliente');

$utc->del(['id_usuario', $id]); 
$c->del(['id', $id]);        

$_SESSION['msg'] = "Removido com sucesso!";
header("Location: ".BASE_URL."usuarios");
exit;
}//del

}