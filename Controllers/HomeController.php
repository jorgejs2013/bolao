<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class HomeController extends Controller {

	private $user;
    private $arrayInfo;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'home',
         'bread'=>'Inicio'        
        );          
}

public function index() {   
$this->arrayInfo['list_js'] = array(
'jquery.table-shrinker',
'shrinker_init' 
);    



$this->loadTemplate('home', $this->arrayInfo);
}//function index   




}