<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Relatorio;
use Models\Crud;
use Models\Config;

class BackupController extends Controller {

	private $user;
  private $arrayInfo;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $usuario = $this->user->getid();
      $this->permissao = new Permissao();        
      $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 
      $permissoes = $this->permissao->getPermissoes($usuario); 

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'Backup',
         'bread'=>'Backup',
         'permissoes'=>$permissoes 
        );   

      
    }

public function index() {   
$c = new Config();
$this->arrayInfo['lista'] = $c->backup();

$this->arrayInfo['list_js'] = array(
'marcar_todos_checkbox2',
);   

$this->loadTemplate('config/backup', $this->arrayInfo);
}//function index 


public function gerar(){
$c = new Config();

if(isset($_POST['table']) && !empty($_POST['table'])){ 

$saida = $c->gerarBackup($_POST['table']);

$nome_arq = 'bd_backup_on_'.time();
$nome_arquivo =  $nome_arq.'.sql';

//Criar o diretório de backup
$diretorio = 'bkp/';
if(!is_dir($diretorio)){
  mkdir($diretorio, 0777, true);
  chmod($diretorio, 0777);
}

if(file_exists($diretorio.$nome_arquivo)){

echo "existe o arquivo";
exit;

}else{

$download = $diretorio.$nome_arquivo;

$file_handle = fopen($download, 'w+');
fwrite($file_handle, $saida);
fclose($file_handle);

//aqui eu mando baixar o arquivo
//$this->baixar($nome_arq);
header("Location:".BASE_URL."Backup/baixar/".$nome_arq);
exit;
}//fim do if que verifica se existe o arquivo
}// fim do if que verifica se teve o post da tabela


}//gerar



public function baixar($download){

$caminho = "bkp/".$download.".sql";

if(file_exists($caminho)){  

header("Expires:0");
//header("Content-Type: application/octet-stream");
header('Content-Type: application/download');
header('Pragma: public'); 
header('Connection: Keep-Alive');
//header("Cache-Control: private", false); 
header("Content-Description: File Transfer");
header("Content-Length:".filesize($caminho));
header('Content-Disposition: attachment; filename='.$download.".sql");

header("Content-Transfer-Encoding:binary");
header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
ob_clean();
flush();
readfile($caminho);
unlink($caminho); 
}//fim do if que verifica se existe o arquivo de download

}//baixar



}