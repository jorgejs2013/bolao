<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class ApostasController extends Controller {

	private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }         

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Cadastro',
         'bread'=>'Apostas',         
        );         
    }

public function index() { 
$this->arr['list_js'] = array(

);  

$this->arr['lista'] = (new Orm('concursos'))->select('*')->get();


$this->loadTemplate('apostas/listar', $this->arr);
}//function index 



public function add(){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  

$this->arr['concurso'] = (new Orm('concursos'))->select('*')->first()->get();

//echo $this->debug($this->arr['concurso']);


$this->loadTemplate('apostas/adicionar', $this->arr);
}//add

public function add_action(){
if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']); 
$telefone = addslashes($_POST['telefone']);  
$aposta = addslashes($_POST['aposta']); 

echo $nome."<br>";
echo $telefone."<br>";
echo $aposta;
exit;

$insere = (new Orm('concursos'))->set([
'nome'=>$nome,
'situacao'=>$situacao,
'senha'=>$senha,
'aposta_automatica'=>$aposta_automatica,
'cambista'=>$cambista,
'codigo_hash'=>$codigo_hash,
'telefone'=>$telefone,
'promotor'=>$promotor,
'gerente'=>$gerente
])->save();

$_SESSION['msg'] = "Operador cadastrado com sucesso!";
  header("Location: ".BASE_URL."concursos");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."concursos/add");
   exit; 
}

}//add action

public function edit($id){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  
$this->arr['info'] = (new Orm('concursos'))->select('*')->where(['id', $id])->first()->get();
$this->arr['id_operador'] = $id;


$this->loadTemplate('concursos/editar', $this->arr);
}//edit


public function edit_action($id){
if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']);
$senha = addslashes($_POST['senha']); 
$codigo_hash = addslashes($_POST['codigo_hash']);   
$telefone = addslashes($_POST['telefone']);  
$promotor = addslashes($_POST['promotor']); 


$situacao = (isset($_POST['situacao'])) ? 'A': 'I';
$aposta_automatica = (isset($_POST['aposta_automatica'])) ? 'S': 'N';
$cambista = (isset($_POST['cambista'])) ? 'S': 'N';
$gerente = (isset($_POST['gerente'])) ? 'S': 'N';


$insere = (new Orm('concursos'))->set([
'nome'=>$nome,
'situacao'=>$situacao,
'senha'=>$senha,
'aposta_automatica'=>$aposta_automatica,
'cambista'=>$cambista,
'codigo_hash'=>$codigo_hash,
'telefone'=>$telefone,
'promotor'=>$promotor,
'gerente'=>$gerente
])->where(['id', $id])->update();

$_SESSION['msg'] = "Operador atualizado com sucesso!";
  header("Location: ".BASE_URL."concursos");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."concursos/edit/".$id);
   exit; 
}

}//add action



public function del($id){
$m = new Orm('concursos');
$m->del(['id', $id]);        

$_SESSION['msg'] = "Removido com sucesso!";
header("Location: ".BASE_URL."concursos");
exit;
}//del

}