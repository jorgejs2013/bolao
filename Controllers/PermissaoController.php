<?php
use Core\Controller;
use Models\Users;
use Models\Helpers;
use Models\Permissao;
use Models\TabelaAcao;
use Models\Crud;

class PermissaoController extends controller {

	private $user;
    private $arrayInfo;

public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $usuario = $this->user->getid();
        $alias = 'super-admin';
        $this->permissao = new Permissao();            
        $permissoes = $this->permissao->getPermissoes($usuario);

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'home',
         'bread'=>'Inicio',
         'permissoes'=>$permissoes 
        );          
}

public function index($id = null) {                
$this->arrayInfo['lista'] = $this->lista($id);
$this->arrayInfo['usuario'] = (object )array('id_usuario'=> $id);    


    $this->loadTemplate('permissoes/permissoes', $this->arrayInfo);
}//index 


public function lista($id_usuario){
$c = new Crud();    

$objTabelaAcao = new TabelaAcao();
$p = new Permissao(); 
         
$tabelas = $c->lista('usuario_tabela');
$data = array();

foreach($tabelas as $tabela){   

 $acoes = $objTabelaAcao->getListaPorTabela($tabela->id_tabela);          
 $action = array();

foreach($acoes as $acao){

    $tem = $p->getPermissao($id_usuario, $tabela->id_tabela, $acao->id_acao);                        
    $marcado = ($tem) ? true : false;
    //echo "Marcado: ".$marcado."<br>";

    $action[] = (object) array(
     'id_acao'=>$acao->id_acao,
     'id_tabela_acao'=>$acao->id_tabela_acao,
     'acao'=>$acao->acao,                 
     'marcado'=>$marcado
    );              
}//foreach acoes

    $data[] = (object) array(
    'tabela'=>$tabela,
    'acoes'=>$action            
    );
            
}//foreach tabelas

return $data;
}//lista


public function inserir() {
$p = new Permissao();
$c = new Crud();

//$objPermissao = new QueryBuilder('usuario_tabela_acao_cliente');

$id_usuario = isset($_POST['id_usuario']) ? strip_tags(filter_input(INPUT_POST, "id_usuario")) : NULL;
$id_tabela = isset($_POST['id_tabela']) ? strip_tags(filter_input(INPUT_POST, "id_tabela")) : NULL;
$id_acao = isset($_POST['id_acao']) ? strip_tags(filter_input(INPUT_POST, "id_acao")) : NULL;

$tem = $p->getPermissao($id_usuario, $id_tabela, $id_acao);

if(!$tem){   
   $insere = $p->inserir($id_usuario, $id_tabela, $id_acao);
   echo json_encode("ok");
}else{
    echo json_encode("nao");
} 

}//inserir 


public function semPermissao(){

$this->loadView('permissoes/sem-permissao');
}//semPermissao    


public function excluir(){
$p = new Permissao();

$id_usuario = isset($_POST['id_usuario']) ? strip_tags(filter_input(INPUT_POST, "id_usuario")) : NULL;
$id_tabela = isset($_POST['id_tabela']) ? strip_tags(filter_input(INPUT_POST, "id_tabela")) : NULL;
$id_acao = isset($_POST['id_acao']) ? strip_tags(filter_input(INPUT_POST, "id_acao")) : NULL;

$p->excluir($id_usuario, $id_tabela, $id_acao);
echo json_encode("ok");
}//excluir


}