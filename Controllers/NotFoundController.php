<?php
use Core\Controller;

class NotFoundController extends Controller {
    

    public function index() {
        $dados = array();
        
        $this->loadView('404', $dados);
    }

}