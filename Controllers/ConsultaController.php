<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;
use Models\Concursos;

class ConsultaController extends Controller {

	private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }         

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Consulta',
         'bread'=>'Consulta',         
        );         
    }

public function index() { 
$this->arr['list_js'] = array(
'chosen.jquery.min',
'parsley/parsley.min',
'parsley/pt-br',
);  

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arr['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
} 

$this->arr['concursos'] = (new Orm('concursos'))->select('*')->order(['id desc'])->get();
$this->arr['operadores'] = (new Orm('operadores'))->select('*')->order(['id desc'])->get();
$this->arr['apostadores'] = (new Orm('usuarios'))->select('*')->order(['codigo desc'])->get();
$this->arr['lista'] = array();
// ON concursos.ID = numeros_apostados.ID_CONCURSO
// INNER JOIN operadores
// ON numeros_apostados.USUARIO = operadores.ID";

$concursos = new Orm('concursos as c');
$this->arr['lista'] = $concursos->select(['*, c.ID as id_concurso, operadores.NOME as nome_operador'])
->join(['numeros_apostados'],['operadores'])
->on(['c.ID','=','numeros_apostados.ID_CONCURSO'],['numeros_apostados.USUARIO','=','operadores.ID'])
->paginate('10')->get();
$this->arr['paginacao'] = $concursos->render('consulta');


//echo $this->debug($this->arr['lista']);


if(isset($_GET['concurso']) OR isset($_GET['operador']) OR isset($_GET['apostador'])){
$concurso  = $_GET['concurso'];
$operador  = $_GET['operador'];
$apostador = $_GET['apostador'];

$c = new Concursos();

$this->arr['lista'] = $c->listaConcursos($concurso, $operador, $apostador);

//$this->debug($this->arr['lista']);
}


$this->loadTemplate('consultas/listar', $this->arr);
}//function index 


public function add(){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min',
'flatpickr/flatpickr',
'flatpickr/flatpickr_init'
); 

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arr['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
} 


$this->loadTemplate('concursos/adicionar', $this->arr);
}//add


}