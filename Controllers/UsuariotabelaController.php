<?php
use Core\Controller;
use Models\Users;
use Models\Helpers;
use Models\Permissao;
use Models\Orm;
use Models\Crud;

class UsuariotabelaController extends controller {

	private $user;
  private $arrayInfo;

    public function __construct() {        
        $this->user = new Users();
        
        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }

      $usuario = $this->user->getid();
      $this->permissao = new Permissao();        
      $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 
      $permissoes = $this->permissao->getPermissoes($usuario);

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'Tabela Permissão',
         'bread'=>'Tabela Permissão',
         'permissoes'=>$permissoes 
        ); 

      
       
    }//Construtor

public function index() {
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
'jquery.table-shrinker',
'shrinker_init',
);

$c = new Orm('usuario_tabela');
$this->arrayInfo['list'] = $c->select('*')->get();

$this->loadTemplate('permissoes/tabelas', $this->arrayInfo);
}//index 

public function add(){
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);    
$id_company  = 1;//$this->user->getCompany();
$h = new Helpers();
$c = new Orm('usuario_tabela');

if(isset($_POST['nome']) && !empty($_POST['nome'])):           
$nome = addslashes(trim($_POST['nome']));
$ativa = addslashes(trim($_POST['acao']));
$slug = $h->Name($nome);   

$form = new \stdClass();
$form->nome_tabela = $nome;
$form->ativo_tabela = $ativa;
$form->alias_tabela = $slug;

$c->set([
'nome_tabela'=>$nome,
'ativo_tabela'=>$ativa,
'alias_tabela'=>$slug
])->save();
   
$this->arrayInfo['msg'] = 'Tabela cadastrada com sucesso!'; 
endif;

    $this->loadTemplate('permissoes/tabela_add', $this->arrayInfo);
}//add


public function edit($id){
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);   
$id_company  = 1;//$this->user->getCompany();
$h = new Helpers();
$c = new Orm('usuario_tabela');

$this->arrayInfo['info'] = $c->select('*')->where(['id_tabela', $id])->first()->get();

if(isset($_POST['nome']) && !empty($_POST['nome'])):           
$nome = addslashes(trim($_POST['nome']));
$ativa = addslashes(trim($_POST['acao'])); 
$slug = $h->Name($nome);      


$c->set([
'nome_tabela'=>$nome,
'ativo_tabela'=>$ativa,
'alias_tabela'=>$slug
])->where(['id_tabela', $id])->update();

$this->arrayInfo['msg'] = 'Tabela atualizada com sucesso!';           
endif;

        $this->loadTemplate('permissoes/tabela_edit', $this->arrayInfo);
}//edit



public function acoes($id_tabela){
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
); 

$id_company  = 1;//$this->user->getCompany();
$c = new Crud(); 
$p = new Permissao();     

if(!$id_tabela){
   header("Location:".BASE_URL."usuarioTabela");
   exit;
}else{
  $tabela = $c->listaPorCampo('usuario_tabela', "id_tabela={$id_tabela}");
}

if(!$tabela){
   header("Location:".BASE_URL."usuarioTabela");
   exit;
}

$this->arrayInfo['tabela']  = $tabela; 
$this->arrayInfo['acoes']   = $c->lista('usuario_acao', null);
$this->arrayInfo['tabelas'] = $c->lista('usuario_tabela');
$this->arrayInfo['lista']   = $p->listaAcoes($id_tabela);

        $this->loadTemplate('permissoes/tabela_acoes', $this->arrayInfo);
}//ações


public function inserir(){   
$c = new Crud(); 

$id_tabela = isset($_POST['id_tabela']) ? strip_tags(filter_input(INPUT_POST, "id_tabela")): NULL;    
$id_acao = isset($_POST['id_acao']) ? strip_tags(filter_input(INPUT_POST, "id_acao")): NULL;

$tem = $c->listaPorCampo('usuario_tabela_acao', "id_tabela={$id_tabela} AND id_acao={$id_acao}");

if(!$tem){
  
  $form = new \stdClass();
  $form->id_tabela = $id_tabela;
  $form->id_acao = $id_acao; 

  $c->inserir('usuario_tabela_acao', $form); 
}

header("Location:".BASE_URL."usuariotabela/acoes/".$id_tabela);
exit;

}//inserir

public function editar($id_tabela, $id_acao){
$c = new Crud();

$form = new \stdClass();
$form->id_tabela = $id_tabela;
$form->id_acao = $id_acao;

$atualizar = $c->atualizar('usuario_tabela_acao', $form, "id_tabela={$id_tabela} AND id_acao={$id_acao}");  
}//editar


public function del($id){
$c = new Crud();
$c->remover('usuario_tabela', "id_tabela={$id}");

header("Location:".BASE_URL."usuarioTabela");
exit;
}//del


}