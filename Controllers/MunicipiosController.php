<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class MunicipiosController extends Controller {

	private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }         

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Cadastro',
         'bread'=>'Municipios',         
        );         
    }

public function index() { 
$this->arr['list_js'] = array(

);  

$this->arr['lista'] = (new Orm('municipios'))->select('*')->get();


$this->loadTemplate('municipios/listar', $this->arr);
}//function index 



public function add(){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min',
'select_estado'
);  


$this->loadTemplate('municipios/adicionar', $this->arr);
}//add

public function add_action(){
if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']);
$uf = addslashes($_POST['uf']); 
$sef = addslashes($_POST['sef']);   
$srf = addslashes($_POST['srf']);  
$ibge = addslashes($_POST['ibge']); 

$insere = (new Orm('municipios'))->set([
'nome'=>$nome,
'uf'=>$uf,
'sef'=>$sef,
'srf'=>$srf,
'ibge'=>$ibge
])->save();

$_SESSION['msg'] = "Município cadastrado com sucesso!";
  header("Location: ".BASE_URL."municipios");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."municipios/add");
   exit; 
}

}//add action

public function edit($id){
$this->arr['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  
$this->arr['info'] = (new Orm('municipios'))->select('*')->where(['codigo', $id])->first()->get();
$this->arr['id_municipio'] = $id;


$this->loadTemplate('municipios/editar', $this->arr);
}//edit


public function edit_action($id){
if(isset($_POST['nome']) && !empty($_POST['nome'])){     
$nome = addslashes($_POST['nome']);
$uf = addslashes($_POST['uf']); 
$sef = addslashes($_POST['sef']);   
$srf = addslashes($_POST['srf']);  
$ibge = addslashes($_POST['ibge']); 

$insere = (new Orm('municipios'))->set([
'nome'=>$nome,
'uf'=>$uf,
'sef'=>$sef,
'srf'=>$srf,
'ibge'=>$ibge
])->where(['codigo', $id])->update();

$_SESSION['msg'] = "Município atualizado com sucesso!";
  header("Location: ".BASE_URL."municipios");
  exit;
}else{
   $_SESSION['formError'] = array('name');   
   header("Location: ".BASE_URL."municipios/edit/".$id);
   exit; 
}

}//add action



public function del($id){
$m = new Orm('municipios');
$m->del(['codigo', $id]);        

$_SESSION['msg'] = "Removido com sucesso!";
header("Location: ".BASE_URL."municipios");
exit;
}//del

}