<?php
use Core\Controller;
use Models\Users;
use Models\Email;
use Models\Orm;
use Models\Permissao;

class AjaxController extends Controller{

private $user;
private $arrayInfo;

public function __construct() {              
$this->user = new Users();

if($this->user->isLogged() == false){                  
 header("Location: ".BASE_URL."login");        
 exit;         
}  

$usuario = $this->user->getid();        
$this->permissao = new Permissao();            
$permissoes = $this->permissao->getPermissoes($usuario);

$this->arrayInfo = array(
  'user'=>$this->user,     
  'permissoes'=>$permissoes 
);  

}//construtor


public function atualizaPagamento(){
$novo_status = $_POST['opc'];
$reserva = $_POST['reserva'];


$atualiza = new Orm('rifa_reserva');
$up = $atualiza->set(['pagamento_cambista'=>$novo_status])->where(['id_rifa_reserva', $reserva])->update();
echo json_encode('ok');
exit;
}//AtualizaPagamento


public function consultaCliente(){
$c = new Crud();
$telefone = filter_input(INPUT_POST, 'telefone');
$data     = $c->listaComJoin('*', 'contato', array(['cidade'], ['estado']),
array(['contato.id_cidade', '=', 'cidade.id_cidade'], ['contato.id_estado', '=', 'estado.id_estado']),
"celular='{$telefone}' AND eh_cliente='S'", false);

echo json_encode($data);
exit;
}//consultaCliente

public function atualizaSituacao(){
$c = new Crud();    
$situacao = filter_input(INPUT_POST, 'situacao');
$id_delivery = filter_input(INPUT_POST, 'id_delivery');

$form = new \stdClass();
$form->status_delivery = $situacao;

$atualiza = $c->atualizar('delivery', $form, "id_delivery={$id_delivery}");

if($atualiza >0){
    echo json_encode('1');
    exit;
}else{
    echo json_encode('2');
    exit;
}

}//


public function consultaClientePorNome(){
$c = new Crud();
$cliente = filter_input(INPUT_POST, 'cliente');
$data     = $c->listaComJoin('*', 'contato', array(['cidade'], ['estado']),
array(['contato.id_cidade', '=', 'cidade.id_cidade'], ['contato.id_estado', '=', 'estado.id_estado']),
"id_contato='{$cliente}' AND eh_cliente='S'", false);

echo json_encode($data);
exit;
}//consultaCliente

public function ncms(){
$c = new Crud();
$data = array();

if(isset($_GET['pesq'])){
$q = $_GET['pesq'];

$result =  $c->search('ncms', 'descricao', $q, true);
if(COUNT($result) > 0){
foreach($result as $row){
    $data[] = array('id'=>$row->ncm, 'text'=>$row->ncm.' - '.$row->descricao);
}
}else{
    $data[] = array('id'=>0, 'text'=>'Nenhum NCM localizado');
}
}

echo json_encode($data);
exit;
}//ncms

public function clientes(){
$c = new Crud();
$data = array();

if(isset($_GET['pesq'])){
$q = $_GET['pesq'];

$result =  $c->search('contato', 'nome', $q, true);
if(COUNT($result) > 0){
foreach($result as $row){
    $data[] = array('id'=>$row->id_contato, 'text'=>$row->id_contato.' - '.$row->nome);
}
}else{
    $data[] = array('id'=>0, 'text'=>'Nenhum cliente localizado');
}
}

echo json_encode($data);
exit;
}//clientes


public function search_products(){
$c = new Crud();

if(isset($_GET['pesq']) && !empty($_GET['pesq'])){
$q = addslashes($_GET['pesq']);

$data = $c->search('produtos','produto', $q);

echo json_encode($data);
exit;
}
}//search products


public function updateAvatar(){
$data = $_POST['image'];
//data:image/png;base64,ipoajdfiajsdfopiajsdfpoiajfpoisajfpio
$image_array_1 = explode(';', $data);
$image_array_2 = explode(",", $image_array_1[1]);
$usuario = $_SESSION['user'];

$data = base64_decode($image_array_2[1]);

$imageName = time().'.png';

file_put_contents("medias/capas/".$imageName, $data);

$qr = new QueryBuilder('usuario');
$atualizado = $qr->set(['imagem'=> $imageName])
                 ->where(['id',$usuario])
                 ->update();

}//updateAvatar



public function uploads(){
	if(!empty($_FILES['file']['tmp_name'])){
		$types_allowed = array('image/jpeg', 'image/png');

		if(in_array($_FILES['file']['type'], $types_allowed)){

            $newname = md5(time().rand(0,999).rand(0,999)).'.jpg';
			move_uploaded_file($_FILES['file']['tmp_name'], 'medias/'.$newname);

            $array = array(
             'location'=> BASE_URL."medias/".$newname
            );
            echo json_encode($array);
		    exit;
		}
	}

}//uploads


public function upload(){
$folder_name = 'medias/';

//quando faz o upload
if(!empty($_FILES)){

  $temp_file = $_FILES['file']['tmp_name'];
  $location = $_FILES['file']['name'];
  $location = str_replace(' ', '-', $location); 
 
$imagens = new Orm('banners');
$inserir = $imagens->set(['imagem'=>$location, 'link'=>$link, 'status_btn'=>$status])->save();
  
move_uploaded_file($temp_file, $folder_name.$location);
}
//quando exclui a imagem
if(isset($_POST['name'])){
  $imagem = $_POST['imagem'];
  
  $filename = $folder_name.$_POST['name'];
  unlink($filename);

  $qr = new Orm('banners');
    $deletar = $qr->del(['id_banner', $imagem]);
}

//pegando as imagens da pasta
//pegando as imagens da pasta
$result = array();
$files = scandir('medias/');
$saida = '<div>';

$imagens = new Orm('banners');
$dados['imagens'] = $imagens->select('*')->get();

foreach ($dados['imagens'] as $imagem){
  $saida .= '<div class="imgs_drop"><img src="'.BASE_URL.'medias/'.$imagem['imagem'].'" width="150" height="100" data-dz-remove /><button type="button" class="remove_image" data-id="'.$imagem['id_banner'].'" id="'.$imagem['imagem'].'">Remover</button></div>';
}

$saida .= '</div>';
echo $saida;
}//upload

}//fim da class ajaxController