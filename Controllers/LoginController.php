<?php
use Core\Controller;
use Models\Users;
use Models\Helpers;

class LoginController extends Controller {

private $user;
private $dados;

public function __construct() {        
  $this->user = new Users();      
}  

public function index() {
$this->dados = array(
'error'=>''
); 

if(!empty($_SESSION['errorMsg'])){
  $this->dados['error'] = $_SESSION['errorMsg'];
  $_SESSION['errorMsg'] = '';
}

if(!empty($_POST['email']) && !empty($_POST['senha'])){       
  $username = strtolower($_POST['email']);      
  $pass = $_POST['senha'];      


    if($this->user->validateLogin($username, $pass)) {    
            $_SESSION['login_tentativas'] = 0;
            header("Location: ".BASE_URL);
            exit;
    } else{

            if (!isset($_SESSION['login_tentativas'])) {
              $_SESSION['login_tentativas'] = 0;
            }

            $_SESSION['login_tentativas']++;

            $this->dados['errorMsg'] = "E-mail e/ou Senha errados!";
            if (isset($_SESSION['login_tentativas']) && $_SESSION['login_tentativas'] >= 9) {
              $this->dados['errorMsg'] = "Várias tentativas, seu acesso foi bloqueado!";
            }else{
              echo $this->dados['m'] = "<script>swal('Use um e-mail válido!');</script>";
              $this->dados['alertmsg2'] = "Digite um e-mail válido!";
           }

      
       if (isset($_SESSION['login_tentativas']) && $_SESSION['login_tentativas'] == 8) {        
        $this->dados['alertmsg'] = "Você tentou várias vezes. Troque a senha ou entre em contato com o suporte, se não você será bloquado.";
      }

      }//if fazerLogin      
}//if principal   se existe email ou senha

$this->loadView('login/login', $this->dados);
}//function index


public function index_action(){

if(!empty($_POST['email']) && !empty($_POST['senha'])){
$email = addslashes($_POST['email']);
$password = addslashes($_POST['senha']);

$u = new Users();
if($u->validateLogin($email, $password)){  
  header("Location: ".BASE_URL);
  exit;
}else{  
  $_SESSION['errorMsg'] = 'Usuário e/ou senha errados';}
}else{
 $_SESSION['errorMsg'] = "Preencha os campos abaixo";
}

$this->dados['errorMsg'] = $_SESSION['errorMsg'];

$this->loadView('login/login', $this->dados);


}//index Action

public function logout(){
unset($_SESSION['token']);
session_destroy();
header("Location:".BASE_URL);
exit;
}//logout



public function signup(){

//  $this->dados = []; 
//  $h = new Helpers();

//  $bairros = new QueryBuilder('bairros');
//  $dados['lista_bairros'] = $bairros->select('*')->get();

// if(isset($_POST['nome']) && !empty($_POST['nome'])):

//   $nome = addslashes(trim($_POST['nome']));
//   $cidade = addslashes(trim($_POST['cidade']));
//   $bairro = addslashes(trim($_POST['bairro']));
//   $cep = addslashes(trim($_POST['cep']));
//   $apelido = addslashes(trim($_POST['apelido']));
//   $slug = $h->Name($apelido);
//   $email = addslashes(trim($_POST['email']));
//   $contato1 = addslashes(trim($_POST['telefone']));
//   $senha = addslashes(trim($_POST['password']));
//   $tipo = addslashes(trim($_POST['tipo']));
//   $newpass = password_hash($senha, PASSWORD_DEFAULT);
//   $grupo  = '2';


//   $bairros = new QueryBuilder('bairros');
//   $dados['getBairro'] = $bairros->select('*')->where(['nome', $bairro])->first()->get();

//   if($dados['getBairro'] == ''){
//      $insereBairro = $bairros->insert(['nome', 'cep', 'valor'])->values([$bairro, $cep, 7])->save();
//   }

// $qr = new QueryBuilder('usuario');
// $sql = $qr->insert(['id_company', 'nome', 'cidade',  'apelido', 'slug', 'email', 'contato1', 'senha', 'data_cad', 'lastactivity','tipo', 'status', 'status_online', 'id_group'])       
//          ->values(['1', $nome, $cidade, $apelido, $slug, $email, $contato1, $newpass, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $tipo, '2', '2', $grupo])
//          ->save();

// $qr2 = new QueryBuilder('clientes');
// $sql2 = $qr2->insert(['ref_user', 'nome', 'bairro', 'cidade', 'telefone1', 'data_cad'])->values([$sql, $nome, $bairro, $cidade, $contato1, date('Y-m-d')])->save();

//  $this->dados['msg'] = "Cadastro Realizado com sucesso!!!";        
// endif; 



// $this->loadView('login/signup', $this->dados);
}//signup

public function restore(){
echo "recuperar senha";
}//restore


}