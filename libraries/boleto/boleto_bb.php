<?php
$online = 'sim';

if($online == 'sim'){
try{
$dsn = "mysql:dbname=unixsist_boletosmil;host=bdhost0043.servidorwebfacil.com";
$dbuser = "unixsist_userbol";
$dbpass = "#boletosmil#";
$pdo = new PDO($dsn, $dbuser, $dbpass);
}catch(PDOException $e){
	die($e->getMessage());		
}

}else{
try{
$dsn = "mysql:dbname=dinamica;host=localhost";
$dbuser = "root";
$dbpass = "";
$pdo = new PDO($dsn, $dbuser, $dbpass);
}catch(PDOException $e){
	die($e->getMessage());		
}	
}

if(isset($id_boleto)){
    $idf = $id_boleto;
}else{
	$idf = 0;
}

//pegando o boleto para imprimir
$qr = "SELECT * FROM fatura_parcelas WHERE id_fatura_parcela = :id_fatura";
$qr = $pdo->prepare($qr);
$qr->execute([':id_fatura'=>$idf]);
$fat  = $qr->fetch(PDO::FETCH_OBJ)->id_fatura;


//pegando agora a fatura
$pesq = $pdo->prepare("SELECT * FROM faturas WHERE id_fatura  = :id_fatura ");
$pesq->bindValue(":id_fatura", $fat);
$pesq->execute();

if($pesq->rowCount() <= 0){
	echo "nao tem fatura";
	exit;
	header("Location:".BASE_URL."faturas");
	exit;
}

//Pegando dados da empresa
$sql = $pdo->prepare("SELECT * FROM empresa e, cidade c, estado es
WHERE e.id_cidade = c.id_cidade AND
e.id_estado = es.id_estado AND
e.id_empresa = '1'");
$sql->execute();
$empresa = $sql->fetch(PDO::FETCH_OBJ);

//Pegando dados da fatura
$sql2 = $pdo->prepare("SELECT * FROM faturas f, fatura_parcelas p, contato c, contas co, cidade ci, estado es WHERE 
f.id_cliente = c.id_contato AND
f.id_conta = co.id AND
c.id_cidade = ci.id_cidade AND 
c.id_estado = es.id_estado AND
f.id_fatura = p.id_fatura
AND p.id_fatura_parcela = :id");
$sql2->bindValue(":id", $id_boleto);
$sql2->execute();
$fatura = $sql2->fetch(PDO::FETCH_OBJ);

//informar ao dashboard que o cliente ja clicou em imprimir ou visualziar o boleto
$visto = $pdo->prepare("UPDATE fatura_parcelas SET visto = 'S', data_visto = :data WHERE id_fatura_parcela = :id");
$visto->bindValue(':data', date('Y-m-d'));
$visto->bindValue(':id', $idf);
$visto->execute();

//Convertendo os campos de data para o padrao correto
$dc = explode("-", $fatura->data_cad);
$datac = $dc[2]."/".$dc[1]."/".$dc[0];

$dv = explode("-", $fatura->venc_parcela);
$datav = $dv[2]."/".$dv[1]."/".$dv[0];


// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 5;
$taxa_boleto = 0;
$data_venc = $datav;  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = $fatura->valor_parcela; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["nosso_numero"] = $fatura->id_fatura_parcela;
$dadosboleto["numero_documento"] = $fatura->id_fatura;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = $datac; // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $fatura->nome_resp.". CPF/CNPJ:".$fatura->cpf;
$dadosboleto["endereco1"] =  $fatura->logradouro. ". ".$fatura->bairro;
$dadosboleto["endereco2"] = $fatura->nome_cidade. ' - '.$fatura->nome_estado.' CEP '.$fatura->cep;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = "Pagamento ref. a ".$fatura->descricao;
$dadosboleto["demonstrativo2"] = $empresa->nome_fantasia;
$dadosboleto["demonstrativo3"] = $empresa->logradouro.". ".$empresa->bairro.". ".$empresa->nome_cidade.". CEP:".$empresa->cep;

// INSTRU��ES PARA O CAIXA
$dadosboleto["instrucoes1"] =  $fatura->instrucao1;
$dadosboleto["instrucoes2"] =  $fatura->instrucao2;
$dadosboleto["instrucoes3"] = $fatura->instrucao3;
$dadosboleto["instrucoes4"] = "";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "1";
$dadosboleto["valor_unitario"] = $valor_boleto;
$dadosboleto["aceite"] = "N";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";


// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //
// DADOS DA SUA CONTA - BANCO DO BRASIL
$dadosboleto["agencia"] = $fatura->agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $fatura->conta; 	// Num da conta, sem digito

// DADOS PERSONALIZADOS - BANCO DO BRASIL
$dadosboleto["convenio"] = $fatura->convenio;  // Num do conv�nio - REGRA: 6 ou 7 ou 8 d�gitos
$dadosboleto["contrato"] = ""; // Num do seu contrato
$dadosboleto["carteira"] = $fatura->carteira;
$dadosboleto["variacao_carteira"] = "-".$fatura->variacao;  // Varia��o da Carteira, com tra�o (opcional)

// TIPO DO BOLETO
$dadosboleto["formatacao_convenio"] = "7"; // REGRA: 8 p/ Conv�nio c/ 8 d�gitos, 7 p/ Conv�nio c/ 7 d�gitos, ou 6 se Conv�nio c/ 6 d�gitos
$dadosboleto["formatacao_nosso_numero"] = "2"; // REGRA: Usado apenas p/ Conv�nio c/ 6 d�gitos: informe 1 se for NossoN�mero de at� 5 d�gitos ou 2 para op��o de at� 17 d�gitos

/*
#################################################
DESENVOLVIDO PARA CARTEIRA 18

- Carteira 18 com Convenio de 8 digitos
  Nosso n�mero: pode ser at� 9 d�gitos

- Carteira 18 com Convenio de 7 digitos
  Nosso n�mero: pode ser at� 10 d�gitos

- Carteira 18 com Convenio de 6 digitos
  Nosso n�mero:
  de 1 a 99999 para op��o de at� 5 d�gitos
  de 1 a 99999999999999999 para op��o de at� 17 d�gitos

#################################################
*/

// SEUS DADOS
$dadosboleto["identificacao"] = $empresa->nome_fantasia;
$dadosboleto["cpf_cnpj"] = $fatura->cnpj;
$dadosboleto["endereco"] = $fatura->logradouro;
$dadosboleto["cidade_uf"] = $fatura->nome_cidade." / ".$fatura->nome_estado;
$dadosboleto["cedente"] = $empresa->razao_social;

// N�O ALTERAR!
include("include/funcoes_bb.php"); 
include("include/layout_bb.php");
?>