<?php
error_reporting(E_ALL);
ini_set("display_errors", "On");
ini_set('error_log', dirname(__FILE__).'/log.txt');
setlocale(LC_ALL, 'pt_BR', "pt_BR.utf-8", "portuguese");

session_start();
require 'config.php';
require 'routers.php';

define("SITE_PATH", realpath(dirname(__FILE__))."/");

set_include_path(
SITE_PATH."Core".PATH_SEPARATOR.
SITE_PATH."Controllers/".PATH_SEPARATOR.
SITE_PATH."Models/".PATH_SEPARATOR.
get_include_path()
);

require_once("Core/Autoload.php");
//instanciando o PHPMailer
require_once("PHPMailer/PHPMailer.php");
require_once("PHPMailer/SMTP.php");
require_once("PHPMailer/Exception.php");

//chamdo a biblioteca fpdf
require_once("libraries/fpdf/fpdf.php");
//chamando a biblioteca do qrcode
require_once("libraries/phpqrcode/qrlib.php");

$carregar = new Core\Autoloader;
$carregar->registrar();

$core = new Core\Core;
$core->run();